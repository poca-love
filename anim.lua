--[[
This file is part of POCA - a puzzle game

POCA is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

anim = {}

function animate(thing, dt, index)
	--[[ This function keeps timing of running animations,
	which are represented by "anim" array and drawn elsewhere.
	Member has to have at least step, laststep, delay and speed defined.
	Optional: paused, delay - postpones animation;
	after[s]() is function to be executed after step s
	update() is function to be executed at each step
	]]
	if thing.paused then return end
	thing.delay = thing.delay - dt
	if settings.speed > 0 or thing.delay < 0 then
		repeat
			if thing.update then thing:update() end
			if thing.after and thing.after[thing.step] then
					thing.after[thing.step]()
			end
			thing.delay = thing.speed
			thing.step = thing.step + 1
			if thing.step > thing.laststep then
				anim[index] = nil
			end
		until not anim[index] or settings.speed < 2
	end
end


function update_anim(dt)
	if current.phase == 'play' then return true end
	for a, b in pairs(anim) do animate(b, dt, a) end
	if not next(anim) then
		return false
	end
	screen.changed = true
	return true
end


function animate_death()
	local laststep = 80
	table.insert(anim, {
		paused = false,
		step = 0,
		laststep = laststep,
		delay = 0,
		speed = .05,
		update = function(self)
			playfield.afterdeath_phase = self.step / self.laststep
			hatch:send('line_transparency', 1 - self.step / self.laststep * .3)
		end
	})
end


function fade_to_level(next_level)
	play_sound('teleport')
	next_level = next_level or game.currentlevel
	-- beforehand, hide cursor
	small_playfield.selectedx, small_playfield.selectedy = nil, nil
	-- remember background
	current.old_background, current.new_background = {}, {}
	for a = 1, 3 do
		current.old_background[a] =
			backgrounds[level_labels[game.currentlevel]].color[a]
		current.new_background[a] =
			backgrounds[level_labels[next_level]].color[a]
	end
	local laststep = 32
	table.insert(anim, {
		paused = false,
		step = 0,
		laststep = laststep,
		after = {
			-- at half, change level
			[16] = function(self)
				setuplevel(next_level)
			end,
			-- after animation, unset
			[laststep] = function(self)
				playfield.fade_phase = nil
				current.background = nil
				current.old_background = nil
				current.new_background = nil
				sounds.teleport:stop()
			end
		},
		delay = 0,
		speed = .08,
		update = function(self)
			if self.step <= self.laststep / 2 then
				playfield.fade_phase = self.step
			else
				playfield.fade_phase = self.laststep - self.step
			end
			local o, n = current.old_background, current.new_background
			current.background = {}
			for a = 1, 3 do
				local d = o[a] - n[a]
				current.background[a] = o[a] - d * self.step / self.laststep
			end
		end
	})
end


function rotate_arrows(x, y, turns)
	play_sound('turn')
	turns = turns or 1
	local laststep = 6
	table.insert(anim, {
		paused = false,
		step = 1,
		laststep = laststep,
		after = {[laststep] = function(self)
			-- after animation, switch shape and redefine arrows
			local pf = playfield[x][y].memory or playfield[x][y]
			if turns % 2 == 1 then
				if pf.shape == 'x' then pf.shape = '+' else pf.shape = 'x' end
				pf.name = get_rotated_name(pf.name)
			end
			pf.arrows = shape_to_arrows(pf.shape)
			pf.center = nil
			playfield[x][y].phase = 0
		end},
		delay = 0,
		speed = .1,
		update = function(self)
			playfield[x][y].phase = self.step / 5 * turns
		end
	})
end


function explode_arrow(x, y, direction, distance, kill, target)
	-- arrows with kill=true show dead player at the end of animation
	play_sound('arrows')
	local laststep = distance * 10 - 5
	table.insert(anim, {
		paused = paused,
		step = 1,
		laststep = laststep,
		after = {[laststep] = function(self)
			playfield[x][y].arrows[direction] = false
			if kill then
				play_sound('dead')
				if not current.showkill then animate_death() end
				current.showkill = true
				-- remove arrows so they don't cause error when reviving memories
				--playfield[playfield.clickedx][playfield.clickedy].arrows = nil
				-- just empty the tile
				playfield[playfield.clickedx][playfield.clickedy] = recognizeink()
			end
			-- yellow arrows are marked with dot here, not earlier,
			-- so that it blends with exploding arrow animation
			if target then
				target.center = 'dot'
				play_sound('tone')
			end
		end},
		delay = 0,
		speed = .02,
		update = function(self)
			playfield[x][y].arrows[direction] = self.step / 10
		end
	})
end


function grow_arrows(x, y)
	-- this animation grows base of regenerating arrows
	-- by changing the phase from -1 to 0
	playfield[x][y].phase = -1
	local laststep = 5
	play_sound('backwards')
	table.insert(anim, {
		paused = false,
		step = 1,
		laststep = laststep,
		delay = 0,
		speed = .1,
		update = function(self)
			playfield[x][y].phase = (self.step - laststep) / laststep
		end
	})
end


function animate_gifts()
	play_sound('open_gifts')
	current.phase = 'opengifts'
	playfield.giftsphase = 0
	local laststep = 10
	table.insert(anim, {
		paused = false,
		step = 1,
		laststep = laststep,
		after = {[laststep] = function(self)
			-- unset ingift for opened gifts in case some additional gifts be created
			inplayfield(function (x, y, p) p.ingift = nil end)
			sounds['open_gifts']:stop()
		end},
		delay = 0,
		speed = .1,
		update = function(self)
			playfield.giftsphase = 1 - (laststep - self.step) / laststep
		end
	})
end


function animate_arrow_gift(x, y)
	playfield[x][y].arrowgiftphase = .4
	local laststep = 10
	table.insert(anim, {
		paused = false,
		step = 1,
		laststep = laststep,
		after = {[laststep] = function(self)
			-- acknowledge arrowgift
			playfield[x][y].arrowgiftphase = nil
			playfield[x][y] = playfield[x][y].arrowgift
			if playfield[x][y].kind == 'gift' then fill_gifts(game.currentlevel) end
		end},
		delay = 0,
		speed = .1,
		update = function(self)
			playfield[x][y].arrowgiftphase = .4 + .6 * self.step / laststep
		end
	})
end


function animate_pyramid()
	-- current.phase will automatically reset to 'play' after the animation
	-- it mustn't be 'play' to get past the check in update_anim
	current.phase = 'rubens_anim'
	-- current.screen must be 'playfield' to get past the check in love.update
	current.screen = 'playfield'
	-- cut out holes in poor_canvas to remove background
	love.graphics.setCanvas(rp.poor_canvas)
	love.graphics.setBlendMode('replace')
	love.graphics.setColor(0,0,0, 0)
	for a = 1, 3 do love.graphics.draw(rp.meshes[a]) end
	love.graphics.setBlendMode('alpha')
	local laststep = 10
	table.insert(anim, {
		paused = false,
		step = 1,
		laststep = laststep,
		after = {[laststep] = function(self)
			rp.meshes = nil -- to display only rich_canvas
		end},
		delay = 0,
		speed = .05,
		update = function(self)
			rp:anim_triangles(1 - self.step / laststep)
		end
	})
end
