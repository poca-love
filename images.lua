--[[
This file is part of POCA - a puzzle game

POCA is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

-- retro style
love.graphics.setDefaultFilter('linear', 'nearest')

images = {
	['0'] = { -- tile
		color = {.3, .3, .3},
		shape = 'floortile',
	},
	['1'] = { -- greenx
		color = {0, 1, 0},
		center = 'plain',
	},
	['2'] = { -- green+
		color = {0, 1, 0},
		center = 'plain',
	},
	['3'] = { -- yellowx
		color = {1, 1, 0},
		center = 'plain',
	},
	['4'] = { -- yellow+
		color = {1, 1, 0},
		center = 'plain',
	},
	['5'] = { -- redx
		color = {1, 0, 0},
		center = 'dot',
	},
	['6'] = { -- red+
		color = {1, 0, 0},
		center = 'dot',
	},
	['7'] = { -- regenex
		color = {0, 1, 0},
		center = 'bmp',
		bmp = love.graphics.newImage('gfx/ankh.png'),
	},
	['8'] = { -- regene+
		color = {0, 1, 0},
		center = 'bmp',
		bmp = love.graphics.newImage('gfx/ankh.png'),
	},
	['9'] = { -- regenex - pressed
		shape = 'floortile',
	},
	A = { -- regene+ - pressed
		shape = 'floortile',
	},
	C = { -- hint
		shape = 'sign',
	},
	D = { -- blackload
		shape = 'hole',
	},
	H = { -- loadredx
		color = {1, 0, 0},
		center = 'dot',
	},
	I = { -- loadred+
		color = {1, 0, 0},
		center = 'dot',
	},
	J = { -- box
		color = {.3, .3, .3},
		shape = 'gift',
	},
	K = { -- genie box
		color = {1, 0, 0},
		shape = 'gift',
	},
	N = { -- wise man
		shape = 'bmp',
		bmp = love.graphics.newImage('gfx/wiseman.png'),
	},
	O = { -- pill
		shape = 'bmp',
		bmp = love.graphics.newImage('gfx/pill.png'),
	},
	P = { -- purple+
		color = {.6, 0, .6},
		center = 'purple',
	},
	Q = { -- purplex
		color = {.6, 0, .6},
		center = 'purple',
	},
	R = { -- killed
		shape = 'bmp',
		bmp = love.graphics.newImage('gfx/dead.png'),
	},
	V = { -- arrow left
		shape = 'left',
	},
	W = { -- arrow up
		shape = 'up',
	},
	X = { -- arrow right
		shape = 'right',
	},
	Y = { -- arrow down
		shape = 'down',
	},
	Z = { -- trinket
		shape = 'trinket',
	},
	a = { -- green+ - memory
		memory = true,
	},
	b = { -- greenx - memory
		memory = true,
	},
	c = { -- loadred+ - memory
		memory = true,
	},
	d = { -- loadredx - memory
		memory = true,
	},
	e = { -- red+ - memory
		memory = true,
	},
	f = { -- redx - memory
		memory = true,
	},
	g = { -- regene+ - pressed - memory
		-- doesn't render, but unused
		memory = true,
	},
	h = { -- regenex - memory
		memory = true,
	},
	i = { -- regenex - pressed - memory
		-- doesn't render, but unused
		memory = true,
	},
	j = { -- yellow+ - memory
		memory = true,
	},
	k = { -- yellowx - memory
		memory = true,
	},
	l = { -- regene+ - memory
		memory = true,
	},
	m = { -- purple+ - memory
		memory = true,
	},
	n = { -- purplex - memory
		memory = true,
	},
	floor_regene_x = {
		bmp = love.graphics.newImage('gfx/floor_regene_x.png'),
	},
	floor_regene_plus = {
		bmp = love.graphics.newImage('gfx/floor_regene_plus.png'),
	},
	cursor = {
		shape = 'cursor',
		color = {.6, .6, 1},
		width = 1,
	},
	cursor_wide = {
		shape = 'cursor',
		color = {.6, .6, 1},
		width = 2,
	},
	pencil_r = {
		shape = 'bmp',
		bmp = love.graphics.newImage('gfx/pencil_r.png'),
	},
	pencil_g = {
		shape = 'bmp',
		bmp = love.graphics.newImage('gfx/pencil_g.png'),
	},
	pencil_y = {
		shape = 'bmp',
		bmp = love.graphics.newImage('gfx/pencil_y.png'),
	},
	dark_object = {
		shape = 'bmp',
		bmp = love.graphics.newImage('gfx/object.png'),
	},
	po = { -- title
		shape = 'bmp',
		bmp = love.graphics.newImage('gfx/po.png'),
	},
	ca = { -- title
		shape = 'bmp',
		bmp = love.graphics.newImage('gfx/ca.png'),
	},
	version = { -- title
		shape = 'bmp',
		bmp = love.graphics.newImage('gfx/version.png'),
	},
	stop = { -- casette
		shape = 'bmp',
		bmp = love.graphics.newImage('gfx/stop.png'),
		scale = 150,
	},
	play = { -- casette
		shape = 'bmp',
		bmp = love.graphics.newImage('gfx/play.png'),
	},
	ffwd = { -- casette
		shape = 'bmp',
		bmp = love.graphics.newImage('gfx/ffwd.png'),
	},
	rewind = { -- casette
		shape = 'bmp',
		bmp = love.graphics.newImage('gfx/rewind.png'),
	},
	eject = { -- casette
		shape = 'bmp',
		bmp = love.graphics.newImage('gfx/eject.png'),
	},
	loop_1 = { -- casette
		shape = 'bmp',
		bmp = love.graphics.newImage('gfx/loop-1.png'),
	},
	loop_all = { -- casette
		shape = 'bmp',
		bmp = love.graphics.newImage('gfx/loop-all.png'),
	},
	loop_none = { -- casette
		shape = 'bmp',
		bmp = love.graphics.newImage('gfx/loop-none.png'),
	},
	cw = { -- rainbow ring, restart
		shape = 'bmp',
		bmp = love.graphics.newImage('gfx/cw.png'),
	},
}

function add_scale(t)
	for k, v in pairs(t) do if type(v) == 'table' and v.bmp then
		-- determine correct scale and offset for bitmaps
		local sizex, sizey = v.bmp:getWidth(), v.bmp:getHeight()
		v.scale = v.scale or math.max(sizex, sizey)
		v.offsetx = v.offsetx or (1 - sizex / v.scale) / 2
		v.offsety = v.offsety or (1 - sizey / v.scale) / 2
	end end
end

add_scale(images)


function draw_floor(x, y, stepped, sleep, shape, scale)
	love.graphics.push()
	love.graphics.scale(screen.tilesize)
	love.graphics.translate(x, y)
	if scale then love.graphics.scale(scale) end
	love.graphics.setColor(images['0'].color, 1)
	love.graphics.rectangle('fill', -.5,-.5, 1,1)
	love.graphics.setColor(1,1,1, .4)
	if stepped then
		love.graphics.polygon('fill', .5,-.5, .5,.5, -.5,.5)
	else
		love.graphics.polygon('fill', -.5,-.5, .5,-.5, -.5,.5)
	end
	love.graphics.setColor(0,0,0, .4)
	if stepped then
		love.graphics.polygon('fill', -.5,-.5, .5,-.5, .5,.5)
	else
		love.graphics.polygon('fill', -.5,-.5, .5,.5, -.5,.5)
	end
	love.graphics.setColor(images['0'].color, 1)
	love.graphics.rectangle('fill', -.45,-.45, .9,.9)
	if not current.symbolic and sleep then
		love.graphics.setColor(1,1,1, 1)
		local img
		if shape == '+' then img = images.floor_regene_plus else img = images.floor_regene_x end
		love.graphics.draw(img.bmp, img.offsetx - .5, img.offsety - .5, 0, 1 / img.scale, 1 / img.scale)
	end
	if shape == 'hole' then
		love.graphics.setColor(0,0,0, 1)
		love.graphics.circle('fill', 0, 0, .3)
		love.graphics.setColor(1,1,1, 1)
	end
	love.graphics.pop()
end


function draw_symbolic_direction(shape, used)
	if used then
		love.graphics.setColor(1,1,1, 1)
	else
		love.graphics.setColor(0,0,0, 1)
	end
	if shape == 'left' then
		love.graphics.line(.5,0, -.5,0)
		love.graphics.line(0,-.5, -.5,0, 0,.5)
	elseif shape == 'right' then
		love.graphics.line(-.5,0, .5,0)
		love.graphics.line(0,-.5, .5,0, 0,.5)
	elseif shape == 'up' then
		love.graphics.line(0,.5, 0,-.5)
		love.graphics.line(-.5,0, 0,-.5, .5,0)
	elseif shape == 'down' then
		love.graphics.line(0,-.5, 0,.5)
		love.graphics.line(-.5,0, 0,.5, .5,0)
	end
	love.graphics.setColor(1,1,1, 1)
end


function draw_image(pftile, x, y, phase, scale, symbolic, symbolic_colored)
	if not phase then phase = 0 end
	function arrow(color, angle, distance, base)
		base = ((base or 0) + 1) / 10
		if angle % (math.pi / 2) > .1 then
			distance = distance * math.sqrt(2)
			-- important when arrows are flying away
			-- initial distance is 0
		end
		local shaft = .5
		if symbolic then
			shaft = shaft / math.cos((angle + math.pi/4) % (math.pi/2) - math.pi/4)
		end
		distance = distance + shaft
		local ox, oy = math.sin(angle) * (distance - shaft), math.cos(angle) * (distance - shaft)
		local ex, ey = math.sin(angle) * distance, math.cos(angle) * distance
		if not symbolic then
			love.graphics.setColor(color)
			love.graphics.polygon('fill', ex, ey,
				ox + math.sin(angle + math.pi / 2) * base, oy + math.cos(angle + math.pi / 2) * base,
				ox - math.sin(angle + math.pi / 2) * base, oy - math.cos(angle + math.pi / 2) * base
			)
		else
			if not symbolic_colored then love.graphics.setColor(0,0,0, 1) end
			love.graphics.line(ox, oy, ex, ey)
		end
	end

	if not pftile.name then return end
	if images[pftile.name].memory then
		draw_image(pftile.memory, x, y, phase, (scale or 1) * .5)
		return
	end
	love.graphics.push()
	love.graphics.setLineWidth(.01)
	love.graphics.scale(screen.tilesize)
	love.graphics.translate(x, y)
	if scale then love.graphics.scale(scale) end
	love.graphics.push()
	local function gifts_aperture()
		local ph = playfield.giftsphase
		love.graphics.rectangle('fill', -.5 * ph, -.5 * ph, 1 * ph, 1 * ph)
	end
	if not symbolic then
		if pftile.ingift then -- draw gift surface without aperture
			love.graphics.stencil(gifts_aperture, 'replace', 1)
			love.graphics.setStencilTest('less', 1)
		end
		if pftile.ingift or images[pftile.name].shape == 'gift' then
			if pftile.ingift == 'devil' or pftile.name == 'K' then
				love.graphics.setColor(images.K.color, 1)
			else
				love.graphics.setColor(images.J.color, 1)
			end
			love.graphics.rectangle('fill', -.5, -.5, 1, 1)
			love.graphics.setColor(1,1,1, .4)
			love.graphics.polygon('fill', -.50,-.50, .50,-.50, -.50,.50)
			love.graphics.setColor(0,0,0, .4)
			love.graphics.polygon('fill', -.50,-.50, .50,.50, -.50,.50)
		end
		if pftile.ingift then -- invert stencil to draw gift contents
			love.graphics.setStencilTest('gequal', 1)
		end
 	else -- symbolic
		if images[pftile.name].shape == 'gift' then
			love.graphics.setColor(0,0,0, 1)
			love.graphics.line(-.5,0, 0,-.5, .5,0, 0,.5, -.5,0)
		end
	end
	if images[pftile.name].color then
		love.graphics.setColor(images[pftile.name].color, 1)
	else
		love.graphics.setColor(1,1,1, 1)
	end
	if pftile.kind == 'regene' then
		for a = 1, 8 do if pftile.arrows[a] then
			arrow(pftile.color or images[pftile.name].color, math.pi / 4 * (a + 1) , pftile.arrows[a], phase) --!!
		end end
	elseif pftile.arrows then
		for a = 1, 8 do if pftile.arrows[a] then
				arrow(pftile.color or images[pftile.name].color, math.pi / 4 * (a + 1 + phase) , pftile.arrows[a], nil)
		end end
	end
	if images[pftile.name].shape == 'sign' then
		love.graphics.setColor(.8,.8,.8, 1)
		if os.time() % 2 == 0 then -- moving arrow
			love.graphics.polygon('fill', -.06, -.25, 0, -.39, .06, -.25)
			love.graphics.rectangle('fill', -.02, -.25, .04, .15)
		else
			love.graphics.polygon('fill', -.06, -.27, 0, -.41, .06, -.27)
			love.graphics.rectangle('fill', -.02, -.27, .04, .15)
		end
		love.graphics.printf(levels[game.currentlevel].sign,
			-.5, -.05, screen.tilesize, 'center', nil, 1 / screen.tilesize
		)
	end
	if images[pftile.name].shape == 'bmp' then
		love.graphics.setColor(1,1,1, 1)
		love.graphics.draw(images[pftile.name].bmp,
			images[pftile.name].offsetx - .5, images[pftile.name].offsety - .5,
			0, 1 / images[pftile.name].scale, 1 / images[pftile.name].scale
		)
	end
	if images[pftile.name].shape == 'hole' then
		-- actual hole drawn in draw_floor()
		love.graphics.setColor(1,1,1, 1)
		if screen.defaultfont then
			love.graphics.print(pretty_name(playfield.skiplevel, true),
				-.11, -.11,
				nil, 1 / screen.tilesize
			)
		end
	end
	if images[pftile.name].shape == 'trinket' then
		local trinket = levels[game.currentlevel].trinket
		local t
		if symbolic then t = images.dark_object else t = trinkets[trinket] end
		love.graphics.setColor(1,1,1, 1)
		love.graphics.draw(t.bmp,
			t.offsetx -.50, t.offsety -.50,
			0, 1 / t.scale, 1 / t.scale
		)
	end
	if images[pftile.name].shape == 'cursor' then
		local w = images[pftile.name].width
		local r, g, b, a = love.graphics.getColor()
		love.graphics.setColor(r, g, b, .1)
		love.graphics.rectangle('fill', -.5, -.5, w, 1)
		love.graphics.setColor(r, g, b, .8)
		love.graphics.rectangle('line', -.5, -.5, w, 1)
		love.graphics.rectangle('line', -.4, -.4, w - .2, .8)
	end
	if symbolic then
		draw_symbolic_direction(images[pftile.name].shape, pftile.used)
		if pftile.contains and pftile.used then
			draw_symbolic_direction(images[pftile.contains.name].shape, pftile.used)
		end
	end
	love.graphics.pop()
	if not symbolic and images[pftile.name].center then
		--center == 'plain'
		love.graphics.setColor(.5,.5,.5, 1)
		if pftile.devil then love.graphics.setColor(.9,.3,.3, 1) end
		love.graphics.rectangle('fill', -.15,-.15, .3,.3)
		if pftile.center == 'dot' or images[pftile.name].center == 'dot' then
			love.graphics.setColor(.1,.1,.1, 1)
			love.graphics.circle('fill', 0,0, .1)
		end
		if images[pftile.name].center == 'purple' then
			love.graphics.setColor(.1,.1,.1, 1)
			if current.temppurple then
				love.graphics.rectangle('fill', -.05,-.05, .1, .1)
			else
				love.graphics.rectangle('line', -.05,-.05, .1, .1)
			end
		end
		if images[pftile.name].center == 'bmp' then
			love.graphics.setColor(1,1,1, 1)
			love.graphics.draw(images[pftile.name].bmp, images[pftile.name].offsetx -.50, images[pftile.name].offsety -.50, 0, 1/images[pftile.name].scale, 1/images[pftile.name].scale)
		end
	end
	if pftile.ingift and not symbolic then -- darken gifts content
		love.graphics.setColor(0,0,0, .9 - playfield.giftsphase)
		gifts_aperture()
	end
	love.graphics.setStencilTest()
	love.graphics.pop()
	love.graphics.setColor(1, 1, 1, 1)
	-- small images in loadred
	if pftile.name ~= 'R' and pftile.arrowgift then
		if pftile.arrowgift.shape == 'hole' then
			-- grow temporary hole before drawing it on the bottom layer
			draw_floor(x, y, false, false, 'hole',
				(pftile.arrowgiftphase or .4) * (scale or 1)
			)
		end
		draw_image(pftile.arrowgift, x, y, 0, pftile.arrowgiftphase or (scale or 1) * .4)
	end
end


function draw_initial_playfield()
	-- layer 1: background
	inplayfield(function (x, y, p)
		draw_floor(x - .5, y - .5, false, p.sleep, p.shape)
	end)
	if game.shuffled then -- draw wise man bg
		love.graphics.setBlendMode('screen')
		draw_image({name = 'N'}, 2, 2, nil, 4)
		love.graphics.setBlendMode('alpha')
	end
	-- layer 2: all images and arrows
	inplayfield(function (x, y, p)
		local q = get_initial_tile(game.currentlevel, x, y)
		if q.name ~= '0' or q.shape or q.ingift then
			draw_image(q, x - .5, y - .5, q.phase, nil, current.symbolic)
		end
	end)
end


function draw_playfield()
	local selected_playfield = playfield
	-- layer 1: background
	if not (big_playfield.selectedx and level_labels[game.currentlevel] == 'Secrets') then
		inplayfield(function (x, y, p)
			local stepped = small_playfield.clickedx == x and small_playfield.clickedy == y
			draw_floor(x - .5, y - .5, stepped, p.sleep, p.shape)
		end)
		if game.shuffled then -- draw wise man bg
			love.graphics.setBlendMode('screen')
			draw_image({name = 'N'}, 2, 2, nil, 4)
			love.graphics.setBlendMode('alpha')
		end
	else
		local stepped = big_playfield.clickedx == big_playfield.selectedx and big_playfield.clickedy == big_playfield.selectedy
		local bgtile = big_playfield[big_playfield.selectedx][big_playfield.selectedy]
		draw_floor(2, 2, stepped, nil, nil, 4)
		draw_image(bgtile, 2, 2, nil, 4)
	end
	playfield = small_playfield
	-- layer 2: all images and arrows
	inplayfield(function (x, y, p)
		if p.name ~= '0' or p.shape or p.ingift then
			draw_image(p, x - .5, y - .5, p.phase, nil, current.symbolic)
		end
	end)
	playfield = selected_playfield
	-- layer 3: dead player
	if current.showkill and playfield.clickedx then
		draw_image(recognizeink('R'), playfield.clickedx - .5, playfield.clickedy - .5)
	end
	playfield = selected_playfield
end


function draw_fade()
	-- covers progressively bigger area of the playfield
	-- each time, stencil value is increased by 1
	love.graphics.push()
	love.graphics.translate(screen.playfieldx, screen.playfieldy)
	love.graphics.scale(screen.tilesize)
	local order = {0, 1, 2, 3, 7, 11, 15, 14, 13, 12, 8, 4, 5, 6, 10, 9 }
	for a = 1, 16 do for b = 1, a do
		local x = order[b] % 4
		local y = math.floor(order[b] / 4)
		love.graphics.rectangle('fill', x, y, 1, 1)
	end end
	love.graphics.pop()
end


-- draw spots of paint made when using pencil or matches
function draw_paint()
	if not level_colors then return end
	love.graphics.push()
	love.graphics.translate(screen.playfieldx, screen.playfieldy)
	love.graphics.scale(screen.tilesize)
	love.graphics.draw(level_colors)
	love.graphics.pop()
end


function draw_shuffled()
	love.graphics.push()
	inplayfield(function(x, y, p)
		love.graphics.draw(screen.pfcanvas, screen.pfquads[x][y],
		screen.playfieldx + (game.shuffled[x][y].x - 1) * screen.tilesize,
		screen.playfieldy + (game.shuffled[x][y].y - 1) * screen.tilesize
		)
	end)
	love.graphics.pop()
end


function draw_restart_button(p)
	if p < .7 then return end
	love.graphics.push()
	love.graphics.translate(
		screen.playfieldx + screen.playfieldsize / 2,
		screen.playfieldy + screen.playfieldsize / 2
	)
	local size = (p - .7) * 3.33
	love.graphics.setColor(.6, .6, .6, .5)
	love.graphics.polygon('fill',
		0, screen.tilesize * size / 2,
		- screen.tilesize * size / 2, 0,
		0, - screen.tilesize * size / 2,
		screen.tilesize * size / 2, 0
	)
	draw_image({name = 'cw'}, 0, 0, nil, .55 * size)
	love.graphics.setColor(1, 1, 1)
	love.graphics.pop()
end


function draw_foreground()
	if game.shuffled then
		draw_shuffled()
	else
		-- draw straight
		if playfield.fade_phase then
			local phase = 16 - playfield.fade_phase
			love.graphics.stencil(draw_fade, 'increment')
			-- reveals more or less of the underlying playfield
			-- according to pre-drawn stencil image
			love.graphics.setStencilTest('less', phase)
		end
		if playfield.afterdeath_phase then
			love.graphics.setShader(hatch)
		end
		love.graphics.draw(screen.pfcanvas, screen.playfieldx, screen.playfieldy)
		love.graphics.setShader()
		if playfield.afterdeath_phase then
			draw_restart_button(playfield.afterdeath_phase)
		end
	end
	draw_paint()
	-- cursor
	if small_playfield.selectedx then
		if (small_playfield.selectedx > 1) and
			(small_playfield.selectedx < 4) and
			(small_playfield.selectedy < 1)
		then
			draw_image({name = 'cursor_wide'},
			2 - .5 + screen.playfieldx / screen.tilesize,
			small_playfield.selectedy - .5 + screen.playfieldy / screen.tilesize
			)
		else
			draw_image({name = 'cursor'},
			small_playfield.selectedx - .5 + screen.playfieldx / screen.tilesize,
			small_playfield.selectedy - .5 + screen.playfieldy / screen.tilesize
			)
		end
	end
end


function draw_minimap() -- in Secrets
	if level_labels[game.currentlevel] ~= 'Secrets' then return end
	love.graphics.setColor(.5,.5,.5, 1)
	love.graphics.draw(trinkets.frame.bmp,
		- screen.tilesize * 1.5 / 10 , - screen.tilesize * 1.5 / 10,
		nil, screen.tilesize / trinkets.frame.scale * 1.1
	)
	love.graphics.push()
	love.graphics.scale(screen.tilesize / 5)
	for a = 0, 3 do for b = 0, 3 do
		if not game.seen_levels[level_labels['Secrets'] + a + b * 4] then
			love.graphics.setColor(0, 0, 0, .5)
			love.graphics.rectangle('fill', a, b, 1, 1)
		else
			local t = big_playfield[a + 1][b + 1]
			draw_image(t, (a + .5) / screen.tilesize, (b + .5) / screen.tilesize, nil, 1 / screen.tilesize, true, true)
		end
	end end
	love.graphics.setColor(1, 1, 1)
	love.graphics.rectangle('line',
		big_playfield.selectedx - 1, big_playfield.selectedy - 1, 1, 1
	)
	love.graphics.pop()
end


-- draw map or square at desired location
function show_minimap(solved, all) -- in Secrets
	local x = screen.playfieldx + 3 * screen.tilesize
	local y = screen.playfieldy - screen.tilesize
	local d = screen.tilesize / 5
	if all then
		love.graphics.setColor(1,1,1, 1)
		love.graphics.draw(screen.minimap, x + d / 2, y + d / 2)
	else
		love.graphics.setColor(.5,.5,.5, 1)
		love.graphics.rectangle('line', x + d / 2, y + d / 2, d * 4, d * 4)
	end
	love.graphics.setColor(1,1,1, 1)
	if solved then
		love.graphics.rectangle('line', x + d / 2, y + d / 2, d * 4, d * 4)
	end
end


function draw_pencil() -- in Secrets or when painting
	local y = screen.playfieldy - screen.tilesize
	local image = trinkets.pencil
	if playfield.paint_mode == 1 then
		image = images.pencil_g
	elseif playfield.paint_mode == 2 then
		image = images.pencil_r
	elseif playfield.paint_mode == 3 then
		image = images.pencil_y
	end
	love.graphics.draw(image.bmp,
		screen.playfieldx + screen.tilesize * .15, y  + screen.tilesize * .15,
		nil, screen.tilesize / 70
	)
	love.graphics.setColor(1,1,1, 1)
end


function draw_pencil_stop() -- when painting
	local st = screen.tilesize / 4
	local x = screen.playfieldx + 14 * st
	local y = screen.playfieldy - 2 * st
	love.graphics.setColor(.5,.5,.5, 1)
	love.graphics.polygon('fill',
		x - st / 1.5, y,
		x, y + st,
		x, y + st / 2
	)
	love.graphics.polygon('fill',
		x, y + st,
		x, y + st / 2,
		x + st / 1.5 * 2, y - st
	)
	love.graphics.setColor(1,1,1, 1)
end


function draw_osd()
	-- level name with arrows for traversing levels
	-- or pencil and minimap in Secrets
	local y = screen.playfieldy - screen.tilesize
	local offsety = (screen.tilesize - screen.defaultfont:getHeight()) / 2
	love.graphics.setColor(1, 1, 1)
	-- level name
	love.graphics.printf(pretty_name(game.currentlevel), screen.playfieldx, y + offsety, screen.playfieldsize, 'center')
	-- arrows for traversing levels
	if not (current.symbolic or current.painting) then
		local enabled
		if traversable(-1) then enabled = 'fill' else enabled = 'line' end
		love.graphics.polygon(enabled,
			screen.playfieldx + screen.tilesize * .35, y + screen.tilesize / 2,
			screen.playfieldx + screen.tilesize * .6, y + screen.tilesize * .35,
			screen.playfieldx + screen.tilesize * .6, y + screen.tilesize * .65
		)
		if traversable(1) then enabled = 'fill' else enabled = 'line' end
		love.graphics.polygon(enabled,
			screen.playfieldsize + screen.playfieldx - screen.tilesize * .35, y + screen.tilesize / 2,
			screen.playfieldsize + screen.playfieldx - screen.tilesize * .6, y + screen.tilesize * .35,
			screen.playfieldsize + screen.playfieldx - screen.tilesize * .6, y + screen.tilesize * .65
		)
	end
	if current.painting then
		draw_pencil()
		draw_pencil_stop()
	end
	if level_labels[game.currentlevel] == 'Secrets' then
		if has_trinket('pencil') then draw_pencil() end
		show_minimap(game.solved_levels[game.currentlevel], has_trinket('frame'))
	end
end

