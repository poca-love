--[[
This file is part of POCA - a puzzle game

POCA is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

function love.conf(t)
   t.window.title = "Poca"
   t.window.icon = "gfx/icon.png"
   t.window.width = 600
   t.window.height = 600
   t.window.fullscreen = false
   t.window.resizable = true -- should be false for android portrait
   t.identity = "poca"
   t.version = "11.3" -- love version
   t.externalstorage = false
   t.modules.joystick = false
   t.modules.physics = false
   t.accelerometerjoystick = false
   t.modules.audio = true
   t.modules.math = true
   t.modules.sound = true
   t.modules.system = true
   t.modules.thread = false
   t.window.vsync = 1
end
