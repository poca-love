--[[
This file is part of POCA - a puzzle game

POCA is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


function serialize(o, data) -- adapted from "Programming in Lua"
	data = data or ''
	if type(o) == 'number' then
		data = data .. o
	elseif type(o) == 'string' then
		data = data .. string.format('%q', o)
	elseif type(o) == 'boolean' then
		if o then data = data .. 'true' else data = data .. 'false' end
	elseif type(o) == 'table' then
		data = data .. '{\n'
	for k,v in pairs(o) do
		if type(k) == 'number' then
			data = data .. '  [' .. k .. '] = '
		else
			data = data .. '  ' .. k .. ' = '
		end
		data = data .. serialize(v)
		data = data .. ',\n'
	end
		data = data .. '}\n'
	else
		error('cannot serialize a ' .. type(o))
	end
	return data
end


function save(t, f)
	local ok, err = love.filesystem.write(f, 'return ' .. serialize(t))
	if not ok then error(err) end
end


function load_settings()
	local s, err = love.filesystem.load('settings')
	if not err then -- preserve default settings if not saved
		for k, v in pairs(s()) do settings[k] = v end
	end
end


function load_game(f)
	init()
	local saved_game, err = love.filesystem.load(f)
	if not err then game = saved_game() end
	-- compatibility with older saved games
	for a = 1, #game.wise_man_has do
		if game.wise_man_has[a] == 'advice_geany' then
			game.wise_man_has[a] = 'advice_genie'
		end
	end
	setuplevel(game.currentlevel)
end


function read_slots()
	local slots = {}
	for a = 1, 4 do
		local saved_game, err = love.filesystem.load('game' .. a)
		local tmpgame
		if not err then
			tmpgame = saved_game()
			if tmpgame then
				slots[a] = pretty_name(tmpgame.currentlevel)
			else
				slots[a] = 'unreadable'
			end
		end
	end
	return slots
end
