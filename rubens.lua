--[[
Ruben's Pyramid - a minigame is a part of POCA - a puzzle game

POCA is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

require('special')

rp = {


hijacked_love_functions = {
	'draw', 'resize',
	'mousepressed',
	'keypressed'
},


draw = function() -- hijacked function
	-- pyramid
	love.graphics.setColor(1,1,1, 1)
	if not rp.meshes then
		love.graphics.draw(rp.rich_canvas, rp.bigx, rp. bigy)
	else -- draw rotating triangles over canvas with these traingles missing
		love.graphics.draw(rp.poor_canvas, rp.bigx, rp. bigy)
		for a = 1, 3 do
			love.graphics.draw(rp.meshes[a], rp.bigx, rp.bigy)
		end
		-- restore selected triangle to hide weird shapes that appear when rotating
		local p = rp.pyramid
		local x = rp.bigx + p[p.selectedr][p.selectedt].x
		local y = rp.bigy + p[p.selectedr][p.selectedt].y
		rp.draw_triangle(x, y, p[p.selectedr][p.selectedt].color)
	end
	--cursor
	rp:draw_pyramid_cursor()
	-- buttons and label
	rp.buttons:draw()
end,


-- height of equilateral triangle
h = function(a)
	return a * 1.732 / 2
end,


draw_button_image = function(name, selected, disabled)
	local images = {
		more = function()
			love.graphics.line(.5,1, .5,0)
			love.graphics.line(0,.5, 1,.5)
		end,
		less = function()
			love.graphics.line(0,.5, 1,.5)
		end,
		x = function()
			love.graphics.line(0,0, 1,1)
			love.graphics.line(1,0, 0,1)
		end,
		shuffle = function()
			love.graphics.polygon('line', 0,.1, .5,rp.h(1)+.1, 1,.1)
		end,
		fast = function()
			if rp.no_anim then love.graphics.setColor(1,1,1, .4) end
			love.graphics.rectangle('line', 0,0, .2,.2)
			love.graphics.rectangle('line', 0,.4, .2,.2)
			love.graphics.rectangle('line', 0,.8, .2,.2)
		end,
	}

	-- draw button background
	love.graphics.setColor(.4,.4,.4, 1)
	if selected then
		love.graphics.rectangle('fill', 0, 0, 1, 1, .1)
	else
		love.graphics.rectangle('line', 0, 0, 1, 1, .1)
	end
	love.graphics.setLineWidth(.03)
	love.graphics.rectangle('line', 0, 0, 1, 1, .1)
	-- draw button image
	love.graphics.push()
	love.graphics.translate(.1, .1)
	love.graphics.scale(.8)
	if disabled then
		love.graphics.setColor(1,1,1, .4)
	else
		love.graphics.setColor(1,1,1, 1)
	end
	images[name]()
	love.graphics.pop()
end,


buttons = {
	{
		x, y, image = 'less',
		onclick = function()
			rp.size = math.max(rp.min_size, rp.size - 1)
			rp:init()
		end
	},
	{
		x, y, image = 'more',
		onclick = function()
			rp.size = math.min(rp.max_size, rp.size + 1)
			rp:init()
		end
	},
	{
		x, y, image = 'fast',
		onclick = function()
			rp.no_anim = not rp.no_anim
		end
	},
	{
		x, y, image = 'shuffle',
		onclick = function()
			rp:shuffle()
		end
	},
	{
		x, y, image = 'x',
		onclick = function()
			rp:quit()
		end
	},
	draw = function(self)
		for a = 1, #self do
			love.graphics.push()
			love.graphics.translate(self[a].x, self[a].y)
			love.graphics.scale(rp.buttonsize)
			rp.draw_button_image(self[a].image, self.selected == a, self[a].disabled)
			love.graphics.pop()
		end
	end,
	selected = nil,
},


-- 10 random clicks
shuffle = function()
	local function last(psize)
		return psize * (psize + 1) / 2
	end
	for a = 1, 10 do
		local rnd = math.random(last(rp.size))
		for row = 1, rp.size do
			if last(row) >= rnd then
				rp:pyramid_click(row, rnd - last(row - 1), true) -- no animation
				break
			end
		end
	end
	-- remove last click indicator
	rp.pyramid.selectedr, rp.pyramid.selectedt = nil, nil
	-- remove any meshes
	rp.meshes = false
end,


resize = function() -- hijacked function
	if current.phase == 'rubens_anim' then return end
	resize_common()
	if screen.h < rp.h(screen.w) then
		rp.margin = screen.h / 10
		rp.bigh = screen.h - rp.margin
		rp.bigw = rp.bigh * 1.1547
	else
		rp.margin = screen.w / 10
		rp.bigw = screen.w - rp.margin
		rp.bigh = rp.h(screen.w - rp.margin)
	end
	rp.bigx = (screen.w - rp.bigw) / 2
	rp.bigy = (screen.h - rp.bigh) / 2
	rp.buttonsize = rp.bigw / 7
	local buttony = rp.bigy / 2
	local buttonx = {}
	buttonx[2] = rp.bigx / 2
	buttonx[3] = buttonx[2] + rp.buttonsize + rp.margin / 3
	buttonx[5] = screen.w - rp.buttonsize - rp.bigx / 2
	buttonx[4] = buttonx[5] - rp.buttonsize - rp.margin / 3
	for a = 2, #buttonx do
		rp.buttons[a].x, rp.buttons[a].y = buttonx[a], buttony
	end
	rp.buttons[1].x = buttonx[2]
	rp.buttons[1].y = buttony + rp.buttonsize + rp.margin / 3
	rp.labelwidth = rp.buttonsize
	rp.smallw = rp.bigw / rp.size
	rp.smallh = rp.h(rp.smallw)
	rp.inpyramid(rp.pyramid, function(p, row, tri)
		p[row][tri].x = rp.bigw / 2 + (tri - 1 - row / 2) * rp.smallw
		p[row][tri].y = (row - 1) * rp.smallh
	end)
	rp:update_canvases()
end,


update_canvases = function(self)
	if not self.poor_canvas then
		self.poor_canvas = love.graphics.newCanvas(self.bigw, self.bigh)
		-- without mirrored triangles
	end
	if not self.rich_canvas then
		self.rich_canvas = love.graphics.newCanvas(self.bigw, self.bigh)
		-- with mirrored triangles
	end
	love.graphics.setCanvas(self.poor_canvas)
		self:draw_pyramid(true) -- exclude mirrored triangles
	love.graphics.setCanvas(self.rich_canvas)
		self:draw_pyramid()
	love.graphics.setCanvas()
end,


pyramid_click = function(self, r, t, no_anim)
	local function mirror(p, r, t)
		for row = r + 1, rp.size do -- skip first
			for tri = t, t + math.floor((row - r) / 2) do
				-- (row - r - 1) to exclude central triangles which go nowhere
				-- but they need to be marked as mirrored nevertheless
				local tempcolor = p[row][tri].color
				p[row][tri].color = p[row][2 * t + row - r - tri].color
				p[row][2 * t + row - r - tri].color = tempcolor
				p[row][tri].mirrored = true
				p[row][2 * t + row - r - tri].mirrored = true
			end
		end
	end
	self:pyramid_clear_mirrored()
	self.pyramid.selectedr = r
	self.pyramid.selectedt = t
	self:init_meshes()
	for a = 1, 3 do
		mirror(self.pyramid, self.pyramid.selectedr, self.pyramid.selectedt)
		self.pyramid = rp.rotate_pyramid(self.pyramid)
	end
	self:update_canvases()
	if not (no_anim or self.no_anim) then animate_pyramid() end
end,


pyramid_clear_mirrored = function(self)
	self.inpyramid(self.pyramid, function(p, row, tri)
		p[row][tri].mirrored = nil
	end)
end,


mousepressed = function(mx, my, button) -- hijacked function
	if current.phase == 'rubens_anim' then return end
	rp.pyramid.selectedr = nil
	rp.pyramid.selectedt = nil
	rp.buttons.selected = nil
	-- check for button clicks
	for a = 1, #rp.buttons do
		if
			isin(mx, my, rp.buttons[a].x, rp.buttons[a].y, rp.buttonsize, rp.buttonsize) and
			not rp.buttons[a].disabled
		then
			rp.buttons[a].onclick()
			return
		end
	end
	-- check for pyramid clicks
	rp.inpyramid(rp.pyramid, function(p, r, t)
		if isin(mx - rp.bigx, my - rp.bigy, p[r][t].x, p[r][t].y, rp.smallw, rp.smallh) then
			rp:pyramid_click(r,t)
			return
		end
	end)

end,


cursor_move = function(dir)
	-- default values
	if not rp.pyramid.selectedr then
		rp.pyramid.selectedr = rp.pyramid.selectedr or 1
		rp.pyramid.selectedt = rp.pyramid.selectedt or 1
	elseif dir == 'up' then
		-- jump to third button from the tip of the pyramid
		if rp.pyramid.selectedr == 1 then
			rp.pyramid.selectedr = 0
			rp.pyramid.selectedt = 3
		-- special case: move to another arrow button
		elseif rp.pyramid.selectedr == 0 and rp.pyramid.selectedt == 1 then
			rp.pyramid.selectedt = 2
		-- shound't do anything at 0th row (doesn't move to -1 and left)
		else
			rp.pyramid.selectedr = rp.pyramid.selectedr - 1
			if
				(rp.pyramid.selectedr % 2) == 1 and
				rp.pyramid.selectedr > 1
			then
				rp.pyramid.selectedt = rp.pyramid.selectedt - 1
			end
		end
	elseif dir == 'down' then
		if rp.pyramid.selectedr == 0 and rp.pyramid.selectedt == 2 then
			-- special case: move to another arrow button
			rp.pyramid.selectedt = 1
		else
			-- shound't do anything at last row
			rp.pyramid.selectedr = rp.pyramid.selectedr + 1
			if
				(rp.pyramid.selectedr % 2) == 0 and
				rp.pyramid.selectedr <= rp.size
			then
				rp.pyramid.selectedt = rp.pyramid.selectedt + 1
			end
		end
	elseif dir == 'left' then
		--jump to buttons from 1
		if rp.pyramid.selectedr == 1 then
			rp.pyramid.selectedr = 0
			rp.pyramid.selectedt = 3
		else
			rp.pyramid.selectedt = rp.pyramid.selectedt - 1
		end
	elseif dir == 'right' then
		--jump to buttons from 1
		if rp.pyramid.selectedr == 1 then
			rp.pyramid.selectedr = 0
			rp.pyramid.selectedt = 4
		else
			rp.pyramid.selectedt = rp.pyramid.selectedt + 1
		end
	end
	rp.pyramid.selectedr = math.max(0, rp.pyramid.selectedr)
	rp.pyramid.selectedr = math.min(rp.size, rp.pyramid.selectedr)
	rp.pyramid.selectedt = math.max(1, rp.pyramid.selectedt)
	if rp.pyramid.selectedr > 0 then
		rp.pyramid.selectedt = math.min(rp.pyramid.selectedr, rp.pyramid.selectedt)
		rp.buttons.selected = nil
	else -- buttons
		rp.pyramid.selectedt = math.min(#rp.buttons, rp.pyramid.selectedt)
		rp.buttons.selected = rp.pyramid.selectedt
	end
end,


keypressed = function(k) -- hijacked function
	if current.phase == 'rubens_anim' then return end
	if k == 'q' then
		love.event.push('quit')
	elseif k == 'escape' then
		rp:quit()
	elseif k == 'up' or k == 'down' or  k == 'left' or k == 'right' then
		rp.cursor_move(k)
	elseif k == 'return' and rp.pyramid.selectedr then
		if rp.pyramid.selectedr > 0 then
			rp:pyramid_click(rp.pyramid.selectedr, rp.pyramid.selectedt)
		else
			local clicked_button = rp.pyramid.selectedt
			if not rp.buttons[rp.pyramid.selectedt].disabled then
				rp.buttons[rp.pyramid.selectedt].onclick()
			end
			-- preserve selection
			rp.pyramid.selectedr = 0
			rp.pyramid.selectedt = clicked_button
		end
	end
end,


-- init or go through pyramid
inpyramid = function(p, f, lastrow)
	for row = 1, lastrow or p.size do
		p[row] = p[row] or {}
		for triangle = 1, row do
			p[row][triangle] = p[row][triangle] or {color = 0}
			if f then f(p, row, triangle) end
		end
	end
end,


rotate_pyramid = function(p)
	local newp = {size = p.size}
	rp.inpyramid(newp, function(np, r, t)
		np[r][t].x, np[r][t].y = p[r][t].x, p[r][t].y
	end) -- just init and copy coordinates
	rp.inpyramid(newp, function(np, r, t)
		np[np.size - t + 1][r - t + 1].color = p[r][t].color
		np[np.size - t + 1][r - t + 1].mirrored = p[r][t].mirrored
		-- preserve selection
		if p.selectedr == r and p.selectedt == t then
			np.selectedr = np.size - t + 1
			np.selectedt = r - t + 1
		end
	end)
	return newp
end,


init_pyramid = function(size)
	-- init white and paint pyramid top red, then rotate...
	local p = {size = size}
	rp.inpyramid(p) --just init
	local function paint_top(color, height)
		rp.inpyramid(p, function(p, row, triangle)
				p[row][triangle].color = color
		end, height) -- paint until here
	end
	for a = 1, 3 do
		paint_top(a, size / 2)
		p = rp.rotate_pyramid(p)
	end
	return p
end,


-- initialize big triangles for animation
init_meshes = function(self)
	if self.no_anim then return end
	local p = self.pyramid
	if not (p.selectedr and p.selectedr > 0) then
		self.meshes = nil
		return
	end
	local v = {}
	local sr = p.selectedr
	local st = p.selectedt
	-- bottom triangle
	local tx1 = p[sr][st].x + self.smallw / 2
	local ty1 = p[sr][st].y
	local bottomy = p[self.size][st].y + self.smallh
	local dx1 = tx1 - p[self.size][st].x
	v[1] = {
		tx1, ty1,   tx1 - dx1, bottomy,   tx1 + dx1, bottomy
	}
	-- left triangle
	local tx2 = p[sr][st].x + self.smallw
	local ty2 = p[sr][st].y + self.smallh
	local dx2 = tx2 - p[sr][1].x
	v[2] = {
		tx2, ty2,   p[sr][1].x, ty2,   tx2 - dx2 / 2, ty2 - self.h(dx2)
	}
	-- right triangle
	local tx3 = p[sr][st].x
	local dx3 = p[sr][sr].x - p[sr][st].x + self.smallw
	v[3] = {
		tx3, ty2,   tx3 + dx3, ty2,   tx3 + dx3 / 2, ty2 - self.h(dx3)
	}
	self.meshes = {}
	for a = 1, 3 do
		self.meshes[a] = love.graphics.newMesh({
			{ v[a][1], v[a][2], v[a][1] / self.bigw, v[a][2] / self.bigh },
			{ v[a][3], v[a][4], v[a][3] / self.bigw, v[a][4] / self.bigh },
			{ v[a][5], v[a][6], v[a][5] / self.bigw, v[a][6]  / self.bigh }
		})
		self.meshes[a]:setTexture(self.rich_canvas) -- sample triangles from the whole picture
	end
	self.vertices = v
end,


-- exchange places of two last vertices
anim_triangles = function(self, phase)
	-- sometimes meshes get unset at the last step of animation and this
	-- needs to be checked to prevent crashes
	if not self.meshes then return end
	local v = self.vertices
	for a = 1, 3 do
		local dx = (v[a][5] - v[a][3]) * phase
		local dy = (v[a][6] - v[a][4]) * phase
		self.meshes[a]:setVertices({
				{ v[a][3] + dx, v[a][4] + dy, v[a][3] / self.bigw, v[a][4] / self.bigh },
				{ v[a][5] - dx, v[a][6] - dy, v[a][5] / self.bigw, v[a][6] / self.bigh }
		}, 2)
	end
end,


draw_triangle = function(x, y, color) -- upper left
	if color == 0 then
		love.graphics.setColor(.9,.9,.9, 1)
	elseif color == 1 then
		love.graphics.setColor(.9,.1,.1, 1)
	elseif color == 2 then
		love.graphics.setColor(.1,.9,.1, 1)
	elseif color == 3 then
		love.graphics.setColor(.1,.1,.9, 1)
	end
	love.graphics.polygon('fill',
		x, y + rp.smallh,
		x + rp.smallw / 2, y,
		x + rp.smallw, y + rp.smallh
	)
	love.graphics.setColor(1,1,1, 1)
end,


-- the pyramid without some triangles will be drawn on poor_canvas.
-- although the poor_canvas will be cut out before animation,
-- to remove background, it's still better to exclude these triangles,
-- because canvas-to meshes mapping is not perfect
draw_pyramid = function(self, exclude_mirrored)
	-- pyramid background
	love.graphics.setColor(.2,.2,.2, 1)
	love.graphics.polygon('fill',
		0, 0 + rp.bigh,
		0 + rp.bigw, 0 + rp.bigh,
		0 + rp.bigw / 2, 0
	)
	local p = self.pyramid
	rp.inpyramid(p, function(p, r, t)
		if not (exclude_mirrored and p[r][t].mirrored) then
			rp.draw_triangle(p[r][t].x, p[r][t].y, p[r][t].color)
		end
	end)
end,


draw_pyramid_cursor = function(self)
	local p = self.pyramid
	local r = p.selectedr
	local t = p.selectedt
	if r and r > 0 then
		love.graphics.setColor(0,0,0, .5)
		love.graphics.circle('fill',
			rp.bigx + p[r][t].x + rp.smallw / 2, rp.bigy + p[r][t].y + rp.smallh * 2 / 3,
			rp.smallh / 3
		)
	end
end,


-- called when changing the height
init = function(self)
	self.pyramid = self.init_pyramid(self.size)
	self.buttons[2].disabled = self.size >= self.max_size
	self.buttons[1].disabled = self.size <= self.min_size
	self.resize()
end,


main = function(self)
	self.old_speed = settings.speed
	if settings.speed == 2 then
		settings.speed = 0
		self.no_anim = true
	end
	hijack(self)
	self.max_size = 20
	self.min_size = 4
	self.size = 4
	self:init()
end,


quit = function(self)
	release(self)
	love.resize()
	change_screen('menu_objects')
	settings.speed = self.old_speed
end,


} -- end of rp

