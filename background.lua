--[[
This file is part of POCA - a puzzle game

POCA is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


backgrounds = {
	Apartment = {
		color = {.1, .2, .3},
		music = 'opening'
	},
	Block = {
		color = {.3, .2, 0},
		music = 'street'
	},
	City = {
		color = {.2, .1, .2},
		music = 'city'
	},
	Dig = {
		color = {0, .3, 0},
		music = 'graveyard'
	},
	Elevation = {
		color = {.1, .3, .3},
		music = 'windmills'
	},
	Faydom = {
		color = {.6, .3, .3},
		music = 'path'
	},
	Genie = {
		color = {.6, .4, .3},
		music = 'frula'
	},
	Hive = {
		color = {.7, .6, .3},
		music = 'amnesia'
	},
	Island = {
		color = {.3, .6, .6},
		music = 'sacred_ground'
	},
	Jail = {
		color = {.3, .3, .3},
		music = 'jail'
	},
	Knockout = {
		color = {.3, .7, .5},
		music = 'beta'
	},
	Lighthouse = {
		color = {.3, .5, .5},
		music = 'cliffs'
	},
	Monastery = {
		color = {.1, 0, 0},
		music = 'bells'
	},
	Narrow = {
		color = {.4, .3, .2},
		music = 'walk'
	},
	Overcharge = {
		color = {.7, .1, .5},
		music = 'papian'
	},
	Purple = {
		color = {.6, .1, .6},
		music = 'loop'
	},
	Quicksand = {
		color = {.7, .6, .2},
		music = 'shakes'
	},
	Remorse = {
		color = {.1, .2, .6},
		music = 'remorse'
	},
	Secrets = {
		color = {0, 0, 0},
		music = 'cave_of_secrets'
	},

}


sounds = {}


function load_sounds()
	for name, volume in pairs{
		click = .3,
		tone = .3,
		teleport = .9,
		open_gifts = 1,
		turn = 1,
		arrows = .3,
		dead = .9,
		backwards = .3,
		pickup = .7,
	} do
		if not sounds[name] then
			sounds[name] = love.audio.newSource('snd/' .. name .. '.wav', 'static')
		end
		sounds[name]:setVolume(volume * settings.sounds_volume)
	end
end


function play_music(title, repeatable)
	if not (title and settings.music) then return end
	local filename = title
	-- if title isn't in backgrounds, assume it's a filename
	if backgrounds[title] then filename = backgrounds[title].music end
	if not repeatable and current.played == filename then return end
	current.played = filename
	filename = 'snd/' .. (filename or title) .. '.it'
	if not love.filesystem.getInfo(filename) then
		print('Music not found: ' .. title)
		return
	end
	if current.music then
		current.music:stop()
		current.music:release()
		current.music = nil
	end
	current.music = love.audio.newSource(filename, 'stream')
	current.music:setVolume(settings.music_volume)
	current.music:play()
end


function play_sound(s)
	if s == 'dead' and settings.vibrate then love.system.vibrate(.3) end
	if not settings.sounds then return end
	sounds[s]:stop()
	sounds[s]:play()
end


function draw_background()
	local b = current.background or
		backgrounds[level_labels[game.currentlevel]].color or {0, 0, 0}
	love.graphics.clear(b)
end
