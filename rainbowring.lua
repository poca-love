--[[
Rainbow Ring - a minigame is a part of POCA - a puzzle game

POCA is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


require('special')

rr = {

hijacked_love_functions = {
	'draw', 'resize',
	'mousepressed', 'mousemoved', 'mousereleased',
	'keypressed'
},


hijacked_globals = {'menus'},


init =  function(self, length)
	self.ending = false
	self.game_mode = false
	self.length = self.length or 7
	self.next_coin = false
	self.ring = {}
	self.menu_button = {}
	self.random = {}
	self:reset_coins()
	self.resize()
	self.last_score = nil
	self.time_started = nil
end,


ring_insert = function(self, pos, p)
	if self.ring[pos] then return false end
	local newring = {}
	for a = 1, self.length do
		local newpos = (2 * a - pos - 1) % self.length + 1
		newring[newpos] = self.ring[a]
	end
	newring[pos] = p
	self.ring = newring
	return true
end,


save_scores = function()
	local ok, err = love.filesystem.write('rr_scores', 'rr.scores = ' .. serialize(rr.scores))
	if not ok then error(err) end
end,


load_scores = function()
	rr.scores = {}
	local rr_scores, err = love.filesystem.load('rr_scores')
	if not err then rr_scores() end
end,


main = function(self)
	hijack(self)
	self:populate_menus()
	self:init()
	self:load_scores()
	self.show_menu = true
	menus.main.onload()
end,


draw_coin = function(self, x, y, color, value, selected)
	--rr.rainbow(love.timer.getTime() * 10 % 100 / 100), 1
	color = color or {1, 1, 1}
	love.graphics.setColor(color[1] * 1.3, color[2] * 1.3, color[3] * 1.3, 1)
	love.graphics.circle('fill', x - rr.smallr * .02, y - rr.smallr * .02, rr.smallr)
	love.graphics.setColor(color[1] * .7, color[2] * .7, color[3] * .7, 1)
	love.graphics.circle('fill', x + rr.smallr * .02, y + rr.smallr * .02, rr.smallr)
	if selected then
		love.graphics.setColor(color[1] * .7, color[2] * .7, color[3] * .7, 1)
	else
		love.graphics.setColor(color)
	end
	love.graphics.circle('fill', x, y, rr.smallr * .96)
	love.graphics.setColor(color[1] * 1.3, color[2] * 1.3, color[3] * 1.3, 1)
	love.graphics.circle('fill', x + rr.smallr * .02, y + rr.smallr * .02, rr.smallr * .7)
	love.graphics.setColor(color[1] * .7, color[2] * .7, color[3] * .7, 1)
	love.graphics.circle('fill', x - rr.smallr * .02, y - rr.smallr * .02, rr.smallr * .7)
	love.graphics.setColor(color)
	love.graphics.circle('fill', x, y, rr.smallr * .66)
	if value then
		love.graphics.setColor(1,1,1, .9)
		local offsety = screen.fontheight / 2
		love.graphics.printf(value, x - rr.smallr / 2, y - offsety, rr.smallr ,'center')
	end
	love.graphics.setColor(1,1,1, 1)
end,


draw_playfield = function(self)
	local r = screen.playfieldsize / 2
	local centerx = screen.playfieldx + r
	local centery = screen.playfieldy + r
	love.graphics.setColor(1,1,1, 1)
	love.graphics.rectangle('fill', self.playfieldx, self.playfieldy,
		self.playfieldsize, self.playfieldsize, self.smallr / 2
	)
	love.graphics.setColor(0,0,0, 1)
	love.graphics.setLineWidth(self.smallr / 4)
	love.graphics.circle('line', self.bigx, self.bigy, self.bigr)
	for a = 1, self.length do
		love.graphics.setColor(0,0,0, 1)
		love.graphics.circle('fill', self.coins[a].x, self.coins[a].y, self.smallr * 1.25)
		if self.ring[a] then
			self:draw_coin(self.coins[a].x, self.coins[a].y, self.elements[self.ring[a]].color, self.elements[self.ring[a]].value)
		else
			if self.selected and (self.selected.y == 1) and (self.selected.x + 1 == a) then
				love.graphics.setColor(1,1,1, .5)
			else
				love.graphics.setColor(1,1,1, 1)
			end
			love.graphics.circle('fill', self.coins[a].x, self.coins[a].y, self.smallr)
		end
	end
	love.graphics.setLineWidth(1)
	love.graphics.setColor(1,1,1, 1)
	for a = 1, #self.elements do
		if not self.elements[a].hidden or
			(a == #self.elements and self.selected and not self.ending) -- keep next_coin in keyboard selection
		then
			self:draw_coin(
				self.elements[a].x, self.elements[a].y,
				self.elements[a].color or
				(self.next_coin and self.elements[self.next_coin].color) or
				{.5, .5, .5},
				self.elements[a].value or self.next_coin or '?',
				self.elements[a].selected or
					self.selected and (
						((self.selected.y == 0) and (self.selected.x + 1 == a)) or
						((self.selected.y == 2) and (a == self.length + 1))
					)
			)
		end
	end
	-- ending text
	if self.ending then
		love.graphics.setColor(0, 0, 0)
		local offsety = screen.fontheight / 2
		love.graphics.printf(self.ending, self.bigx - self.bigr / 2, self.bigy - offsety, self.bigr ,'center')
	end
	-- menu button
	local w = self.smallr * 2 / 3
	local x1 = self.menu_button.x + w * .25
	local y1 = self.menu_button.y + w * .25
	local x2 = self.menu_button.x + w * 1.75
	local y2 = self.menu_button.y + w * 1.75
	if self.menu_button.selected then
		love.graphics.setColor(1,1,1, .5)
		love.graphics.rectangle('fill', self.menu_button.x, self.menu_button.y, w * 2, w * 2, w / 4)
		love.graphics.setColor(.2,.6,.8, 1)
	else
		love.graphics.setColor(.1,.3,.4, 1)
		love.graphics.rectangle('line', self.menu_button.x, self.menu_button.y, w * 2, w * 2, w / 4)
	end
	love.graphics.setLineWidth(w * .1)
	if self.game_mode and not self.ending then
		love.graphics.line(x1, y1, x2, y2)
		love.graphics.line(x1, y2, x2, y1)
	else
		love.graphics.line(x1, (4 * y1 + y2) / 5, x2, (4 * y1 + y2) / 5)
		love.graphics.line(x1, (y1 + y2) / 2, x2, (y1 + y2) / 2)
		love.graphics.line(x1, (y1 + 4 * y2) / 5, x2, (y1 + 4 * y2) / 5)
	end
	love.graphics.setLineWidth(1)
	love.graphics.setColor(1, 1, 1)
end,


draw = function() -- hijacked function
	love.graphics.clear(.2, .6, .8)
	if rr.show_menu then
		draw_menu()
		return
	end
	rr:draw_playfield()
end,


rainbow = function(h, a)
	local r, g, b
	if h < 1/3 then
		r = math.cos(3 * math.pi * h / 2)
		g = math.sin(3 * math.pi * h / 2)
		b = -1
	elseif h < 2/3 then
		h = h - 1/3
		r = -1
		g = math.cos(3 * math.pi * h / 2)
		b = math.sin(3 * math.pi * h / 2)
	else
		h = h - 2/3
		r = math.sin(3 * math.pi * h / 2)
		g = -1
		b = math.cos(3 * math.pi * h / 2)
	end
	return {r, g, b, a or 1}
end,


reset_coins = function(self)
	self.coins = {}
	self.elements = {}
	for a = 1, self.length do self.coins[a] = {} end
	for a = 1, self.length do
		self.elements[a] = {
			color = rr.rainbow((a - 1) / self.length),
			value = a
		}
	end
	self.elements[self.length + 1] = {}
end,


resize = function()  -- hijacked function
	resize_common()
	-- rainbow ring-specific variables
	rr.bigx = screen.w / 2
	rr.bigy = screen.h / 2
	local bigr_w = screen.w / (2 + 6 / rr.length)
	local bigr_h = screen.h / (2 + 15 / rr.length)
	rr.bigr = math.min(bigr_w, bigr_h)
	rr.smallr = 2 * rr.bigr / rr.length
	rr.playfieldx = rr.bigx - rr.bigr - rr.smallr * 1.25
	rr.playfieldy = rr.bigy - rr.bigr - rr.smallr * 1.25
	rr.playfieldsize = 2 * (rr.bigr + rr.smallr * 1.25)
	-- coins on ring
	for a = 1, rr.length do
		local pos = (a - 1) * 2 * math.pi / rr.length
		rr.coins[a].x = rr.bigx + math.sin(pos) * rr.bigr
		rr.coins[a].y = rr.bigy + math.cos(pos) * rr.bigr
	end
	-- palette
	local offsetx = (screen.w - 2 * rr.smallr) / (rr.length - 1)
	local offsety = rr.playfieldy - rr.smallr * 1.25
	for a = 1, rr.length do
		rr.elements[a].x = (a - 1) * offsetx + rr.smallr
		rr.elements[a].y = offsety
	end
	-- central button
	rr.elements[rr.length + 1].x = rr.bigx
	rr.elements[rr.length + 1].y = rr.bigy
    -- menu button
	rr.menu_button.x = rr.smallr * .25
	rr.menu_button.y = rr.playfieldy + rr.playfieldsize + rr.smallr * (3.5 / 6 )
	-- menu background
	screen.pfcanvas:renderTo(rr.menubg)
end,


-- determines if the clicked point is within radius
isin = function(mx, my, x, y, r)
	return math.abs(mx - x) ^ 2 + math.abs(my - y) ^ 2 <= r ^ 2
end,


check_score = function(self)
	self.last_score = love.timer.getTime() - self.time_started
	local today = os.time()
	local ending
	if self.ending == 'Impressive!' then
		ending = 'ccw'
	else
		ending = 'cw'
	end
	self.scores[self.length] = self.scores[self.length] or {}
	local a = 0
	repeat
	a = a + 1
	until
		not self.scores[self.length][a] or
		not self.scores[self.length][a][2] or
		self.last_score < self.scores[self.length][a][2]
	table.insert(self.scores[self.length], a, {today, self.last_score, ending})
	table.remove(self.scores[self.length], 6) -- shave table to 5
	if a <= 5 then -- show high score
		self:save_scores()
		self.show_menu = true
		menus.main.onload()
	end
end,


-- checks for victory condition
-- exits if ring not entirely full
ring_check = function(self)
	local function wrap(a) return (a - 1) % self.length + 1 end
	local forwards, backwards = true, true
	for a = 2, self.length do
		if not (self.ring[a - 1] and self.ring[a]) then return end
		forwards = forwards and (wrap(self.ring[a - 1] + 1) == self.ring[a])
		backwards = backwards and (wrap(self.ring[a - 1] - 1) == self.ring[a])
	end
	if forwards then
		if self.game_mode == 1 then
			self.ending = 'Bravo!'
		elseif self.game_mode == 2 then
			self.ending = 'Wow!'
		elseif self.game_mode == 3 then
			self.ending = 'Impressive!'
			self:check_score()
		end
	elseif backwards then
		if self.game_mode == 1 then
			self.ending = 'Ovarb!'
		elseif self.game_mode == 2 then
			self.ending = 'Antiwow!'
		elseif self.game_mode == 3 then
			self.ending = 'Expressive!'
			self:check_score()
		end
	else
		self.ending = '' -- mark ring as full but no victory
	end
	-- hide last element
	self.elements[self.length + 1].hidden = true
end,


click1 = function(self, a)
	if not (self.next_coin and (not self.game_mode or self.game_mode == 1)) then return end
	local ne = self.elements[self.next_coin]
	self.game_mode = 1
	self.elements[self.length + 1].hidden = true
	self:ring_insert(a, self.next_coin)
	self:ring_check()
	ne.hidden = true
	rr.next_coin = false
end,


click2 = function(self, a)
	if not self.game_mode or self.game_mode == 2 or self.game_mode == 3 then
		if not self.game_mode then
			self.game_mode = 2
			for b = 1, self.length do
				self.elements[b].hidden = true
			end
		end
		if self.game_mode == 2 then
			self.next_coin = (self.next_coin or 1)
		end
		if self:ring_insert(a, self.next_coin) then
			if self.game_mode == 2 then
				if self.next_coin < self.length then
					self.next_coin = self.next_coin + 1
				end
			elseif self.game_mode == 3 then
				self.next_coin = self.random[#self.random]
				self.random[#self.random] = nil
			end
			self:ring_check()
		end
	end
end,


-- activates random mode
click3 = function(self, a)
	for b = 1, self.length do
		self.elements[b].hidden = true
	end
	self.game_mode = 3
	self.random = random_list(self.length)
	self.next_coin = self.random[#self.random]
	self.random[#self.random] = nil
	self.time_started = love.timer.getTime()
end,


click_menu_button = function(self)
	self.menu_button.selected = false
	if self.game_mode and not self.ending then
		self:init()
	else
		self.show_menu = true
		menus.main.onload()
	end
end,


mousepressed = function(mx, my, button) -- hijacked function
	-- menu button click
	if isin(mx, my, rr.menu_button.x, rr.menu_button.y, rr.smallr * 2, rr.smallr * 2) then
		rr:click_menu_button()
		return
	end
	if rr.selected then
		rr.selected = false
		if not rr.game_mode or rr.game_mode == 1 then rr.next_coin = false end
	end
	if rr.show_menu then
		menu_click(mx, my)
		return
	end
	if rr.ending then
		rr:init()
		return
	end
	for a = 1, rr.length do
		if rr.isin(mx, my, rr.coins[a].x, rr.coins[a].y, rr.smallr) then
			rr:click2(a)
		end
	end
	-- drag - game mode 1
	for a = 1, rr.length do
		if rr.isin(mx, my, rr.elements[a].x, rr.elements[a].y, rr.smallr) and not rr.elements[a].hidden
		then
			rr.next_coin = a
			love.mouse.setGrabbed(true)
		end
	end
	-- click to '?' - game mode 3
	if rr.isin(mx, my,
		rr.elements[rr.length + 1].x, rr.elements[rr.length + 1].y, rr.smallr)
		and (not rr.game_mode)
	then
		rr:click3()
	end
end,


mousemoved = function(x, y, dx, dy) -- hijacked function
	if rr.show_menu then return end
	if isin(x, y, rr.menu_button.x, rr.menu_button.y, rr.smallr * 2, rr.smallr * 2) then
		rr.menu_button.selected = true
	else
		rr.menu_button.selected = false
	end
	if rr.selected then return end
	if not rr.next_coin or rr.game_mode == 3 then return end
	rr.elements[rr.next_coin].x = rr.elements[rr.next_coin].x + dx
	rr.elements[rr.next_coin].y = rr.elements[rr.next_coin].y + dy
end,


mousereleased = function(mx, my, button) -- hijacked function
	if rr.show_menu then return end
	love.mouse.setGrabbed(false)
	if rr.selected then
		rr.selected = false
		return
	end
	if not (rr.next_coin and (not rr.game_mode or rr.game_mode == 1)) then return end
	local ne = rr.elements[rr.next_coin]
	for a = 1, rr.length do
		if ne and not rr.ring[a] and rr.isin(ne.x, ne.y, rr.coins[a].x, rr.coins[a].y, rr.smallr)
		then
			rr:click1(a)
			ne.hidden = true
		end
	end
	rr.next_coin = false
	rr.resize()
end,


-- keyboard selection
mark = function(self, x)
	rr.menu_button.selected = false
	if self.selected.y == 0 then -- pallette
		if x == 0 and self.elements[self.selected.x + 1].hidden then x = 1 end
		if self.ending or not self.game_mode == 1 then return end -- skip if no palette
		local n = 0
		repeat
			self.selected.x = (self.selected.x + x) % self.length
			n = n + 1
		until not self.elements[self.selected.x + 1].hidden or (n > self.length)
	elseif self.selected.y == 1 then -- ring
		if x == 0 and self.ring[self.selected.x + 1] then x = 1 end
		if self.ending then return end -- skip if all full
		local n = 0
		repeat
			n = n + 1
			self.selected.x = (self.selected.x - x) % self.length
		until not self.ring[self.selected.x + 1] or (n > self.length)
	elseif self.selected.y == 3 then -- menu button
		rr.menu_button.selected = true
	end
end,


keypressed = function(k)
	if rr.show_menu then
		if k == 'left' then
			menu_select(-1, 0)
		elseif k == 'right' then
			menu_select(1, 0)
		elseif k == 'up' then
			menu_select(0, -1)
		elseif k == 'down' then
			menu_select(0, 1)
		elseif k == 'return' then
			menu_press()
		end
	else -- not menu
		if not rr.selected then rr.selected = {x = math.floor(rr.length / 2), y = 0} end
		if k == 'up' then
			if rr.game_mode then
				rr.menu_button.selected = not rr.menu_button.selected
				return
			end
			rr.next_coin = false
			rr.selected.y = (rr.selected.y - 1) % 4
			rr:mark(0)
		elseif k == 'down' then
			if rr.game_mode then
				rr.menu_button.selected = not rr.menu_button.selected
				return
			end
			rr.next_coin = false
			rr.selected.y = (rr.selected.y + 1) % 4
			rr:mark(0)
		elseif k == 'left' then
			if rr.selected.y == 3 then rr.selected.y = 1 end -- menu button was sel
			if rr.game_mode == 2 or rr_game_mode == 3 then rr.selected.y = 1 end
			rr:mark(-1)
		elseif k == 'right' then
			if rr.selected.y == 3 then rr.selected.y = 1 end -- menu button was sel
			if rr.game_mode == 2 or rr_game_mode == 3 then rr.selected.y = 1 end
			rr:mark(1)
		elseif k == 'return' then
			if rr.menu_button.selected then
				rr:click_menu_button()
				return
			end
			if rr.ending then
				rr:init()
				return
			elseif rr.selected.y == 0 then -- pallette
				rr.next_coin = rr.selected.x + 1
				rr.selected.y = 1
				rr:mark(0)
			elseif rr.selected.y == 1 then -- ring
				rr:click1(rr.selected.x + 1)
				rr:click2(rr.selected.x + 1)
				if rr.game_mode == 1 then rr.selected.y = 0 end
				rr:mark(0)
			elseif rr.selected.y == 2 then
				rr.selected.y = 1
				rr:click3()
				rr:mark(0)
			end
		end
	end
	-- global keys
	if k == 'q' then
		love.event.push('quit')
	end
	if k == 'escape' or k == 'm' then
		rr.show_menu = not rr.show_menu
		if rr.show_menu then menus.main.onload() end
	end
end,


-- background in menu
menubg = function()
	local ts = screen.tilesize
	-- show coins
	love.graphics.clear(.2, .6, .8)
	love.graphics.setColor(1, 1, 1)
	love.graphics.rectangle(
		'fill',
		0, ts * 0.5,
		screen.playfieldsize, ts * 3,5
	)
	local offsetx = rr.smallr + .8 * ts
	for a = 1, rr.length do
		rr:draw_coin(
			offsetx + (a - 1) * (2.3 * ts - rr.smallr) / rr.length,
			ts * 0.5, rr.elements[a].color
		)
	end
	-- darken central square for text display
	love.graphics.setColor(0,0,0, .25)
	love.graphics.rectangle('fill',
		ts * .8, ts * .2,
		ts * 2.4, ts * .6
	)
	love.graphics.setColor(1, 1, 1)
	-- prepare to print scores
	local scores = rr.get_scores(rr.length) or {}
	local max_date_width, max_time_width = 0, 0
	for a = 1, #scores do -- get max width of fuzzy date
		max_date_width = math.max(
			screen.defaultfont:getWidth(scores[a].date), max_date_width
		)
		max_time_width = math.max(
			screen.defaultfont:getWidth(scores[a].time), max_time_width
		)
	end
	local max_width = ts * .6 + max_date_width + max_time_width
	offsetx = math.floor((screen.playfieldsize - max_width) / 2)
	local offsety = 1.5 * ts
	-- actually print scores
	love.graphics.setColor(0, 0, 0)
	if #scores > 0 then love.graphics.print('High scores', offsetx, ts) end
	for a = 1, #scores do
		-- draw rectangle behind fresh scores
		love.graphics.setColor(rr.rainbow((a - 1) / 5, .5))
		local highlight_length = ts * .25
		if scores[a].date == 'just now' then highlight_length = ts * .1 + max_width end
		love.graphics.rectangle(
			'fill',
			offsetx - ts * .05, offsety + (a - 1) * ts / 3,
			highlight_length, screen.fontheight, ts * .1
		)
		love.graphics.setColor(0, 0, 0)
		-- print score position and date
		love.graphics.print(a .. '. ',
			offsetx, offsety + (a - 1) * ts / 3
		)
		love.graphics.print(scores[a].date,
			offsetx + ts * .3, offsety + (a - 1) * ts / 3
		)
		 -- draw direction
		love.graphics.setColor(0, 0, 0)
		if scores[a].direction == 'ccw' then
			draw_image({name = 'cw'},
				(ts * .1 + offsetx + ts * .4 + max_date_width) / ts,
				(ts * .11 + offsety + (a - 1) * ts / 3) / ts,
				0, -.18
			)
		elseif scores[a].direction == 'cw' then
			draw_image({name = 'cw'},
				(ts * .1 + offsetx + ts * .4 + max_date_width) / ts,
				(ts * .11 + offsety + (a - 1) * ts / 3) / ts,
				0, .18
			)
		end
		-- draw score time
		love.graphics.setColor(0, 0, 0)
		love.graphics.print(scores[a].time,
			offsetx + max_width - screen.defaultfont:getWidth(scores[a].time), offsety + (a - 1) * ts / 3
		)
	end
	love.graphics.setColor(1, 1, 1)
end,


menus = {},


get_scores = function(length)
	if not (rr.scores and rr.scores[length]) then return end
	local function fuzzy_ago(s)
		if s < 60 then return 'last minute' end
		local m = math.floor(s / 60)
		local h = math.floor(m / 60)
		local d = math.floor(h / 24)
		local y = d / 365
		if y >= 1 then return string.format('%.1f', y) .. ' years ago' end
		m = m % 60
		h = h % 24
		d = d % 365
		local o
		if d >= 1 then
			o = d .. ' day'
			if d > 1 then o = o .. 's' end
			o = o .. ', '
			if h > 0 then o = o .. h .. 'h, ' end
		else
			o = ''
			if h > 0 then o = o .. h .. 'h, ' end
			if m > 0 then o = o .. m .. '\', ' end
		end
		o = string.sub(o, 1, -3) .. ' ago'
		return o
	end
	local rrsl = rr.scores[length]
	local scores = {}
	for a = 1, 5 do
		if rrsl[a] and rrsl[a][1] then
			scores[a] = {
				date = fuzzy_ago(os.time() - rrsl[a][1]),
				time = string.format('%.2fs', rrsl[a][2]),
				direction = rrsl[a][3]
			}
			if (rr.last_score == rrsl[a][2]) then
				scores[a].date = 'just now'
			end
		end
	end
	return scores
end,


populate_menus = function(self)
	rr.menu_elements = {
		coin_minus = {
			x = .05, y = .05, w = .15, h = .15,
			content = '-',
			onclick = function ()
				rr.menu_elements.confirm_reset.hidden = true
				rr.menu_elements.reset_scores.hidden = false
				rr.length = math.max(5, rr.length - 2)
				rr:init()
			end,
		},
		coin_count = {
			x = .25, y = 0, w = .5, h = .25,
			content = function()
				return rr.length .. ' Coins'
			end,
		},
		coin_plus = {
			x = .8, y = .05, w = .15, h = .15,
			content = '+',
			onclick = function ()
				rr.menu_elements.confirm_reset.hidden = true
				rr.menu_elements.reset_scores.hidden = false
				rr.length = math.min(11, rr.length + 2)
				rr:init()
			end,
		},
		confirm_reset = {
			x = .5, y = .85, w = .25, h = .15,
			content = 'Are you sure?',
			onclick = function ()
				rr.scores[rr.length] = {}
				rr.menu_elements.confirm_reset.hidden = true
				rr.menu_elements.reset_scores.hidden = false
				screen.pfcanvas:renderTo(rr.menubg)
			end,
		},
		reset_scores = {
			x = .25, y = .85, w = .25, h = .15,
			content = 'Reset',
			onclick = function ()
				rr.menu_elements.confirm_reset.hidden = false
				rr.menu_elements.reset_scores.hidden = true
			end,
		},
	}

	menus = {
		main = {
			onload = function()
				rr.menu_elements.confirm_reset.hidden = true
				rr.menu_elements.reset_scores.hidden = false
				screen.pfcanvas:renderTo(rr.menubg)
			end,
			{
				x = 0, y = - .25, w = .5, h = .25,
				content = 'Play\nRainbow Ring',
				onclick = function () rr.show_menu = false end,
			},
			{
				x = .5, y = -.25, w = .5, h = .25,
				content = 'Return to POCA',
				onclick = function ()
					release(rr)
					-- becomes relevant after changing screens
					change_screen('menu_objects')
					love.resize()
				end,
			},
			{
				x = 0, y = 0, w = 1, h = 1,
				content = function() return {
					bmp = screen.pfcanvas,
					offsetx = 0, offsety = 0,
					scale = screen.playfieldsize
				} end,
			},
			rr.menu_elements.coin_minus,
			rr.menu_elements.coin_count,
			rr.menu_elements.coin_plus,
			rr.menu_elements.reset_scores,
			rr.menu_elements.confirm_reset,
		},
	}
	-- default menu
	menus.current = menus.main
end,


} -- end of rr
