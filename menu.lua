--[[
This file is part of POCA - a puzzle game

POCA is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


-- create new canvas for menu element, if nonexistent or resized
function new_canvas_needed(e)
	if not (
			e.canvas and
			(e.canvas:getWidth() == math.floor(
					e.w * screen.playfieldsize
				)
			)
		)
	then
		-- math.floor probably redundant here, but
		-- just to be sure, to prevent unnecesary resizing
		e.canvas = love.graphics.newCanvas(
			math.floor(e.w * screen.playfieldsize),
			e.h * screen.playfieldsize
		)
		return true
	end
end


-- menu elements and menus (in corresponding tables)


-- reusable
menu_elements = { -- relative to playfield size and position
	-- casette
	casette_next = {
		x = .5, y = .5, w = .25, h = .25,
		content = function()
			return images.ffwd
		end,
		onclick = function()
			local ml = menus.menu_casette.music_list
			if not current.played then
				current.played = backgrounds[level_labels[game.currentlevel]].music or
					'opening'
			end
			for a = 1, #ml do
				if ml[a] == current.played then
					a = a % #ml + 1
					play_music(ml[a], true)
					menus.menu_casette.play = true
					break
				end
			end
		end,
	},
	casette_play = {
		x = .25, y = .5, w = .25, h = .25,
		content = function()
			if current.music and current.music:isPlaying() then
				return images.stop
			else
				return images.play
			end
		end,
		onclick = function()
			if not current.played then
				current.played = backgrounds[level_labels[game.currentlevel]].music or
					'opening'
			end
			if current.music and current.music:isPlaying() then
				current.music:stop()
				menus.menu_casette.play = nil
			else
				play_music(current.played, true)
				menus.menu_casette.play = true
			end
		end,
	},
	-- wise man
	wise_man_picture = {
		x = 0, y = 0, w = .25, h = .25,
		content = images['N']
	},
	wise_man_says = {
		x = .25, y = 0, w = .5, h = .25,
		content = 'Wise man says:'
	},
	-- objects
	object_name = {
		x = 0, y = 0, w = 1, h = .2,
		content = function()
			if #game.trinkets == 0 then return 'You have no objects.' end
			if not current.trinket then current.trinket = #game.trinkets end
			return current.trinket .. '. ' .. trinkets[game.trinkets[current.trinket]].name
		end,
	},
	big_object = {
		x = .25, y = .25, w = .5, h = .5,
		page = 1,
		content = function(self, dx)
			-- do nothing if no trinket
			local t = game.trinkets[current.trinket]
			if not t then return end
			-- return text or draw on canvas, then return canvas
			local _, _, real_w, real_h = get_absolute_dimensions(self)
			local function drawbmp(c, dx)
				dx = dx or 0
				love.graphics.draw(c.bmp,
					dx + c.offsetx * screen.tilesize, c.offsety * screen.tilesize,
					0, real_w / c.scale, real_h / c.scale
				)
			end
			if self.page == 1 then -- trinket image
				-- draw previous or next image when sweeping
				if self.sweep_x or new_canvas_needed(self) then
					love.graphics.setCanvas(self.canvas)
					love.graphics.clear()
					dx = dx or 0
					dx = math.max(- real_w, dx)
					dx = math.min(real_w, dx)
					-- draw left, right and center imgs
					drawbmp(menus.menu_objects.get_object_img(-1), dx - real_w)
					drawbmp(menus.menu_objects.get_object_img(1), dx + real_w)
					drawbmp(menus.menu_objects.get_object_img(0), dx)
					love.graphics.setCanvas()
				end
				return {
					bmp = self.canvas, offsetx = 0, offsety = 0,
					scale = screen.tilesize / self.w
				}
			elseif self.page == 2 then -- trinket description
				local d = ''
				if game.trinkets[current.trinket] == 'cigarettes' then
					d = '\n\nThere are ' .. game.cigarettes .. ' cigarettes left.'
				end
				return trinkets[game.trinkets[current.trinket]].description .. d
			end
		end,
		onclick = function(self)
			local t = game.trinkets[current.trinket]
			if not t then return end
			if
				trinkets[t].onclick and
				not ( -- drawing in 'Secrets' is default
					t == 'pencil' and
					level_labels[game.currentlevel] == 'Secrets'
				)
			then
				trinkets[t].onclick()
			elseif trinkets[t].description then
				self.page = self.page % 2 + 1
			end
		end,
		onsweep = function(self, dx)
			local w = self.w * screen.playfieldsize
			if dx > w / 2 then
				menu_elements.previous_object:onclick()
			elseif (dx < - w / 2) then
				menu_elements.next_object:onclick()
			elseif math.abs(dx) < w / 8 then
				self:onclick()
			end
		end
	},
	-- donate
	donate_title = {
		x = 0, y = -.25, w = .5, h = .25,
		content = 'Select an object to donate',
	},
	big_object_donate = {
		x = .25, y = .25, w = .5, h = .5,
		page = 1, -- show pic, not description
		content = function(self, dx)
			-- borrow content from big_object
			-- uses own canvas, page, dx
			return menu_elements.big_object.content(self, dx)
		end,
		onclick = function(self)
			menu_elements.donate_title.hidden = true
			menu_elements.confirm_donate.hidden = false
		end,
		onsweep = function(self, dx)
			local w = self.w * screen.playfieldsize
			if dx > w / 2 then
				menu_elements.previous_object_donate:onclick()
			elseif (dx < - w / 2) then
				menu_elements.next_object_donate:onclick()
			elseif math.abs(dx) < w / 8 then
				self:onclick()
			end
		end
	},
	previous_object = {
		x = 0, y = .4, w = .2, h = .2,
		content = '-',
		onclick = function()
			change_screen('menu_objects')
			current.trinket = select_trinket(-1)
		end,
	},
	next_object = {
		x = .8, y = .4, w = .2, h = .2,
		content = '+',
		onclick = function()
			change_screen('menu_objects')
			current.trinket = select_trinket(1)
		end,
	},
	previous_object_donate = {
		x = 0, y = .4, w = .2, h = .2,
		content = '-',
		onclick = function()
			change_screen('menu_objects_donate')
			current.trinket = select_trinket(-1)
		end,
	},
	next_object_donate = {
		x = .8, y = .4, w = .2, h = .2,
		content = '+',
		onclick = function()
			change_screen('menu_objects_donate')
			current.trinket = select_trinket(1)
		end,
	},
	light_cig = {
		hidden = true,
		x = 0, y = .8, w = .3, h = .2,
		content = 'Light a cigarette',
		onclick = function()
			game.matches = game.matches - 1
			if game.matches == 0 then lose_trinket('matches') end
			change_screen('story_cigarettes')
		end,
	},
	light_secret = {
		hidden = true,
		x = .35, y = .8, w = .4, h = .2,
		content = 'Light surroundings',
		onclick = function()
			game.matches = game.matches - 1
			if game.matches == 0 then lose_trinket('matches') end
			auto_paint()
			change_screen('playfield')
		end,
	},
	confirm_donate = {
		hidden = true,
		x = 0, y = -.25, w = .5, h = .25,
		content = 'Really donate?',
		onclick = function()
			table.insert(game.wise_man_has, game.trinkets[current.trinket])
			if game.trinkets[current.trinket] == 'map' then
				wise_man_lose('advice_map')
			end
			lose_trinket(game.trinkets[current.trinket])
			menus.menu_objects.onload()
			menus.wise_man_offers.offer_pool = wise_get_offer() -- get list of 3 elements or less
			change_screen('wise_man_offers')
		end,
	},
	-- common
	play = {
		x = 0, y = -.25, w = .5, h = .25,
		content = 'Play',
		onclick = function() change_screen('playfield') end,
	},
	exit_to_title = {
		x = .5, y = -.25, w = .5, h = .25,
		content = 'Main menu',
		onclick = function() change_screen('menu_main') end,
	},
	-- save
	delete_saved = {
		x = .75, y = .75, w = .25, h = .25,
		content = 'Delete all',
		onclick = function()
			menu_elements.confirm_delete.hidden = false
			menu_elements.delete_saved.hidden = true
		end,
	},
	confirm_delete = {
		hidden = true,
		x = .75, y = .5, w = .25, h = .25,
		content = 'Are you sure?',
		onclick = function()
			for a = 1, 4 do love.filesystem.remove('game' .. a) end
			menus.menu_save.onload()
		end,
	},
	-- load
	restart_game = {
		x = .75, y = .75, w = .25, h = .25,
		content = 'Restart game',
		onclick = function()
			menu_elements.confirm_restart.hidden = false
			menu_elements.restart_game.hidden = true
		end,
	},
	confirm_restart = {
		hidden = true,
		x = .75, y = .5, w = .25, h = .25,
		content = 'Are you sure?',
		onclick = function()
			menu_elements.restart_game.hidden = false
			menu_elements.confirm_restart.hidden = true
			init()
			setuplevel()
		end,
	},
}
for a = 1, 4 do
	menu_elements['save_slot' .. a ] = {
		x = 0, y = (a - 1) * .25, w = .5, h = .25,
		content = function()
			return 'Slot ' .. a .. ': ' .. (menus.save_slots_info[a] or 'empty')
		end,
		onclick = function()
			save(game, 'game' .. a)
			change_screen('playfield')
		end,
	}
	menu_elements['load_slot' .. a ] = {
		x = 0, y = (a - 1) * .25, w = .5, h = .25,
		content = function()
			return 'Slot ' .. a .. ': ' .. (menus.save_slots_info[a] or 'empty')
		end,
		onclick = function()
			load_game('game' .. a)
			change_screen('playfield')
		end,
	}
end


menus = {
	menu_casette = {
		background = function()
			inplayfield(function (x, y, p)
				draw_floor(x - .5, y - .5)
			end)
		end;
		onload = function(self)
			self.music_list = {}
			for k, v in pairs(backgrounds) do
				table.insert(self.music_list, v.music)
			end
			table.insert(self.music_list, 'stairway')
		end,
		menu_elements.play,
		menu_elements.exit_to_title,
		{	-- return to objects menu
			x = .75, y = .75, w = .25, h = .25,
			content = function()
				return images.eject
			end,
			onclick = function()
				if current.music then
					current.music:stop()
					current.music:release()
					current.music = nil
				end
				change_screen('menu_objects')
			end,
		},
		{	-- title
			x = 0, y = 0, w = .75, h = .25,
			content = 'POCA Soundtrack'
		},
		{	-- icon
			x = .75, y = 0, w = .25, h = .25,
			content = function() return trinkets.casette end,
		},
		{	-- now_playing
			x = 0, y = .25, w = 1, h = .25,
			content = function(self, dx)
				-- return text only when not playing
				if not settings.music then
					return 'Music turned off in Options menu.'
				elseif not current.music or not current.music:isPlaying() then
					return ''
				end
				-- return canvas when playing
				local ml = menus.menu_casette.music_list
				local order = 0
				repeat
					order = order + 1
				until order > #ml or ml[order] == current.played
				local title = order .. '/' .. #ml .. '\n' .. current.played
				local seconds = math.floor(current.music:tell()) .. 's /\n' ..
					math.floor(current.music:getDuration()) .. 's'
				local oy = screen.tilesize / 2 - screen.fontheight
				local snd_pos = current.music:tell('samples') /
					current.music:getDuration('samples') * screen.playfieldsize
				local new_pos = snd_pos + dx
				new_pos = math.max(0, new_pos)
				new_pos = math.min(screen.playfieldsize, new_pos)
				-- draw
				new_canvas_needed(self)
				love.graphics.setCanvas(self.canvas)
				love.graphics.clear(.15, .15, .15)
				love.graphics.setColor(0, .5, 0)
				love.graphics.rectangle('fill',
					0, 0,
					snd_pos, screen.tilesize
				)
				if new_pos < snd_pos then
					love.graphics.setColor(.15, .15, .15)
				end
				love.graphics.polygon('fill',
					snd_pos, 0,
					snd_pos, screen.tilesize,
					new_pos, screen.tilesize / 2
				)
				love.graphics.setColor(1, 1, 1)
				love.graphics.printf(title,
					0, oy, 3 * screen.tilesize, 'center'
				)
				love.graphics.printf(seconds,
					3 * screen.tilesize, oy, screen.tilesize, 'center'
				)
				love.graphics.setCanvas()
				return {
					bmp = self.canvas, offsetx = 0, offsety = 0,
					scale = screen.playfieldsize
				}
			end,
			onsweep = function(self, dx)
				if not current.music then return end
				-- seeking in samples doesn't work for modules
				local snd_pos = current.music:tell()
				local d = current.music:getDuration()
				local new_pos = snd_pos + dx * d / screen.playfieldsize
				new_pos = math.max(0, new_pos)
				new_pos = math.min(d, new_pos)
				current.music:seek(new_pos)
			end
		},
		{	-- prev
			x = 0, y = .5, w = .25, h = .25,
			content = function()
				return images.rewind
			end,
			onclick = function()
				local ml = menus.menu_casette.music_list
				if not current.played then
					current.played = backgrounds[level_labels[game.currentlevel]].music or
						'opening'
				end
				for a = 1, #ml do
					if ml[a] == current.played then
						a = (a - 2) % #ml + 1
						play_music(ml[a], true)
						menus.menu_casette.play = true
						break
					end
				end
			end,
		},
		menu_elements.casette_play,
		menu_elements.casette_next,
		{	-- repeat
			x = .75, y = .5, w = .25, h = .25,
			content = function()
				-- handle repeat
				-- better not setLooping, as songs can be skipped
				if
					menus.menu_casette.play and
					current.music and not current.music:isPlaying()
				then
					if menus.menu_casette.loop == 1 then
						menu_elements.casette_play.onclick()
					elseif menus.menu_casette.loop == 2 then
						menu_elements.casette_next.onclick()
					end
				end
				-- actual content
				local l = menus.menu_casette.loop or 0
				return unpack(
					{images.loop_none, images.loop_1, images.loop_all},
					l + 1, l + 1
				)
			end,
			onclick = function()
				menus.menu_casette.loop = ((menus.menu_casette.loop or 0) + 1) % 3
				if -- repeat all, while music in progress
					menus.menu_casette.loop > 0 and
					current.music and current.music:isPlaying()
				then
					menus.menu_casette.play = true
				else
					menus.menu_casette.play = false
				end
			end,
		},
	},
	menu_hints = {
		{	-- hint book description
			x = .25, y = .25, w = .5, h = .5,
			content = function(self)
				return trinkets['hints_book'].description
			end,
			onclick = function(self)
				 change_screen('menu_objects')
			end,
		},
		{	-- open hint book button
			x = 0, y = .8, w = .4, h = .2,
			content = 'Open anyway',
			onclick = function(self)
				 change_screen('menu_show_hint')
			end,
		},
		menu_elements.previous_object,
		menu_elements.next_object,
		menu_elements.play,
		menu_elements.exit_to_title,
		menu_elements.object_name,
	},
	menu_show_hint = {
		background = function()
			-- show plafield in background
			draw_initial_playfield()
		end,
		onload = function()
			-- get hint text
			menus.menu_show_hint.text = (
				hints[levels[game.currentlevel].setup] or
				"The page is missing."
			)
			-- possibly destroy book
			if math.random (1,6) == 1 then -- bad luck
				menus.menu_show_hint.text = (
					menus.menu_show_hint.text ..
					'\n\nThe book crumbles into dust!'
				)
				lose_trinket('hints_book')
			end
		end,
		{	-- draw hint book picture
			x = .375, y = -.25, w = .25, h = .25,
			content = function(self)
				return trinkets['hints_book']
			end,
			onclick = function() change_screen('playfield') end,
			transparent = true,
		},
		{	-- print hint text
			x = 0, y = 0, w = 1, h = 1,
			content = function(self)
				return menus.menu_show_hint.text
			end,
			onclick = function() change_screen('playfield') end,
			transparent = true,
		},
	},
	menu_matches = {
		onload = function (self)
			menu_elements.big_object.page = 2
			menu_elements.light_cig.hidden = not has_trinket('cigarettes')
			menu_elements.light_secret.hidden = level_labels[game.currentlevel] ~= 'Secrets'
		end,
		{	-- custom big_object
			x = .25, y = .25, w = .5, h = .5,
			content = function(self)
				return trinkets['matches'].description ..
					'\n\nThere are ' .. game.matches .. ' matches left.'

			end,
			onclick = function(self)
				change_screen('menu_objects')
			end,
		},
		menu_elements.object_name,
		menu_elements.previous_object,
		menu_elements.next_object,
		menu_elements.exit_to_title,
		menu_elements.play,
		menu_elements.light_cig,
		menu_elements.light_secret,
	},
	menu_objects = {
		-- shows current, previous or next trinket
		get_object_img = function(s) -- s can be -1, 0 or 1
			local t = trinkets[game.trinkets[select_trinket(s)]]
			if -- show minimap instead of frame in Secrets
				t.name == 'Scintilating frame' and
				level_labels[game.currentlevel] == 'Secrets'
			then
				t = {
					bmp = screen.minimap, offsetx = 0, offsety = 0,
					scale = screen.tilesize * 4/5
				}
			end
			return t
		end,
		onload = function(self)
			-- hide + and - when objects < 2
			menu_elements.next_object.hidden = #game.trinkets < 2
			menu_elements.previous_object.hidden = menu_elements.next_object.hidden
			if (not current.trinket) or (not game.trinkets[current.trinket]) then
				current.trinket = #game.trinkets
			end
			menu_elements.big_object.page = 1
			menu_elements.big_object.canvas = nil -- force redraw
		end,
        menu_elements.big_object,
		menu_elements.object_name,
		menu_elements.previous_object,
		menu_elements.next_object,
		menu_elements.play,
		menu_elements.exit_to_title,

	},
	menu_objects_donate = { -- used only with the wise man
		onload = function(self)
			-- hide "donate" if there are no objects
			menu_elements.big_object_donate.hidden = (#game.trinkets == 0)
			menu_elements.confirm_donate.hidden = true
			menu_elements.donate_title.hidden = false
			-- hide + and - when objects < 2
			menu_elements.next_object_donate.hidden = #game.trinkets < 2
			menu_elements.previous_object_donate.hidden = menu_elements.next_object_donate.hidden
			if (not current.trinket) or (not game.trinkets[current.trinket]) then
				current.trinket = #game.trinkets
			end
			menu_elements.big_object_donate.canvas = nil -- force redraw
		end,
		menu_elements.big_object_donate,
		menu_elements.object_name,
		menu_elements.previous_object_donate,
		menu_elements.next_object_donate,
		{ -- abort
			x = .5, y = -.25, w = .5, h = .25,
			content = 'Abort',
			onclick = function()
				if not menu_elements.confirm_donate.hidden then
					menu_elements.confirm_donate.hidden = true
					menu_elements.donate_title.hidden = false
				else
					change_screen('playfield')
				end
			end,
		},
		menu_elements.donate_title,
		menu_elements.confirm_donate,

	},
	menu_save = {
		onload = function()
			menus.save_slots_info = read_slots()
			menu_elements.delete_saved.hidden = false
			menu_elements.confirm_delete.hidden = true
		end,
		menu_elements.exit_to_title,
		menu_elements.save_slot1, menu_elements.save_slot2,
		menu_elements.save_slot3, menu_elements.save_slot4,
		menu_elements.delete_saved,
		menu_elements.confirm_delete,
		{ -- save title
			x = 0, y = -.25, w = .5, h = .2,
			content = 'Save where?',
		},
	},
	menu_load = {
		onload = function()
			menus.save_slots_info = read_slots()
			menu_elements.restart_game.hidden = false
			menu_elements.confirm_restart.hidden = true
			for a = 1, 4 do
				menu_elements['load_slot' .. a].hidden = not menus.save_slots_info[a]
			end
		end,
		{ -- load title
			x = 0, y = -.25, w = .5, h = .2,
			content = 'Load from which slot?',
		},
		menu_elements.exit_to_title,
		menu_elements.load_slot1, menu_elements.load_slot2,
		menu_elements.load_slot3, menu_elements.load_slot4,
		menu_elements.restart_game,
		menu_elements.confirm_restart,
	},
	menu_map = { -- full-playfield map
		{
			x = 0, y = 0, w = 1, h = 1,
			content = function() return trinkets.map end,
			onclick = function() change_screen('menu_objects') end,
		},
		menu_elements.exit_to_title,
		menu_elements.play,
	},
	menu_frame = { -- full-playfield frame
		{
			x = 0, y = 0, w = 1, h = 1,
			content = function()
				if -- show minimap instead of frame in Secrets
					game.trinkets[current.trinket] == 'frame' and
					level_labels[game.currentlevel] == 'Secrets'
				then
					return {
						bmp = screen.minimap, offsetx = 0, offsety = 0,
						scale = screen.tilesize * 4/5
					}
				else
					return trinkets[game.trinkets[current.trinket]]
				end
			end,
			onclick = function() change_screen('menu_objects') end,
			transparent = true,
		},
		menu_elements.exit_to_title,
		menu_elements.play,
	},
	menu_settings = {
		get_newvolume = function(oldvolume, dx)
			dx = dx or 0
			local newvolume = oldvolume + dx / 
				screen.playfieldsize / .9
			newvolume = math.max(0, newvolume)
			return math.min(1, newvolume)
		end,
		-- sets volume or clicks to turn audio on or off
		-- audio_kind = 'music' or 'sounds'
		audio_onsweep = function(e, dx, audio_kind)
			local w = e.w * screen.playfieldsize
			if math.abs(dx) < w / 90 then
				e:onclick()
			else
				settings[audio_kind .. '_volume'] =
					menus.menu_settings.get_newvolume(
						settings[audio_kind .. '_volume'], dx
					)
				-- turn on or off music or sounds depending on volume
				settings[audio_kind] = settings[audio_kind .. '_volume'] ~= 0
			end
		end,
		-- shows text and volume bar
		-- audio_kind = 'music' or 'sounds'
		audio_content = function(e, dx, audio_kind)
			if e.sweep_x or new_canvas_needed(e) then
				local _, _, real_w, real_h = get_absolute_dimensions(e)
				love.graphics.setCanvas(e.canvas)
				love.graphics.clear()
				local newvolume = menus.menu_settings.get_newvolume(
					settings[audio_kind .. '_volume'], dx
				)
				-- draw image
				love.graphics.setColor(0, .5, 0)
				love.graphics.rectangle('fill',
					real_w * .05, real_h / 2,
					real_w * .9 * newvolume, real_h / 3
				)
				love.graphics.setColor(0, 1, 0)
				love.graphics.rectangle('line',
					real_w * .05, real_h / 2,
					real_w * .9, real_h / 3
				)
				love.graphics.setColor(1, 1, 1)
				love.graphics.printf(
					{
						{1, 1, 1},
						(audio_kind == 'music' and 'Music is ' or 'Sounds are '),
						(settings[audio_kind] and {1, 1, 1} or {1,1,1, .5}),
						(settings[audio_kind] and 'on' or 'off') ..
						'\n\nVolume: ' ..
						math.floor(newvolume * 100) .. '%'
					}, 0, screen.tilesize / 10, real_w, 'center'
				)
				love.graphics.setCanvas()
			end
			return {
				bmp = e.canvas, offsetx = 0, offsety = 0,
				scale = screen.playfieldsize / e.w
			}
		end,
		menu_elements.play,
		menu_elements.exit_to_title,
		{ -- music
			x = 0, y = 0, w = 1, h = .25,
			content = function(self, dx)
				return menus.menu_settings.audio_content(self, dx, 'music')
			end,
			onsweep = function(self, dx)
				menus.menu_settings.audio_onsweep(self, dx, 'music')
				if current.music then
					current.music:setVolume(settings.music_volume)
				end
			end,
			onclick = function()
				settings.music = not settings.music
				if not settings.music and current.music then
					current.music:pause()
				elseif current.music then
					current.music:play(true)
				end
			end,
		},
		{ -- sounds
			x = 0, y = .25, w = 1, h = .25,
			content = function(self, dx)
				return menus.menu_settings.audio_content(self, dx, 'sounds')
			end,
			onsweep = function(self, dx)
				menus.menu_settings.audio_onsweep(self, dx, 'sounds')
				load_sounds() -- sets volume
				play_sound('tone')
			end,
			onclick = function()
				settings.sounds = not settings.sounds
			end,
		},
		{ -- animations
			x = 0, y = .5, w = 1, h = .25,
			content = function()
				local a = {
					'at normal speed',
					'quicker',
					'disabled'
				}
				return 'Animations are ' ..
					a[settings.speed + 1]
			end,
			onclick = function()
				settings.speed = (settings.speed + 1) % 3
			end,
		},
		{ -- vibration toggle
			x = 0, y = .75, w = 1, h = .25,
			content = function()
				return 'Vibration is ' ..
					(settings.vibrate and 'on' or 'off')
			end,
			onclick = function()
				settings.vibrate = not settings.vibrate
			end,
		},
	},
	story_beginning = {
		{
			x = 0, y = 0, w = 1, h = 1,
			content = 'My journey began when arrows invaded every room of my apartment, including my bedroom.',
			onclick = function() change_screen('playfield') end,
		},
	},
	story_corpse = {
		content = '',
		onload = function(self)
			self.content = click_corpse()
		end,
		{
			x = 0, y = 0, w = 1, h = 1,
			content = function() return menus.story_corpse.content end,
			onclick = function() change_screen('playfield') end,
	},
	},
	story_big_death = {
		{
			x = 0, y = .25, w = 1, h = .5,
			content = 'You sense everything got back where it was.',
			onclick = function() change_screen('playfield') end,
		},
	},
	story_big_end = {
		{
			x = 0, y = 0, w = 1, h = 1,
			content = 'The end. Thanks for playing!',
			onclick = function() love.event.push('quit') end,
		},
	},
	story_battle = {
		menu_elements.wise_man_says,
		{
			x = 0, y = .25, w = 1, h = .5,
			content = 'Are you mad? You are going to finish the game!',
			onclick = function() start_wise_man_battle() end,
		},
	},
	story_cigarettes = {
		onload = function()
			game.cigarettes = game.cigarettes - 1
			if game.cigarettes == 0  then
				lose_trinket('cigarettes')
			end
		end,
		{
			x = 0, y = 0, w = 1, h = 1,
			content = function()
				if game.cigarettes == 2 then
					return 'You hastily light a cigarette and inhale. You feel somewhat better.'
				elseif game.cigarettes == 1 then
					return 'You take your time smoking a cigarette. It feels good.'
				elseif game.cigarettes == 0 then
					return 'You nervously smoke a cigarette. It feels slightly nauseating.'
				end
			end,
			onclick = function() change_screen('playfield') end,
		},
	},
	-- wise man
	wise_man = { -- wise man accepts donations
		background = function()
			inplayfield(function (x, y, p)
				draw_floor(x - .5, y - .5, false, nil, nil)
			end)
		end,
		menu_elements.wise_man_picture,
		{ -- wise man text
			x = 0, y = .25, w = 1, h = .5,
			content = 'I am the wise man.\n' ..
				'Not to talk much is wise.\n\n' ..
				'I accept your generous donations.',
		},
		{ -- donation button
			x = 0, y = -.25, w = .5, h = .25,
			content ='Donate',
			onclick = function()
				change_screen('menu_objects_donate')
				menu_elements.confirm_donate.hidden = true
			end
		},
		{ -- decline button
			x = .5, y = -.25, w = .5, h = .25,
			content ='Decline',
			onclick = function()
				change_screen('playfield')
			end
		},
	},
	wise_man_offers = {
		onload = function(self)
			-- offer pool created when donate button was pressed
			if #self.offer_pool == 0 then
				change_screen('wise_man_thanks')
				return
			end
			self.offer = self.offer_pool[#self.offer_pool]
			self.offer_pool[#self.offer_pool] = nil
			self.offer_text = (
				wise_advices[self.offer] and wise_advices[self.offer][1]
				) or
				trinkets[self.offer].description or
				trinkets[self.offer].name
			local text_ending = string.sub(self.offer_text, -1)
			if not (text_ending == '.' or text_ending == '!') then
				self.offer_text = self.offer_text .. '.'
			end
		end,
		{ -- Yes button
			x = .2, y = .75, w = .2, h = .2,
			content ='Yes',
			onclick = function()
				local offer = menus.wise_man_offers.offer
				-- Wise man loses offered
				wise_man_lose(offer)
				-- player gains offered, if trinket
				if trinkets[offer] then
					get_trinket(offer)
				else
				-- if advice
					menus.wise_man_thanks.advice = wise_advices[offer]
				end
				change_screen('wise_man_thanks')
			end
		},
		{ -- No button ; make a new offer
			x = .6, y = .75, w = .2, h = .2,
			content ='No',
			onclick = function() change_screen('wise_man_offers') end,
		},
		menu_elements.wise_man_picture,
		{ -- wise man text
			x = 0, y = .25, w = 1, h = .5,
			content = function()
				return 'Wise man offers: ' ..
				menus.wise_man_offers.offer_text ..
				'\n\nDo you accept?'
			end,
		},
	},
	wise_man_thanks = {
		onload = function(self)
			if self.advice then
				self.towrite = 'Hear this ' .. self.advice[1] .. '!\n\n' .. self.advice[2]
			else
				self.towrite = nil
			end
			self.advice = nil -- reset for the next time
		end,
		menu_elements.wise_man_picture,
		menu_elements.wise_man_says,
		{ -- wise man text
			x = 0, y = .25, w = 1, h = .5,
			content = function()
				return menus.wise_man_thanks.towrite or 'Thank you for your donation!'
			end
		},
		{ -- donation button
			x = 0, y = -.25, w = .5, h = .26,
			content ='Trade another item',
			onclick = function() change_screen('menu_objects_donate') end
		},
		{ -- play
			x = .5, y = -.25, w = .5, h = .25,
			content ='Goodbye!',
			onclick = function() change_screen('playfield') end
		},
	},
	-- about
	menu_about = {
		background = function()
			inplayfield(function (x, y, p)
				draw_floor(x - .5, y - .5)
			end)
		end,
		onload = function(self)
			self.page = 1
		end,
		menu_elements.play,
		menu_elements.exit_to_title,
		{ -- copyright
			x = .5, y = 0, w = .5, h = .25,
			content = 'Copyright 2007, 2019, 2020 Pajo\n<xpio@tut.by>'
		},
		{ -- po
			x = 0, y = 0, w = .25, h = .25,
			content = images.po,
		},
		{ -- ca
			x = .25, y = 0, w = .25, h = .25,
			content = images.ca,
		},
		{ -- text - refreshes bg which gets destroyed on resize
			x = 0, y = .25, w = 1, h = .5,
			content = function()
				local page = menus.menu_about.page
				if page == 1 then
					return 'This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.'
				elseif page == 2 then
					return 'This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.'
				elseif page == 3 then
					return 'You should find a copy of the GNU General Public License somewhere in this game. If not, see <https://www.gnu.org/licenses/>.'
				end
			end,
		},
		{ -- button: Copyright
			x = 0, y = .75, w = .33, h = .25,
			content = 'Copyright',
			onclick = function()
				menus.menu_about.page = 1
			end,
		},
		{ -- button: Warranty
			x = .33, y = .75, w = .34, h = .25,
			content = 'Warranty',
			onclick = function()
				menus.menu_about.page = 2
			end,
		},
		{ -- button: License
			x = .67, y = .75, w = .33, h = .25,
			content = 'License',
			onclick = function()
				menus.menu_about.page = 3
			end,
		},

	},
	-- title
	menu_main = {
		onload = function()
			-- pfcanvas will be used in foreground
			screen.pfcanvas:renderTo(draw_title)
		end,
		menu_elements.play,
		{
			x = .5, y = -.25, w = .25, h = .25,
			content = 'About',
			onclick = function() change_screen('menu_about') end,
		},
		{
			x = .75, y = -.25, w = .25, h = .25,
			content = 'Exit',
			onclick = function() love.event.push('quit') end,
		},
		{ -- title screen
			x = 0, y = 0, w = 1, h = 1,
			content = function()
				return {
					bmp = screen.pfcanvas,
					offsetx = 0, offsety = 0,
					scale = screen.playfieldsize
				}
			end
		},
		{
			x = 0, y = .0, w = .25, h = .25,
			content = 'Load',
			onclick = function() change_screen('menu_load') end,
		},
		{
			x = 0, y = .25, w = .25, h = .25,
			content = 'Save',
			onclick = function() change_screen('menu_save') end,
		},
		{
			x = .75, y = .5, w = .25, h = .25,
			content = 'Objects',
			onclick = function() change_screen('menu_objects') end,
		},
		{
			x = .75, y = .75, w = .25, h = .25,
			content = 'Options',
			onclick = function() change_screen('menu_settings') end,
		},
	}
}


-- menu mechanics


function get_absolute_dimensions(e)
	return
		e.x * screen.playfieldsize + screen.playfieldx,
		e.y * screen.playfieldsize + screen.playfieldy,
		e.w * screen.playfieldsize,
		e.h * screen.playfieldsize
end


function draw_menu_elements()
	local function draw_element(e, selected)
		if e.hidden then return end
		local x, y, w, h = get_absolute_dimensions(e)
		if selected then
			love.graphics.setColor(.5, 0, 1)
		else
			love.graphics.setColor(.1, .1, .1)
		end
		if e.onclick and not e.transparent then
			love.graphics.rectangle('fill', x, y, w, h)
			love.graphics.setColor(.5, .5, .5)
			love.graphics.rectangle('line', x + 2, y + 2, w - 4, h - 4)
		end
		love.graphics.setColor(1,1,1)
		local c
		if e.content then
			if type(e.content) == 'function' then
				local dx = 0
				if e.sweep_x then
					dx = (love.mouse.getPosition() - e.sweep_x)
				end
				c = e:content(dx)
			else
				c = e.content
			end
			if type(c) == 'string' then
				local lines = select(2, screen.defaultfont:getWrap(c, w - 4))
				local offsety = (h - screen.defaultfont:getHeight() * #lines) / 2
				love.graphics.printf(c, x + 2, y + 2 + offsety, w - 4 ,'center')
			elseif type(c) == 'table' and c.bmp then
				local function drawbmp(c)
					love.graphics.draw(c.bmp,
						x + c.offsetx * screen.tilesize, y + c.offsety * screen.tilesize,
						0, w / c.scale
					)
				end
				drawbmp(c)
			end
		end
	end
	for a = 1, #menus.current do
		draw_element(menus.current[a], menus.current.selected == a)
	end
end


function draw_menu()
	menus.current = menus.current or menus[current.screen]
	if menus.current.background then 
		screen.pfcanvas:renderTo(menus.current.background)
	end
	love.graphics.setColor(.5,.5,.5)
	if game.shuffled and not menus.current.background then
		draw_shuffled()
	else
		love.graphics.draw(screen.pfcanvas, screen.playfieldx, screen.playfieldy)
	end
	draw_menu_elements()
end


function menu_click(mx, my, sweep_end)
	if not menus.current then return end
	if sweep_end then
		for _, e in ipairs(menus.current) do
			if e.sweep_x then
				local dx = mx - e.sweep_x
				e.sweep_x = nil
				e.canvas = nil -- force redraw
				e:onsweep(dx)
			end
		end
	else -- not sweep_end: start sweep or normal click if no sweep defined
		for _, e in ipairs(menus.current) do
			if (not e.hidden) and isin(mx, my, get_absolute_dimensions(e)) then
				if e.onsweep then
					e.sweep_x = mx
				elseif e.onclick then
					e:onclick()
				end
			end
		end
	end
end


function menu_select(dx, dy)
	-- if nothing selected, select first
	local selected = menus.current.selected
	if not selected or not dx then
		menus.current.selected = 1
		return
	end

	-- search for suitable element in the desired direction
	-- start from the element' center, then check in the desired direction
	-- but with deviation along the opposite (passive; pas) axis
	local function searchelement(startx, starty, dx, dy)
		local RESOLUTION = 50 -- dots per playfield
		local DEVIATION = .5
		local start_act, start_pas, d_act = startx, starty, dx
		if dx == 0 then start_act, start_pas, d_act = starty, startx, dy end
		for act = start_act, start_act + d_act, d_act / RESOLUTION do
			for pas = 0, math.abs(start_act - act), 1 / RESOLUTION do
				for dir = - DEVIATION, DEVIATION, 2 * DEVIATION do
					for e = 1, #menus.current do
						if not (selected == e) then
							local mce = menus.current[e]
							local x, y = act, start_pas + pas * dir
							if dx == 0 then x, y = y, x end
							if
								mce.onclick and not mce.hidden and
								isin(x, y, mce.x, mce.y, mce.w, mce.h)
							then
								return e
							end
						end
					end
				end
			end
		end
	end

	-- search from center of current element
	local newselected = searchelement(
		menus.current[selected].x + menus.current[selected].w / 2,
		menus.current[selected].y + menus.current[selected].h / 2,
		dx or 0, dy or 0
	)

	if newselected then	menus.current.selected = newselected end
end


function menu_press()
	for a = 1, #menus.current do
		local e = menus.current[a]
		if e.onclick and (a == menus.current.selected) and not e.hidden then
			e:onclick()
			return
		end
	end
	-- if nothing selected, execute the first with onclick
	for i, e in ipairs(menus.current) do
		if e.onclick then
			e:onclick()
			return
		end
	end
end


function change_screen(newscreen, only_once)
	if only_once and game.shown_screens[newscreen] then return end
	game.shown_screens[newscreen] = true
	menus.current = nil
	current.screen = newscreen
	if menus[current.screen] and menus[current.screen].onload then menus[current.screen]:onload() end
end
