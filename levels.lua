--[[
This file is part of POCA - a puzzle game

POCA is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


tiletypes = {
	['0'] = {kind = 'tile', passive = true},
	['1'] = {kind = 'green', shape = 'x'},
	['2'] = {kind = 'green', shape = '+'},
	['3'] = {kind = 'yellow', shape = 'x'},
	['4'] = {kind = 'yellow', shape = '+'},
	['5'] = {kind = 'red', shape = 'x'},
	['6'] = {kind = 'red', shape = '+'},
	['7'] = {kind = 'regene', shape = 'x', sleep = 0},
	['8'] = {kind = 'regene', shape = '+', sleep = 0},
	['9'] = {
		kind = 'regene', shape = 'x',
		sleep = 1, sleeping_name = '7', passive = true
	},
	A = {
		kind = 'regene', shape = '+',
		sleep = 1, sleeping_name = '8', passive = true
	},
	C = { kind = 'click_hint', passive = true },
	D = {
		kind = 'blackload', shape = 'hole', passive = true,
		onclick = function () fade_to_level(playfield.skiplevel) end
	},
	H = {kind = 'loadred', shape = 'x'},
	I = {kind = 'loadred', shape = '+'},
	J = {kind = 'gift'},
	K = {kind = 'devilgift'},
	N = {kind = 'wiseman', onclick = function() change_screen('wise_man') end},
	O = {kind = 'pill', passive = true},
	P = {kind = 'purple', shape = 'x'},
	Q = {kind = 'purple', shape = '+'},
	R = {kind = 'killed', image = images.killed,
		afterclick = function(self)
			change_screen('story_corpse')
			self.afterclick = nil
		end
	},
	V = {kind = 'secrets_arrow', passive = true, dx = -1, dy = 0}, -- left
	W = {kind = 'secrets_arrow', passive = true, dx = 0, dy = -1}, -- up
	X = {kind = 'secrets_arrow', passive = true, dx = 1, dy = 0}, -- right
	Y = {kind = 'secrets_arrow', passive = true, dx = 0, dy = 1},  -- down
	Z = {kind = 'trinket', passive = true,
		afterclick = function(self)
			get_trinket(self.trinket)
			self.trinket = nil
			self.afterclick = nil
		end
	},
	a = {kind = 'memory', of = '2'}, -- green+
	b = {kind = 'memory', of = '1'}, -- greenx
	c = {kind = 'memory', of = 'I'}, -- loadred+
	d = {kind = 'memory', of = 'H'}, -- loadredx
	e = {kind = 'memory', of = '6'}, -- red+
	f = {kind = 'memory', of = '5'}, -- redx
	g = {kind = 'memory', of = 'A'}, -- regene+ sleep
	h = {kind = 'memory', of = '7'}, -- regenex
	i = {kind = 'memory', of = '9'}, -- regenex sleep
	j = {kind = 'memory', of = '4'}, -- yellow+
	k = {kind = 'memory', of = '3'}, -- yellowx
	l = {kind = 'memory', of = '8'}, -- regene+
	m = {kind = 'memory', of = 'P'}, -- purplex
	n = {kind = 'memory', of = 'Q'}, -- purple+
}


levels = {
[1] = { setup = '0000001000C00000', onload = function () change_screen('story_beginning', true) end, label = 'Apartment', sign = 'click' },
[2] = { setup = '00Z010C002000000', trinket = 'hints_book', sign = 'pick up item' },
[3] = { setup = '11C2002110002012', sign = 'menu' },
[4] = { setup = '0112002020100020' },
[5] = { setup = '1002120100211101' },
[6] = { setup = 'ZJ06CJ060J650J60', label = 'Block', trinket = 'gpl3', sign = 'license' },
[7] = { setup = '6656650050506005' },
[8] = { setup = '6660060006000000' },
[9] = { setup = '5556600650065566' },
[10] = { setup = '6666000006000600' },
[11] = { setup = '0000065605650656' },
[12] = { setup = '0Z00404000000400', trinket = 'cigarettes', label = 'City' },
[13] = { setup = '0003000003033030' },
[14] = { setup = '0040004004440000' },
[15] = { setup = '0040040003303003' },
[16] = { setup = '003403003J000300', gifts = 'Z', trinket = 'beastfang' },
[17] = { setup = '3003000033000303' },
[18] = { setup = '4000040040004404' },
[19] = { setup = '4404004004440004' },
[20] = { setup = '3003330000403044' },
[21] = { setup = '0000000100001818', label = 'Dig' },
[22] = { setup = '0020077000200007' },
[23] = { setup = 'Z020871200800201', trinket = 'woodenflower' },
[24] = { setup = 'R810C02009080201', sign = 'dead explorer' },
[25] = { setup = '7080020000000007' },
[26] = { setup = '8108000000200007' },
[27] = { setup = '0001063001503000', label = 'Elevation' },
[28] = { setup = '0000303033350303' },
[29] = { setup = '0003080004803000' },
[30] = { setup = 'C00C84180000H000', arrowgift = 'Z', trinket = 'rabbit', sign = 'traverse'},
[31] = { setup = '6DD68C6D060D3006', sign = 'passage', skiplevel = 'Elevation 4' },
[32] = { setup = '8000000018700502' },
[33] = { setup = '6004920653004442' },
[34] = { setup = '003001A700300000' },
[35] = { setup = '0406480080037060' },
[36] = { setup = '8606006006734030' },
[37] = { setup = '03D63560D6130030' },
[38] = { setup = 'IIIII5I5IIIII5I5', arrowgift = '6' },
[39] = { setup = '02119003R3019644' },
[40] = { setup = '6402206064264804' },
[41] = { setup = '00200401203J0006', gifts = 'D', skiplevel = 'Faydom 2' },
[42] = { setup = '4444465445644444' },
[43] = { setup = 'Z104020010004003', trinket = 'marbles', label = 'Monastery' },
[44] = { setup = '0700003003000070' },
[45] = { setup = '3360335656330533' },
[46] = { setup = '700003J00330P007', gifts = '3' },
[47] = { setup = '2000020055PJQ000', gifts = 'D', skiplevel = 'Faydom' },
[48] = { setup = '0002121124000020', label = 'Narrow' },
[49] = { setup = '3636636335355353' },
[50] = { setup = '3335300530033335' },
[51] = { setup = 'H0IH00H00530740H', arrowgift = '1' },
[52] = { setup = '400J000440030340', gifts = 'Z', trinket = 'matches' },
[53] = { setup = '200040030b100420', label = 'Overcharge' },
[54] = { setup = 'b0000k0000j0000b' },
[55] = { setup = '000j064006400708' },
[56] = { setup = '0b000ab00a0b0a00' },
[57] = { setup = 'jjjj0110jjjj0Z00', trinket = 'tinyheart' },
[58] = { setup = '44jjjj4jj4jj4j44' },
[59] = { setup = '0000aaa0lb800000' },
[60] = { setup = 'aa55al00la00al33' },
[61] = { setup = '0l006b60j60l6j60' },
[62] = { setup = 'QQPPQPQPPQPQPPQQ', label = 'Purple' },
[63] = { setup = '0n0nmmnn0nmmmnnZ', trinket = 'robotoy'},
[64] = { setup = 'Q00000Q0P0Q0P000' },
[65] = { setup = '0PQP0QmQ0PmP0QmQ' },
[66] = { setup = 'Q0mn0mZPQPm0Q0QQ', trinket = 'casette' },
[67] = { setup = '00P00Q0PP0P00QQ0' },
[68] = { setup = '00P0QPQ0mPPPmPnm' },
[69] = { setup = '00000P0PQ0000QP0' },
[70] = { setup = 'nnnnnnnnnnnnnnnn' },
[71] = { setup = 'Q0010PP02012Q0P0', label = 'Quicksand' },
[72] = { setup = '0P0PP5650P5000P0' },
[73] = { setup = '4003000700000008' },
[74] = { setup = '3n023j30jnn31n3P' },
[75] = { setup = 'mhj3j0k0Z907f0kn', trinket = 'gameglitch' },
[76] = { setup = '004I4HHIIHIH0IJI', gifts = 'Z', arrowgift = '3', trinket = 'pistol' },
[77] = { setup = 'Q65QQ5Q5QQ56Q565' },
[78] = { setup = '4mmPbnPn008P8h4a' },
[79] = { setup = '0CJ2cj74J8Jj4jJ8', sign = 'save!', gifts = 'Z000', arrowgift = 'e', trinket = 'photos' },
[80] = { setup = '8J8J7J7JJ878NJ8J', onload = function() setup_wise_man_battle() end, label = 'Remorse', skiplevel = 'Secrets' },
[81] = { setup = '00000J0J00D00J0Z', gifts='III', arrowgift = 'J', trinket = 'chocolate', skiplevel = 'Monastery 4', label = 'Faydom' },
[82] = { setup = '00D00J0JJ0J00J0J', gifts = '743337', skiplevel = 'Island 7' },
[83] = { setup = '40440K000C040004', onload = function() if game.underpill then events('pill off') end end, label = 'Genie', sign = 'strange box' },
[84] = { setup = '0004040J00J44J44', gifts = '444', label = 'Hive' },
[85] = { setup = '5500403065000510' },
[86] = { setup = '0000412030506780', label = 'Island' },
[87] = { setup = 'IIII278100601218', arrowgift = 'D' },
[88] = { setup = '4DI44HDDIDDID4HD', arrowgift = 'D', skiplevel = 'Island' },
[89] = { setup = '007800044200810Z', trinket = 'titmagazine' },
[90] = { setup = '0000002002J20020', gifts = 'N' },
[91] = { setup = '0JI00IH0IIII0IJI', gifts = '0Z', arrowgift = '3', trinket = 'toyhorse' },
[92] = { setup = '000J00008780J0J0', gifts = '634' },
[93] = { setup = '000J020100000502', gifts = 'D', skiplevel = 'Elevation 15' },
[94] = { setup = '0000683000001001' },
[95] = { setup = '0000070330300307', label = 'Jail' },
[96] = { setup = '10100J00P0P00000', gifts = 'O', label = 'Knockout' },
[97] = { setup = 'Z87Z807000080Z8Z', trinket = '1000points', label = 'Lighthouse' },
[98] = { setup = '1ZV453622Y014X3W', trinket = 'teleport_ur', onload = function() secrets_big(1, 1) end, label = 'Secrets' },
[99] = { setup = '2V1JJ2031W10J3Z2', gifts = 'XYJ', trinket = 'matches', onload = function() secrets_big(2, 1) end },
[100] = { setup = 'VW55VW6336YX35YX', onload = function() secrets_big(3, 1) end },
[101] = { setup = '46002XY00WV10035', onload = function() secrets_big(4, 1) end },
[102] = { setup = '3001Z0W00V8X50Y1', trinket = 'teleport_ul', onload = function() secrets_big(1, 2) end },
[103] = { setup = '01100530J16JJJJJ', gifts = 'JJVWYX', onload = function() secrets_big(2, 2) end },
[104] = { setup = '0464424WV0X64Y20', onload = function() secrets_big(3, 2) end },
[105] = { setup = '53W1333XV3304Y02', onload = function() secrets_big(4, 2) end },
[106] = { setup = '3X4V14306W6175Y4', onload = function() secrets_big(1, 3) end },
[107] = { setup = '53353VW53YX35335', onload = function() secrets_big(2, 3) end },
[108] = { setup = '12102408181JJJJJ', gifts = 'JVWXY', onload = function() secrets_big(3, 3) end },
[109] = { setup = '203J0303303JJJ2J', gifts = 'JVWXY', onload = function() secrets_big(4, 3) end },
[110] = { setup = '35W0535XV5350Y53', onload = function() secrets_big(1, 4) end },
[111] = { setup = '242Z4WX11VY40242', trinket = 'teleport_dl', onload = function() secrets_big(2, 4) end },
[112] = { setup = 'J18JZ4082563J25J', gifts = 'VWXY', trinket = 'rainbowring', onload = function() secrets_big(3, 4) end },
[113] = { setup = 'JJX5806400057JZ0', gifts = 'VWY', trinket = 'raregem', onload = function() secrets_big(4, 4) end },
last = "005P00316400Q200"
}

-- scan levels for purple and yellow arrows
-- (will be used to check if devil or purple applies when skipping levels)
for a = 1, #levels do
	levels[a].has_yellow, levels[a].has_purple = false, false
	local l = levels[a].setup .. (levels[a].gifts or '') .. (levels[a].arrowgift or '')
	for b = 1, #l do
		local c = string.sub(l, b, b)
		levels[a].has_purple = levels[a].has_purple or c == 'P' or c == 'Q' or c == 'm' or c == 'n'
		levels[a].has_yellow = levels[a].has_yellow or c == '3' or c == '4' or c == 'j' or c == 'k'
	end
end


level_labels = {}
--[[
 1 Apartment
 6 Block
12 City
21 Dig
27 Elevation
43 Monastery
48 Narrow
53 Overcharge
62 Purple
71 Quicksand
80 Remorse
81 Faydom
83 Genie
84 Hive
86 Island
95 Jail
96 Knockout
97 Lighthouse
98 Secrets

Level labels work both ways: a number returns the area name
and area name returns the first level of the area.
]]
for a = 1, #levels do
	if levels[a].label then
		level_labels[a] = levels[a].label
		level_labels[levels[a].label] = a
	end
end
setmetatable(level_labels, {__index = function (table, key)
	if type(key) == 'number' and key > 1 and not rawget(table, key) then return table[key - 1] end
end})


function get_rotated_name(tilename)
	--if tiletypes[tilename].kind == 'memory' then
	--	return get_memory_name(get_rotated_name(tiletypes[tilename].of))
	--end
	local shape
	if tiletypes[tilename].shape == '+' then shape = 'x' else shape = '+' end
	for name, v in pairs(tiletypes) do
		if v.kind == tiletypes[tilename].kind and v.shape == shape then
			return name
		end
	end
end


function get_memory_name(tilename)
	for name, v in pairs(tiletypes) do
		if v.of == tilename then return name end
	end
end


function shape_to_arrows(tileshape)
	-- arrows at 8 positions, at angles from 0 to 3pi /2
	if tileshape == '+' then
		return {0, false, 0, false, 0, false, 0, false}
	elseif tileshape == 'x' then
		return {false, 0, false, 0, false, 0, false, 0}
	end
end


function recognizeink(chr, recursion)
	if (recursion or 0) > 10 then return end
	chr = chr or '0' -- default: floor tile, used in opening of gifts
	local tile = {} -- this should be copied, not referenced, because it gets modified
	for k, v in pairs(tiletypes[chr]) do tile[k] = v end
	tile.name = chr
	-- modifications
	if (not tile.sleep) or (tile.sleep == 0) then
		tile.arrows = shape_to_arrows(tile.shape)
	else
		tile.arrows = {}
	end
	if tile.kind == 'memory' then
		tile.memory = recognizeink(tile.of, (recursion or 0) + 1)
		tile.passive = true
	elseif tile.kind == 'loadred' then
		tile.arrowgift = recognizeink(levels[game.currentlevel].arrowgift, (recursion or 0) + 1)
	elseif tile.kind == 'trinket' then
		if had_trinket(levels[game.currentlevel].trinket) then
			tile = recognizeink() -- plain tile, remove trinket
		else
			tile.trinket = levels[game.currentlevel].trinket
		end
	elseif tile.kind == 'secrets_arrow' then
		tile.onclick = function () secrets_arrow(tile.dx, tile.dy) end
		tile.used = game.secrets_arrows_used[secrets_arrow_number(tile.dx, tile.dy)]
	elseif tile.kind == 'wiseman' and game.solved_levels[level_labels['Remorse']] then
		tile = recognizeink('D')
	elseif tile.kind == 'click_hint' and game.seen_levels[game.currentlevel] then
		tile = recognizeink()
	end
	return tile
end


function fill_gifts(currentlevel)
	local giftpos = 1
	inplayfield(function (x, y, p)
		if playfield[x][y].kind == 'gift' or playfield[x][y].kind == 'devilgift' then
			if levels[currentlevel].gifts then
				playfield[x][y].gift = string.sub(levels[currentlevel].gifts, giftpos, giftpos)
			end
			if not playfield[x][y].gift or playfield[x][y].gift == '' then playfield[x][y].gift = '0' end
			giftpos = giftpos + 1
		end
	end)
end


function setup_big_playfield()
	game.purple, current.temppurple = false, false
	big_playfield.clickedx, big_playfield.clickedy = false, false
	local c, mc, pos
	for y = 1, 4 do for x = 1, 4 do
		pos = (y - 1) * 4 + x
		c = string.sub(levels.last, pos, pos)
		big_playfield[x][y] = recognizeink(c)
	end end
end


function get_initial_tile(currentlevel, x, y)
	local c, mc, pos
	pos = (y - 1) * 4 + x
	c = string.sub(levels[currentlevel].setup, pos, pos)
	-- modifications
	if game.purple then
		if c == 'P' then c = 'Q' elseif c == 'Q' then c = 'P' end
		if c == 'm' then c = 'n' elseif c == 'n' then c = 'm' end
	end
	if game.underpill then
		c = get_memory_name(c) or c
	end
	--
	return recognizeink(c)
end


function setuplevel(currentlevel)
	playfield.afterdeath_phase = nil
	current.died = false
	current.showkill = false
	game.shuffled = false
	if currentlevel and levels[currentlevel] then --load only existing levels
		game.currentlevel = currentlevel
	else
		currentlevel = game.currentlevel
	end
	current.symbolic = (currentlevel >= level_labels['Secrets'])
	current.temppurple = game.purple
	playfield.skiplevel = nil
	playfield.goto = nil -- teleport arrows in Secrets use this
	current.phase = nil -- will reset to 'play'
	inplayfield(function (x, y, p)
		playfield[x][y] = get_initial_tile(currentlevel, x, y)
	end)
	-- set gift property of visible gifts as set in level table
	-- gifts generated during the game are not taken into account
	-- but there is only one level which generates gifts (81),
	-- and it is not affected because gifts are present at the beginning
	fill_gifts(currentlevel)
	secrets_check_gifts()
	playfield.clickedx, playfield.clickedy = false, false
	playfield.wisemanx, playfield.wisemany = false, false
	playfield.bgtile = nil
	playfield.trinket = levels[game.currentlevel].trinket
	-- skiplevel
	if levels[game.currentlevel].skiplevel then
		local area, level
		local f = string.find(levels[game.currentlevel].skiplevel, ' ')
		if f then
			area = string.sub(levels[game.currentlevel].skiplevel, 1, f - 1)
			level = string.sub(levels[game.currentlevel].skiplevel, f + 1)
		else
			area = levels[game.currentlevel].skiplevel
			level = 1
		end
		playfield.skiplevel = level_labels[area] + level - 1
	end
	-- onload
	if levels[game.currentlevel].onload then
		levels[game.currentlevel].onload()
	end
	-- update list of seen levels
	update_seen_levels(
		game.seen_levels, game.currentlevel,
		game.devil, game.underpill, game.purple,
		levels[game.currentlevel].has_yellow,
		levels[game.currentlevel].has_purple
	)
	paint_level()
	if screen then screen.minimap:renderTo(draw_minimap) end
	play_music(level_labels[game.currentlevel])
	save(game, 'autosave')
end


function update_seen_levels(list, level, devil, underpill, purple, has_yellow, has_purple)
	-- list can be game.solved_levels or game.seen_levels
	list[level] = list[level] or {}
	list[level][seen_levels_score(devil, underpill, purple)] = true
	-- if the level has no yellow arrows,
	-- if seen/solved as devil, mark as seen/solved with no devil and vice versa
	if not has_yellow then
		update_seen_levels(list, level, not devil, underpill, purple, true, has_purple)
	end
	-- if the level has no purple arrows,
	-- mark as seen/solved for the other purple variant as well
	if not has_purple then
		update_seen_levels(list, level, devil, underpill, not purple, has_yellow, true)
	end
end


function pretty_name(level, short)
	if not level then return '' end
	local area = level_labels[level]
	local order = level - level_labels[area] + 1
	if short then
		return string.sub(area, 1, 1) .. order
	elseif order == 1 then
		return area
	else
		return area .. ' ' .. order
	end
end


-- creates index based on current game state
-- used to create and check lists of solved and seen levels
function seen_levels_score(devil, underpill, purple)
	return (devil and 1 or 0) + (underpill and 2 or 0) + (purple and 4 or 0)
end


-- checks if skipping levels backwards or forwards is allowed
function traversable(l)
	local next_level = game.currentlevel + l
	if next_level < 1 then return false end
	-- Faydom-Lighthouse segment is not continuous with other levels,
	-- being between Remorse and Secrets, isolated by
	-- disallowing traversing to Remorse and by Lighthouse being unsolvable.
	-- Remorse is automatically entered by solving the previous level and
	-- it can be exited only by teleport, as it's unsolvable.
	-- Secrets are entered by teleport and can't be exited
	if
		level_labels[game.currentlevel] == 'Remorse' or
		level_labels[next_level] == 'Remorse' or
		level_labels[game.currentlevel] == 'Secrets'
	then
		return false
	end
	-- prevent going forwards (-1 for underpill, +1 otherwise)
	-- at levels not previously solved in the same state
	-- however, if level is already solved,
	-- allow going forwards without any further checks
	local forwards = (l == -1 and game.underpill) or (l == 1 and not game.underpill)
	if forwards then
		return game.solved_levels[game.currentlevel] and
			game.solved_levels[game.currentlevel][seen_levels_score(
				game.devil, game.underpill, game.purple
			)]
	end
	-- only going backwards below
	-- only levels previously visited in the same state should be reachable
	-- by going backwards, but allow going backwards regardless of the purple state
	return game.seen_levels[next_level] and (
			game.seen_levels[next_level][seen_levels_score(
				game.devil, game.underpill, game.purple
			)] or
			game.seen_levels[next_level][seen_levels_score(
				game.devil, game.underpill, not game.purple
			)]
		)
end
