--[[
    Pocket Explosions, a small game of memory and imagination
    Copyright (C) 2017, 2020 Pajo <xpio at tut dot by>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]


pe = {

	MAP_WIDTH = 4,
	MAP_HEIGHT = 4,
	COLOR_FG = {1, .016, .05},
	COLOR_BOARD = {0, 0, .0625},
	COLOR_HELPFIRE = {.17, .17, .26},
	COLOR_BG = {0, 0, 0},
	COLOR_TEXT = {1, 1, 1},
	font = love.graphics.newImageFont(
		"gfx/font.png", "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ!.$ "
	),


	hijacked_love_functions = {
		'draw', 'update', 'keypressed', 'mousepressed', 'resize'
	}

}


function pe.update(dt) -- hijacked function
	if pe.game_mode ~= 'play' and (love.timer.getTime() > pe.help_frame_ends) then
		pe.help_show(pe.help_frame + 1)
		pe.canvas:renderTo(pe.drawscreen)
	end
	love.timer.sleep(0.05)
end


function pe.draw() -- hijacked function
	love.graphics.setColor(1, 1, 1)
	love.graphics.draw(pe.canvas, 0, 0)
end


function pe.main(self)
	if current.music then current.music:stop() end
	self.resetgame()
	self.hiscore = tonumber(love.filesystem.read('pe_score') or 0)
	self.help_show(1)
	self.resize()
	hijack(self)
end


function pe.resize() -- hijacked function
	local function recalculate(w, h, mw, mh)
		pe.box_size = math.min(w / mw, h / mh)
		pe.margin_left = (w - pe.box_size * mw) / 2
		pe.margin_top = (h - pe.box_size * mh) / 2
	end
	local w, h, mw, mh
	w = love.graphics.getWidth()
	h = love.graphics.getHeight()
	if pe.game_mode == 'help' then mw, mh = 4, 4 else mw, mh = pe.MAP_WIDTH, pe.MAP_HEIGHT end
	pe.rotatedisplay = (w > h and mw < mh) or (h > w and mh < mw)
	if pe.rotatedisplay then w, h = h, w end
	recalculate(w, h, mw, mh)
	-- reserve space for title and help text
	if pe.game_mode == 'help' and pe.margin_top < 60 then
	-- space below bottom margin is the same as margin_top
	-- before recalculation
		recalculate(w, h - 80, mw, mh)
		pe.margin_top = pe.margin_top + 20
	end
	pe.box_padding = pe.box_size * 0.06
	pe.draw_box_size = pe.box_size - pe.box_padding - pe.box_padding / (mw - 1)
	for a = 1, #pe.buttons do for b = 1, #pe.buttons[a] do
		local draw_box_x = pe.margin_left + (a - 1) * pe.box_size + pe.box_padding - pe.box_padding / mw * (a - 1)
		local draw_box_y = pe.margin_top + (b - 1) * pe.box_size + pe.box_padding - pe.box_padding / mh * (b - 1)
		pe.buttons[a][b].x, pe.buttons[a][b].y = draw_box_x, draw_box_y
	end end
	-- hiscore and help/exit coordinates
	pe.font_scale = math.max(1, pe.box_size / 100)
	local fw = pe.font:getWidth('HELP') * pe.font_scale
	local fh = pe.font:getHeight() * pe.font_scale
	pe.control_x2 = w - fw - math.max(
		pe.box_padding, pe.margin_left / 2 - 2 * pe.box_padding
	)
	pe.control_y1 = math.max((pe.margin_top - fh) / 2, pe.box_padding)
	pe.control_y2 = h - pe.control_y1 - fh

	pe.canvas = love.graphics.newCanvas()
	pe.canvas:renderTo(pe.drawscreen)
end


function pe.resetgame()
	if not pe.buttons then
		pe.buttons = {}
		for a = 1, pe.MAP_WIDTH do
			pe.buttons[a] = {}
			for b = 1, pe.MAP_HEIGHT do
				pe.buttons[a][b] = {}
			end
		end
	end
	for a = 1, pe.MAP_WIDTH do for b = 1, pe.MAP_HEIGHT do
			pe.buttons[a][b].fire = false
		end
	end
	pe.jewel = {x = math.random(pe.MAP_WIDTH), y = math.random(pe.MAP_HEIGHT)}
	pe.score = 0
	pe.fires = {}
	pe.player = {}
	pe.gameover = false
	pe.help_frame = 1
end


function pe.drawjewel(x, y, fire)
	if fire then
		love.graphics.setColor(pe.COLOR_FG)
	else
		love.graphics.setColor(pe.COLOR_BOARD)
	end
	love.graphics.polygon("fill",
		x + pe.draw_box_size / 2,
		y + pe.box_padding,
		x + pe.draw_box_size - pe.box_padding,
		y + pe.draw_box_size / 2,
		x + pe.draw_box_size / 2,
		y + (pe.draw_box_size - pe.box_padding),
		x + pe.box_padding,
		y + pe.draw_box_size / 2
	)
end


function pe.drawplayer(x, y, fire)
	if fire then
		love.graphics.setColor(pe.COLOR_FG)
	else
		love.graphics.setColor(pe.COLOR_BOARD)
	end
	love.graphics.circle("fill",
		x + pe.draw_box_size / 2, y  + pe.draw_box_size / 2,
		pe.draw_box_size / 2 - pe.box_padding
	)
end


function pe.drawfire(x, y, fire, color)
	love.graphics.setColor(pe.COLOR_FG)
	if not fire then
		love.graphics.rectangle("fill", x, y, pe.draw_box_size, pe.draw_box_size)
	elseif color then --alternate fire
		love.graphics.setColor(color)
		love.graphics.rectangle("fill", x, y, pe.draw_box_size, pe.draw_box_size)
	end
end


function pe.drawpointer(x, y)
	love.graphics.setColor(pe.COLOR_FG[1], pe.COLOR_FG[2], pe.COLOR_FG[3], .5)
	love.graphics.rectangle("fill", x - pe.box_padding, y - pe.box_padding, pe.draw_box_size + 2 * pe.box_padding, pe.draw_box_size + 2 * pe.box_padding)
	love.graphics.setColor(pe.COLOR_BOARD)
	love.graphics.rectangle("fill", x, y, pe.draw_box_size, pe.draw_box_size)
end


function pe.readbutton(x, y)
	if pe.rotatedisplay then
		return pe.buttons[x][y].y, pe.buttons[x][y].x, pe.buttons[x][y].fire
	else
		return pe.buttons[x][y].x, pe.buttons[x][y].y, pe.buttons[x][y].fire
	end
end


function pe.drawscreen()
	love.graphics.clear(pe.COLOR_BG)
	local x, y, fire, mw, mh, ml, mt
	ml, mt = pe.margin_left, pe.margin_top
	if pe.game_mode == 'help' then mw, mh = 4, 4 else mw, mh = pe.MAP_WIDTH, pe.MAP_HEIGHT end
	if pe.rotatedisplay then
		ml, mt = mt, ml
		mw, mh = mh, mw
	end
	love.graphics.setColor(pe.COLOR_BOARD)
	love.graphics.rectangle("fill", ml, mt, pe.box_size * mw, pe.box_size * mh)
	if pe.game_mode == 'play' then
		-- pointer
		if pe.pointer then pe.drawpointer(pe.readbutton(pe.pointer.x, pe.pointer.y)) end
		-- fire
		for a = 1, #pe.buttons do for b = 1, #pe.buttons[a] do
			x, y, fire = pe.readbutton(a, b)
			pe.drawfire(x, y, fire)
		end end
		-- jewel
		pe.drawjewel(pe.readbutton(pe.jewel.x, pe.jewel.y))
		-- player
		if pe.player.x then pe.drawplayer(pe.readbutton(pe.player.x, pe.player.y))	end
		-- score and exit
		local toprint
		if not pe.player.x then toprint = 'HI ' .. pe.hiscore .. '$'
		elseif pe.score > pe.hiscore then toprint = pe.score .. ' !'
		else toprint = pe.score end
		if pe.gameover then toprint = toprint .. '$' end
		love.graphics.setColor(pe.COLOR_TEXT)
		love.graphics.setFont(pe.font)
		local x1 = math.max(
			(pe.margin_left - pe.font:getWidth(toprint) * pe.font_scale) / 2,
			pe.box_padding
		)
		love.graphics.print(toprint, x1, pe.control_y1, 0 , pe.font_scale)
		love.graphics.print('EXIT', pe.control_x2, pe.control_y1, 0 , pe.font_scale)
		love.graphics.print('HELP', pe.control_x2, pe.control_y2, 0 , pe.font_scale)

	elseif pe.game_mode == 'help' then
		pe.drawhelpframe(pe.help_frame)
	end
end


function pe.keypressed(k) -- hijacked function
	if k == 'q' or k == 'escape' then
		pe.quit()
		return
	end
	if not pe.pointer then
		pe.pointer = {}
		if pe.player.x then
			pe.pointer.x, pe.pointer.y = pe.player.x, pe.player.y
		else
			pe.pointer.x, pe.pointer.y = math.floor(pe.MAP_WIDTH / 2), math.floor(pe.MAP_HEIGHT / 2)
		end
	end
	if pe.game_mode ~= 'play' then
		pe.game_mode = 'play'
		pe.resetgame()
		pe.resize()
		return
	end
	if pe.pointer and (k == 'return' or k == 'space') then
		pe.nextturn(pe.pointer.x, pe.pointer.y)
	elseif
		k == 'f1' then
		pe.game_mode = 'help'
		pe.resize()
		pe.help_show(1)
	elseif
		k == 'r' then
		pe.resetgame(1)
	end
	local r, l, d, u = k == 'right', k == 'left', k == 'down', k == 'up'
	r, l, d, u = r or k == 'l', l or k == 'h', d or k == 'j', u or k == 'k'
	if pe.rotatedisplay then r, l, d, u = d, u, r, l end
	if r then pe.pointer.x = pe.pointer.x % pe.MAP_WIDTH + 1
	elseif l then pe.pointer.x = (pe.pointer.x + pe.MAP_WIDTH - 2) % pe.MAP_WIDTH + 1
	elseif d then pe.pointer.y = pe.pointer.y % pe.MAP_HEIGHT + 1
	elseif u then pe.pointer.y = (pe.pointer.y + pe.MAP_HEIGHT - 2) % pe.MAP_HEIGHT + 1 end
	pe.canvas:renderTo(pe.drawscreen)
end


function pe.nextfire(x, y)
	-- place in array = fire size
	local function drawfires()
		local function invertbutton(x, y)
			if pe.buttons[x] and pe.buttons[x][y] then
				pe.buttons[x][y].fire = not pe.buttons[x][y].fire
			end
		end
		-- following algo is good only for size 2+ fires, so
		-- invert the single-tile fire first
		if pe.fires[1] then invertbutton(pe.fires[1].x, pe.fires[1].y) end
		for size = 2, #pe.fires do
			local centerx, centery = pe.fires[size].x, pe.fires[size].y
			for x = centerx - (size - 1), centerx + (size - 1) do
				invertbutton(x, centery - (size - 1))
				invertbutton(x, centery + (size - 1))
			end
			for y = centery - (size - 2), centery + (size - 2) do
				invertbutton(centerx - (size - 1), y)
				invertbutton(centerx + (size - 1), y)
			end
		end
	end
	drawfires() -- delete old fires
	-- grow array until biggest fires exit screen
	if #pe.fires < math.max(pe.MAP_WIDTH, pe.MAP_HEIGHT) then
		pe.fires[#pe.fires + 1] = pe.fires[#pe.fires]
	end
	-- promote fires towards the end of array (the biggest one disappears)
	for a = #pe.fires, 0, -1 do
		pe.fires[a] = pe.fires[a - 1]
	end
	pe.fires[0] = {x = x, y = y}
	drawfires()
end


function pe.nextjewel()
	-- create jewel anywhere, save for last tile
	local nextjewel = math.random(pe.MAP_WIDTH * pe.MAP_HEIGHT - 1)
	-- if same as player then last tile
	if nextjewel == pe.player.x + (pe.player.y - 1) * pe.MAP_WIDTH then
		nextjewel = pe.MAP_WIDTH * pe.MAP_HEIGHT
	end
	pe.jewel.x, pe.jewel.y = (nextjewel - 1) % pe.MAP_WIDTH + 1, math.ceil(nextjewel / pe.MAP_WIDTH)
end


function pe.nextturn(x, y)
	if pe.gameover then
		pe.hiscore = math.max(pe.hiscore, pe.score)
		pe.resetgame()
		return
	end
	pe.nextfire(x, y)
	pe.player.x, pe.player.y = x, y
	if pe.player.x == pe.jewel.x and pe.player.y == pe.jewel.y then
		play_sound('tone')
		pe.score = pe.score + 1
		pe.nextjewel()
	-- penalty for not getting jewel
	elseif not pe.buttons[pe.jewel.x][pe.jewel.y].fire then
		pe.score = math.max(0, pe.score - 1)
		play_sound('backwards')
	end
	-- life or death
	pe.gameover = pe.buttons[pe.player.x][pe.player.y].fire
	if pe.gameover then
		play_sound('dead')
	else
		play_sound('click')
	end
end


function pe.mousepressed(x, y, button) -- hijacked function
	if pe.game_mode ~= 'play' then
		pe.game_mode = 'play'
		pe.resetgame()
		pe.resize()
		return
	end
	-- check button clicks
	if
		isin(x, y,
			pe.control_x2, pe.control_y1,
			pe.font:getWidth('HELP') * pe.font_scale,
			pe.font:getHeight() * pe.font_scale
		)
	then
		pe.quit()
		return
	elseif
		isin(x, y,
			pe.control_x2, pe.control_y2,
			pe.font:getWidth('HELP') * pe.font_scale,
			pe.font:getHeight() * pe.font_scale
		)
	then
		pe.game_mode = 'help'
		pe.resize()
		pe.help_show(1)
	end
	-- playfield click
	pe.pointer = false
	local function inside(p, q)
		return p >= q and p <= q + pe.draw_box_size
	end
	for a = 1, #pe.buttons do for b = 1, #pe.buttons[a] do
		local bx, by = pe.buttons[a][b].x, pe.buttons[a][b].y
		if pe.rotatedisplay then bx, by = by, bx end
		if inside (x, bx) and inside(y, by) then
			pe.nextturn(a, b)
		end
	end end
	pe.canvas:renderTo(pe.drawscreen)
end


function pe.quit()
	love.filesystem.write('pe_score', pe.hiscore)
	release(pe)
	love.resize()
end


pe.slides_text = {
	"1. CLICK TO PICK UP $",
	"2. EVERY STEP STARTS AN EXPLOSION THAT SPREADS OUTWARDS",
	"3. EXPLOSIONS CANCEL EACH OTHER",
	"4. DO NOT STEP INTO EXPLOSION" ,
	"5. PICK UP $ WHENEVER YOU CAN OR SCORE DECREASES"
}

pe.slides = {
-- frame 1
	{
		board = {
			{0,0,0,0},
			{0,0,0,0},
			{0,4,0,0},
			{0,0,0,0}
		},
		text = 1,
		duration = 2500
	},
-- frame 2
	{
		board = {
			{0,0,0,0},
			{0,0,0,0},
			{0,2,0,0},
			{0,0,0,0}
		},
		text = 1,
		duration = 1000
	},
-- frame 3
	{
		board = {
			{0,0,0,0},
			{0,0,0,0},
			{0,2,0,0},
			{4,0,0,0}
		},
		text = 1,
		duration = 400
	},
-- frame 4
	{
		board = {
			{0,0,0,0},
			{0,0,0,0},
			{0,0,0,0},
			{2,0,0,0}
		},
		text = 1,
		duration = 800
	},
-- frame 5
	{
		board = {
			{4,0,0,0},
			{0,0,0,0},
			{0,0,0,0},
			{2,0,0,0}
		},
		text = 1,
		duration = 400
	},
-- frame 6
	{
		board = {
			{2,0,0,0},
			{0,0,0,0},
			{0,0,0,0},
			{0,0,0,0}
		},
		text = 1,
		duration = 800
	},
-- frame 7
	{
		board = {
			{2,0,0,0},
			{0,0,0,0},
			{0,0,0,0},
			{0,0,4,0}
		},
		text = 1,
		duration = 400
	},
-- frame 8
	{
		board = {
			{0,0,0,0},
			{0,0,0,0},
			{0,0,0,0},
			{0,0,2,0}
		},
		text = 1,
		duration = 2000
	},
-- frame 9
	{
		board = {
			{0,0,0,0},
			{0,0,0,0},
			{0,0,0,0},
			{0,0,2,0}
		},
		text = 2,
		duration = 3000
	},
-- frame 10
	{
		board = {
			{0,0,0,0},
			{0,0,0,0},
			{0,0,0,0},
			{0,0,1,0}
		},
		text = 2,
		duration = 350
	},
-- frame 11
	{
		board = {
			{0,0,0,0},
			{0,0,0,0},
			{0,1,1,1},
			{0,1,0,1}
		},
		text = 2,
		duration = 350
	},
-- frame 12
	{
		board = {
			{0,0,0,0},
			{1,1,1,1},
			{1,0,0,0},
			{1,0,0,0}
		},
		text = 2,
		duration = 350
	},
-- frame 13
	{
		board = {
			{1,1,1,1},
			{0,0,2,0},
			{0,0,0,0},
			{0,0,0,0}
		},
		text = 2,
		duration = 350
	},
-- frame 14
	{
		board = {
			{0,0,0,0},
			{0,0,2,0},
			{0,0,0,0},
			{0,0,0,0}
		},
		text = 2,
		duration = 600
	},
-- frame 15
	{
		board = {
			{0,0,0,0},
			{0,0,7,0},
			{0,0,0,0},
			{0,0,0,0}
		},
		text = 2,
		duration = 350
	},
-- frame 16
	{
		board = {
			{0,7,7,7},
			{0,7,0,7},
			{0,7,7,7},
			{0,0,0,0}
		},
		text = 2,
		duration = 350
	},
-- frame 17
	{
		board = {
			{7,0,0,0},
			{7,0,0,0},
			{7,0,0,0},
			{7,7,7,7}
		},
		text = 2,
		duration = 350
	},
-- frame 18
	{
		board = {
			{0,0,0,0},
			{0,0,0,0},
			{0,0,0,0},
			{0,0,0,0}
		},
		text = 2,
		duration = 500
	},
-- frame 19
	{
		board = {
			{0,0,0,0},
			{0,0,0,0},
			{0,0,0,0},
			{0,0,0,0}
		},
		text = 3,
		duration = 2000
	},
-- frame 20
	{
		board = {
			{0,0,0,0},
			{0,0,0,0},
			{0,0,0,0},
			{2,0,0,0}
		},
		text = 3,
		duration = 400
	},
-- frame 21
	{
		board = {
			{0,0,0,0},
			{0,0,2,0},
			{0,0,0,0},
			{1,0,0,0}
		},
		text = 3,
		duration = 400
	},
-- frame 22
	{
		board = {
			{0,0,0,0},
			{0,0,7,0},
			{1,1,0,0},
			{0,1,0,0}
		},
		text = 3,
		duration = 400
	},
-- frame 23.1
	{
		board = {
			{0,7,7,7},
			{1,6,1,7},
			{0,7,6,7},
			{0,0,1,0}
		},
		text = 3,
		duration = 500
	},
-- frame 23.2
	{
		board = {
			{0,7,7,7},
			{1,0,1,7},
			{0,7,0,7},
			{0,0,1,0}
		},
		text = 3,
		duration = 500
	},
-- frame 23.3
	{
		board = {
			{0,7,7,7},
			{1,6,1,7},
			{0,7,6,7},
			{0,0,1,0}
		},
		text = 3,
		duration = 500
	},
-- frame 23.4
	{
		board = {
			{0,7,7,7},
			{1,0,1,7},
			{0,7,0,7},
			{0,0,1,0}
		},
		text = 3,
		duration = 500
	},
-- frame 24.1
	{
		board = {
			{6,1,1,1},
			{7,0,0,1},
			{7,0,0,1},
			{7,7,7,6}
		},
		text = 3,
		duration = 500
	},
-- frame 24.2
	{
		board = {
			{0,1,1,1},
			{7,0,0,1},
			{7,0,0,1},
			{7,7,7,0}
		},
		text = 3,
		duration = 500
	},
-- frame 24.3
	{
		board = {
			{6,1,1,1},
			{7,0,0,1},
			{7,0,0,1},
			{7,7,7,6}
		},
		text = 3,
		duration = 500
	},
-- frame 24.4
	{
		board = {
			{0,1,1,1},
			{7,0,0,1},
			{7,0,0,1},
			{7,7,7,0}
		},
		text = 3,
		duration = 500
	},
-- frame 25
	{
		board = {
			{0,0,0,0},
			{0,0,0,0},
			{0,0,0,0},
			{0,0,0,0}
		},
		text = 4,
		duration = 2000
	},
-- frame 26
	{
		board = {
			{0,2,0,0},
			{0,0,0,0},
			{0,0,0,0},
			{0,0,0,0}
		},
		text = 4,
		duration = 500
	},
-- frame 27
	{
		board = {
			{0,1,0,0},
			{0,0,0,0},
			{0,0,2,0},
			{0,0,0,0}
		},
		text = 4,
		duration = 500
	},
-- frame 28
	{
		board = {
			{1,0,1,0},
			{1,1,1,2},
			{0,0,1,0},
			{0,0,0,0}
		},
		text = 4,
		duration = 500
	},
-- frame 29
	{
		board = {
			{0,0,0,1},
			{0,1,1,1},
			{1,0,1,0},
			{0,3,1,1}
		},
		text = 4,
		duration = 3000
	},
-- frame 30
	{
		board = {
			{4,4,4,4},
			{4,4,4,4},
			{4,4,4,4},
			{4,4,4,4}
		},
		text = 5,
		duration = 4000
	},
-- frame 31
	{
		board = {
			{4,4,4,4},
			{4,4,4,4},
			{4,4,4,4},
			{4,4,4,0}
		},
		text = 5,
		duration = 100
	},
-- frame 32
	{
		board = {
			{4,4,4,4},
			{4,4,4,4},
			{4,4,4,0},
			{4,4,4,0}
		},
		text = 5,
		duration = 100
	},
-- frame 33
	{
		board = {
			{4,4,4,4},
			{4,4,4,0},
			{4,4,4,0},
			{4,4,4,0}
		},
		text = 5,
		duration = 100
	},
-- frame 34
	{
		board = {
			{4,4,4,0},
			{4,4,4,0},
			{4,4,4,0},
			{4,4,4,0}
		},
		text = 5,
		duration = 100
	},
-- frame 35
	{
		board = {
			{4,4,0,0},
			{4,4,4,0},
			{4,4,4,0},
			{4,4,4,0}
		},
		text = 5,
		duration = 100
	},
-- frame 36
	{
		board = {
			{4,0,0,0},
			{4,4,4,0},
			{4,4,4,0},
			{4,4,4,0}
		},
		text = 5,
		duration = 100
	},
-- frame 37
	{
		board = {
			{0,0,0,0},
			{4,4,4,0},
			{4,4,4,0},
			{4,4,4,0}
		},
		text = 5,
		duration = 100
	},
-- frame 38
	{
		board = {
			{0,0,0,0},
			{0,4,4,0},
			{4,4,4,0},
			{4,4,4,0}
		},
		text = 5,
		duration = 100
	},
-- frame 39
	{
		board = {
			{0,0,0,0},
			{0,4,4,0},
			{0,4,4,0},
			{4,4,4,0}
		},
		text = 5,
		duration = 100
	},
-- frame 40
	{
		board = {
			{0,0,0,0},
			{0,4,4,0},
			{0,4,4,0},
			{0,4,4,0}
		},
		text = 5,
		duration = 100
	},
-- frame 41
	{
		board = {
			{0,0,0,0},
			{0,4,4,0},
			{0,4,4,0},
			{0,0,4,0}
		},
		text = 5,
		duration = 100
	},
-- frame 42
	{
		board = {
			{0,0,0,0},
			{0,4,4,0},
			{0,4,4,0},
			{0,0,0,0}
		},
		text = 5,
		duration = 100
	},
-- frame 43
	{
		board = {
			{0,0,0,0},
			{0,4,4,0},
			{0,4,0,0},
			{0,0,0,0}
		},
		text = 5,
		duration = 100
	},
-- frame 44
	{
		board = {
			{0,0,0,0},
			{0,4,0,0},
			{0,4,0,0},
			{0,0,0,0}
		},
		text = 5,
		duration = 100
	},
-- frame 45
	{
		board = {
			{0,0,0,0},
			{0,0,0,0},
			{0,4,0,0},
			{0,0,0,0}
		},
		text = 5,
		duration = 100
	},
-- frame 46
	{
		board = {
			{0,0,0,0},
			{0,0,0,0},
			{0,0,0,0},
			{0,0,0,0}
		},
		text = 5,
		duration = 1000
	},
-- frame 47
	{
		board = {
			{1,1,1,1},
			{1,1,1,1},
			{1,1,1,1},
			{1,1,1,1}
		},
		text = 5,
		duration = 500
	},
}


function pe.help_show(frame)
	pe.game_mode = 'help'
	if not pe.slides[frame] then
		pe.help_frame = 1
	else
		pe.help_frame = frame
	end
	pe.help_frame_ends = love.timer.getTime() + pe.slides[pe.help_frame].duration / 1000
	if pe.box_size then pe.canvas:renderTo(pe.drawscreen) end -- if ever resized
end

--[[
ITEM_NONE = 0
ITEM_NONE_FIRE = 1
ITEM_PLAYER = 2
ITEM_PLAYER_FIRE = 3
ITEM_JEWEL = 4
ITEM_JEWEL_FIRE = 5
ITEM_POINTER = 6
ITEM_NONE_DIFFERENT_FIRE = 7
]]--


function pe.drawhelpframe(slide)
	local margin_bottom = pe.margin_top + pe.box_size * pe.MAP_HEIGHT
	love.graphics.setColor(pe.COLOR_FG)
	love.graphics.setFont(pe.font)
	love.graphics.printf("POCKET EXPLOSIONS",
		0, (pe.margin_top - pe.font:getHeight() * pe.font_scale) / 2,
		love.graphics.getWidth() / pe.font_scale, "center", 0, pe.font_scale
	)
	love.graphics.setColor(pe.COLOR_TEXT)
	love.graphics.printf(pe.slides_text[pe.slides[pe.help_frame].text],
		0, (margin_bottom + 2),
		love.graphics.getWidth() / pe.font_scale, "center", 0, pe.font_scale
	)
	-- board
	local x, y, fire
	local function readslide(x, y)
		return pe.slides[pe.help_frame].board[x][y]
	end
	for a = 1, 4 do for b = 1, 4 do
		x, y = pe.readbutton(a, b)
		fire =readslide(a, b) % 2 == 1
		if readslide(a, b) == 6 then
			pe.drawpointer(x, y, fire)
		end
		if readslide(a, b) == 7 then
			pe.drawfire(x, y, fire, pe.COLOR_HELPFIRE)
		else
			pe.drawfire(x, y, fire)
		end
		if readslide(a, b) == 2 or readslide(a, b) == 3 then
			pe.drawplayer(x, y, fire)
		elseif readslide(a, b) == 4 or readslide(a, b) == 5 then
			pe.drawjewel(x, y, fire)
		end
	end end
end
