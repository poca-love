﻿--[[
This file is a part of POCA - a puzzle game

POCA is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


ending = {

hijacked_love_functions = {
	'update', 'draw', 'resize', 'mousepressed', 'keypressed'
},


draw = function() -- hijacked function
	local canvas = ending.canvases[ending.canvases.current]
	love.graphics.setColor(1, 1, 1)
	love.graphics.draw(canvas, ending.quad, 0, 0)
	love.graphics.setColor(.4, .4, .4, 1 - ending.scale)
	love.graphics.rectangle('fill', 0, 0, screen.w, screen.h)
	love.graphics.setColor(1, 1, 1)
end,


resize = function() -- hijacked function
	resize_common()
	ending.init_canvas()
end,


mousepressed = function(mx, my, button) -- hijacked function
	if os.time() > (ending.started + 5) then ending:quit() end
end,


keypressed = function(k) -- hijacked function
	ending.mousepressed()
end,


init_canvas = function()
	ending.canvases = {
		[false] = screen.pfcanvas,
		[true] = love.graphics.newCanvas(screen.playfieldsize, screen.playfieldsize),
		current = false
	}
	ending.canvases[false]:setWrap('repeat', 'repeat')
	ending.canvases[true]:setWrap('repeat', 'repeat')
	-- draw empty floor on pfcanvas
	ending.canvases[false]:renderTo(function()
		inplayfield(function (x, y, p)
			draw_floor(x - .5, y - .5, false, nil, nil)
		end)
	end)
end,




main = function(self)
	hijack(self)
	self.scale = 1
	self.init_canvas()
	self.started = os.time()
	self.duration = 100 -- seconds
	play_music('city', true)
	ending.music_failed = (not current.music) or (not current.music:isPlaying())
	-- important, as this is timed with music
end,


quit = function(self)
	self.canvases = nil
	release(self)
	love.resize()
	change_screen('menu_main')
end,


update = function(dt) -- hijacked function
	-- update scale
	ending.scale = ending.scale * .9973
	if ending.scale <= .5 then
		local newcanvas = ending.canvases[not ending.canvases.current]
		local oldcanvas = ending.canvases[ending.canvases.current]
		ending.scale = .5
		ending.quad = love.graphics.newQuad(0, 0, screen.w, screen.h,
			screen.playfieldsize * .5, screen.playfieldsize * .5
		)
		newcanvas:renderTo(ending.draw)
		ending.canvases.current = not ending.canvases.current
		ending.scale = 1
	end
	local psize = screen.playfieldsize * ending.scale
	ending.quad = love.graphics.newQuad(
		- screen.w / 2, - screen.h / 2,
		screen.w, screen.h, psize, psize
	)
	-- exit when done
	if
		(settings.music and (not ending.music_failed) and (not current.music:isPlaying()))
		or os.time() > (ending.started + ending.duration)
	then
		ending:quit()
	end
	-- wait
	love.timer.sleep(.05)
end,


} -- end of ending
