--[[
This file is part of POCA - a puzzle game

POCA is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


trinkets = {
	['1000points'] = {
		name = '1000 Points',
		bmp = love.graphics.newImage('gfx/trinkets/1000points.png'),
	},
	['2000points'] = {
		name = '2000 Points',
		bmp = love.graphics.newImage('gfx/trinkets/2000points.png'),
		description = 'Not bad.',
	},
	['3000points'] = {
		name = '3000 Points',
		bmp = love.graphics.newImage('gfx/trinkets/3000points.png'),
		description = 'Not one, not two, but three thousand points, found in Lighthouse.',
	},
	['4000points'] = {
		name = '4000 Points',
		bmp = love.graphics.newImage('gfx/trinkets/4000points.png'),
		description = 'Congratulations! These are all points in the game.',
	},
	beastfang = {
		name = 'Beast Fang',
		bmp = love.graphics.newImage('gfx/trinkets/beastfang.png'),
		description = 'A fang that once belonged to an ancient beast, sent to museum, but unfortunately lost in transport.'
	},
	cigarettes = {
		name = 'A Box of Cigarettes',
		bmp = love.graphics.newImage('gfx/trinkets/cigarettes.png'),
		description = 'A box of highly addictive cigarettes found in the City.',
	},
	casette = {
		name = 'POCA Soundtrack',
		bmp = love.graphics.newImage('gfx/trinkets/casette.png'),
		onclick = function() change_screen('menu_casette') end
	},
	chocolate = {
		name = 'Chocolate',
		bmp = love.graphics.newImage('gfx/trinkets/chocolate.png'),
	},
	frame = {
		name = 'Scintilating frame',
		bmp = love.graphics.newImage('gfx/trinkets/frame.png'),
		onclick = function() change_screen('menu_frame') end,
		description = 'A display of some sort, not immediately useful.'
	},
	gameglitch = {
		name = 'Game Glitch',
		bmp = love.graphics.newImage('gfx/trinkets/gameglitch.png'),
		onclick = function() gg:main() end,
	},
	gpl3 = {
		name = 'A copy of the GNU GPL',
		bmp = love.graphics.newImage('gfx/trinkets/gpl3.png'),
		onclick = function() license:main() end
	},
	hints_book = {
		name = 'Book of Hints',
		bmp = love.graphics.newImage('gfx/trinkets/hints_book.png'),
		description = 'This book is badly damaged and can only be opened a few times. Please use sparingly!',
		onclick = function() change_screen('menu_hints') end
	},
	marbles = {
		name = 'Marbles',
		bmp = love.graphics.newImage('gfx/trinkets/marbles.png'),
		description = 'Three glass marbles',
	},
	map = {
		name = 'Map',
		bmp = love.graphics.newImage('gfx/map.png'),
		onclick = function() change_screen('menu_map') end
	},
	matches = {
		name = 'A Matchbook',
		bmp = love.graphics.newImage('gfx/trinkets/matches.png'),
		description = 'Matches are used to light cigarettes or illuminate dark places.',
		onclick = function () change_screen('menu_matches') end,
	},
	pencil = {
		name = '3-color pencil',
		bmp = love.graphics.newImage('gfx/trinkets/pencil.png'),
		description = 'A huge pencil, that can add color to the playfield, should it become too dull',
		onclick = function()
			current.painting = not current.painting
			playfield.paint_mode = nil
			change_screen('playfield')
		end,
	},
	photos = {
		name = 'Photo Booth Photos',
		bmp = love.graphics.newImage('gfx/trinkets/photos.png'),
		description = 'Photos of me & my dad.',
	},
	pistol = {
		name = 'Pistol',
		bmp = love.graphics.newImage('gfx/trinkets/pistol.png'),
		description = 'A dangerous object, not very useful in this game'
	},
	pocketexplosions = {
		name = 'Pocket explosions',
		bmp = love.graphics.newImage('gfx/trinkets/pocketexplosions.png'),
		onclick = function() pe:main() end,
		description = 'A hard game',
	},
	rabbit = {
		name = 'Snow Dome with Rabbit',
		bmp = love.graphics.newImage('gfx/trinkets/rabbit.png'),
		description = 'A rabbit is underneath a tiny dome. When you shake it, flakes unsettle and slowly fall, like snow.'
	},
	rainbowring = {
		name = 'Rainbow Ring',
		bmp = love.graphics.newImage('gfx/trinkets/rainbowring.png'),
		onclick = function() rr:main() end,
	},
	raregem = {
		name = 'Rare Gem',
		bmp = love.graphics.newImage('gfx/trinkets/raregem.png'),
	},
	robotoy = {
		name = 'RoboToy',
		bmp = love.graphics.newImage('gfx/trinkets/robotoy.png'),
		description = 'A toy that says: "We are the space robots. We are here to protect you."'
	},
	rubenspyramid = {
		name = 'Ruben\'s Pyramid',
		bmp = love.graphics.newImage('gfx/trinkets/rubenspyramid.png'),
		onclick = function() rp:main() end,
	},
	tinyheart = {
		name = 'Tiny Heart',
		bmp = love.graphics.newImage('gfx/trinkets/tinyheart.png'),
	},
	teleport_ul = {
		name = 'Pocket Teleport',
		bmp = love.graphics.newImage('gfx/trinkets/teleport_ul.png'),
		onclick = function()
			change_screen('playfield')
			current.phase = 'play'
			secrets_arrow(-1, -1)
			next_phase()
		end,
	},
	teleport_dl = {
		name = 'Pocket Teleport',
		bmp = love.graphics.newImage('gfx/trinkets/teleport_dl.png'),
		onclick = function()
			change_screen('playfield')
			current.phase = 'play'
			secrets_arrow(-1, 1)
			next_phase()
		end,
	},
	teleport_ur = {
		name = 'Pocket Teleport',
		bmp = love.graphics.newImage('gfx/trinkets/teleport_ur.png'),
		onclick = function()
			change_screen('playfield')
			current.phase = 'play'
			secrets_arrow(1, -1)
			next_phase()
		end,
	},
	titmagazine = {
		name = 'Tit Magazine',
		bmp = love.graphics.newImage('gfx/trinkets/titmagazine.png'),
		description = 'A magazine full of interesting pictures',
	},
	toyhorse = {
		name = 'Toy Horse',
		bmp = love.graphics.newImage('gfx/trinkets/toyhorse.png'),
	},
	woodenflower = {
		name = 'Wooden Flower in a Vase',
		bmp = love.graphics.newImage('gfx/trinkets/woodenflower.png'),
	},
	autodescribe = function(self)
		for k, e in pairs(self) do
			if (type(e) == 'table') and (not e.description) and (not e.onclick) then
				for l = 1, #levels do
					if levels[l].trinket == k then
						self[k].description = self[k].name ..
							', found in ' .. level_labels[l] .. '.'
						break
					end
				end
			end
		end
	end,
}

add_scale(trinkets)
trinkets:autodescribe()


function select_trinket(d)
	if #game.trinkets > 0 then
		return (current.trinket - 1 + d) % #game.trinkets + 1
	else
		return false
	end
end


function get_trinket(trinket_name)
	if not trinket_name then return end
	play_sound('pickup')
	local place = #game.trinkets + 1
	if trinket_name == '1000points' then -- if new 1000points are picked up
		place = place - 1
		if game.trinkets[#game.trinkets] == '1000points' then
			trinket_name = '2000points'
		elseif game.trinkets[#game.trinkets] == '2000points' then
			trinket_name = '3000points'
		elseif game.trinkets[#game.trinkets] == '3000points' then
			trinket_name = '4000points'
		else -- first 1000 points
			place = place + 1
		end
	end
	game.trinkets[place] = trinket_name
	game.former_trinkets[#game.former_trinkets + 1] = trinket_name
	current.trinket = place
end


-- shifts back all trinkets after the replaced one
function lose_trinket(trinket_name)
	print('lose_trinket(' .. trinket_name .. ')')
	if not has_trinket(trinket_name) then return end
	local replace = false
	for a = 1, #game.trinkets do
		if game.trinkets[a] == trinket_name then
			replace = true
		elseif replace then
			game.trinkets[a - 1] = game.trinkets[a]
		end
	end
	game.trinkets[#game.trinkets] = nil
end


function had_trinket(t)
	for _, gt in ipairs(game.former_trinkets) do
		if gt == t then return true end
	end
	return false
end


function has_trinket(t)
	for _, gt in ipairs(game.trinkets) do
		if gt == t then return true end
	end
	return false
end


function click_corpse()
	local content = 'You search the remains of a dead explorer and find '
	local s = game.searched_corpses[game.currentlevel]
	game.searched_corpses[game.currentlevel] = true
	if not (s or had_trinket('map')) then
		get_trinket('map')
		content = content .. 'a map.'
	elseif not (s or had_trinket('rubenspyramid')) then
		if not has_trinket('hints_book') then
			get_trinket('hints_book')
			content = content .. 'a Book of Hints and '
		end
		get_trinket('rubenspyramid')
		content = content .. 'a strange puzzle.'
	else
		content = content .. 'nothing special.'
	end
	return content
end


