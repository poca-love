--[[
This file is part of POCA - a puzzle game

POCA is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]



function secrets_big(bigx, bigy)
	current.painting = false -- done automatically if pencil
	big_playfield.selectedx, big_playfield.selectedy = bigx, bigy
end


-- an unique number that depends on direction and current level
function secrets_arrow_number(dx, dy)
	local direction
	for a = 1, #directions do
		if (dx == directions[a][1]) and (dy == directions[a][2]) then
			direction = a - 1 / 2 -- 0 to 3
		end
	end
	local slevel = game.currentlevel - level_labels['Secrets'] -- 0 to 15
	return slevel + direction * 16
end


-- click on secrets arrow
-- check death or go immediately if already used
function secrets_arrow(dx, dy)
	local function secrets_go(dx, dy)
		local newx = big_playfield.selectedx + dx
		local newy = big_playfield.selectedy + dy
		if newx < 1 or newx > 4 or newy < 1 or newy > 4 then
			newx = big_playfield.selectedx
			newy = big_playfield.selectedy
		end
		-- return level
		return level_labels['Secrets'] + newx - 1 + (newy - 1) * 4
	end
	local san = secrets_arrow_number(dx, dy)
	-- if diagonal or used
	if (dx ~= 0 and dy ~= 0) or game.secrets_arrows_used[san] then
		fade_to_level(secrets_go(dx, dy)) -- go immediately
	else
		playfield.goto = {secrets_go(dx, dy), san} -- go if player survives
	end
end


-- show arrows hidden in gifts, if already used
function secrets_check_gifts()
	if not level_labels[game.currentlevel] == 'Secrets' then return end
	inplayfield(function (x, y, p) if p.gift then
		local tile = recognizeink(p.gift)
		if (tile.kind == 'secrets_arrow') and tile.used then
			playfield[x][y].contains = tile
			playfield[x][y].used = tile.used
			playfield[x][y].onclick = tile.onclick
		end
	end end)
end


function paint_mode(x, y)
	-- cycles through paint modes
	if not x then
		if not playfield.paint_mode then playfield.paint_mode = 0 end
		playfield.paint_mode = (playfield.paint_mode + 1) % 4
		if playfield.paint_mode == 0 then playfield.paint_mode = nil end
		return
	-- ... or sets a pixel
	else
		if not game.painted_levels[game.currentlevel] then
			game.painted_levels[game.currentlevel] = {}
		end
		local pl = game.painted_levels[game.currentlevel]
		pl[x] = pl[x] or {}
		if pl[x][y] == playfield.paint_mode then
			pl[x][y] = 0
		else
			pl[x][y] = playfield.paint_mode
		end
		paint_level()
	end
end


-- paints level_colors (4x4 canvas) according to game.painted.levels[currentlevel]
function paint_level()
	if not level_colors then
		level_colors = love.graphics.newCanvas(4, 4)
		level_colors:setFilter('linear', 'linear')
	else
		level_colors:renderTo(love.graphics.clear)
	end
	local pl = game.painted_levels[game.currentlevel]
	if not pl then return end
	inplayfield(function (x, y, p)
		local mode = 0
		if pl[x] then mode = pl[x][y] end
		if mode == 1 then
			love.graphics.setColor(0,1,0, .5)
		elseif mode == 2 then
			love.graphics.setColor(1,0,0, .5)
		elseif mode == 3 then
			love.graphics.setColor(1,1,0, .5)
		else
			love.graphics.setColor(0,0,0, 0)
		end
		love.graphics.setCanvas(level_colors)
		love.graphics.setPointSize(1)
		love.graphics.setBlendMode('replace')
		love.graphics.points(x - .5 , y - .5) -- should be on center of square
		love.graphics.setBlendMode('alpha')
		love.graphics.setCanvas()
	end)
end


-- mark arrows according to their colors
function auto_paint()
	local function get_mode(text)
		if text == 'green' then return 1 end
		if text == 'red' then return 2 end
		if text == 'yellow' then return 3 end
	end
	game.painted_levels[game.currentlevel] = {}
	inplayfield(function(x, y, p)
		local mode = get_mode(p.kind)
		if mode then
			game.painted_levels[game.currentlevel][x] =
				game.painted_levels[game.currentlevel][x] or {}
			game.painted_levels[game.currentlevel][x][y] = mode
		end
	end)
	paint_level()
end


function setup_wise_man_battle()
	game.painted_levels[game.currentlevel] = nil
	paint_level()
	if game.solved_levels[game.currentlevel] then return end
	if not game.wisepuzzle then
		inplayfield(function (x, y, p)
			p.onclick = function ()
				change_screen('story_battle')
				playfield.clickedx, playfield.clickedy = nil, nil
				current.phase = nil -- will reset to play
			end
			if p.kind == 'wiseman' then
				playfield.wisemanx, playfield.wisemany = x, y
			end
		end)
	else -- puzzle finished
		game.solved_levels[game.currentlevel] = true
		game.wisepuzzle = false
		game.shuffled = false
		inplayfield(function (x, y, p)
			if p.kind == 'wiseman' then
				playfield[x][y] = recognizeink('D')
			end
		end)
	end
end


function start_wise_man_battle()
	inplayfield(function (x, y, p) p.onclick = nil end)
	playfield[playfield.wisemanx][playfield.wisemany].onclick = function ()
		current.died = true
		current.showkill = true
		draw_foreground()
	end
	current.symbolic = true
	-- shuffle playfield
	local wisemix = random_list(16)
	local pl = game.painted_levels[game.currentlevel]
	local plnew = { {}, {}, {}, {} }
	game.shuffled = { {}, {}, {}, {} }
	for x = 1, 4 do for y = 1, 4 do
		local a = x + (y - 1) * 4
		game.shuffled[x][y] = {}
		local rx = (wisemix[a] - 1) % 4 + 1
		local ry = math.floor((wisemix[a] - 1) / 4) + 1
		game.shuffled[x][y].x = rx
		game.shuffled[x][y].y = ry
		if pl and pl[x] and pl[x][y] then plnew[rx][ry] = pl[x][y] end
	end end
	game.painted_levels[game.currentlevel] = plnew
	paint_level()
	change_screen('playfield')
end


function start_wise_man_puzzle()
	inplayfield(function (x, y, p) playfield[x][y] = recognizeink() end)
	playfield.clickedx, playfield.clickedy = nil, nil
	playfield.wisemanx, playfield.wisemany = nil, nil
	game.painted_levels[game.currentlevel] = nil
	paint_level()
end


function setup_big_death()
	playfield = small_playfield
	setuplevel()
	setup_big_playfield()
	anim = {}
	current.phase = 'play'
	change_screen('story_big_death')
	screen.minimap:renderTo(draw_minimap)
end


function events(e)
	if e == 'pill on' then
		game.underpill = true
		setuplevel(level_labels['Knockout'])
	elseif e == 'pill off' then
		game.underpill = false
		game.devil = false
		setuplevel(level_labels['Genie'])
	elseif e == 'devil on' then
		game.devil = true
	end
end


-- used in minigames
-- replaces functions from the main game
-- old functions are saved as rr.old_xx
function hijack(mini)
	local h = mini.hijacked_love_functions
	for a = 1, #h do
		mini['old_' .. h[a]] = love[h[a]]
		love[h[a]] = mini[h[a]]
	end
	local h = mini.hijacked_globals
	if not h then return end
	for a = 1, #h do
		mini['old_' .. h[a]] = _G[h[a]]
		_G[h[a]] = mini[h[a]]
	end
end


-- restore hijacked functions and globals
function release(mini)
	local h = mini.hijacked_love_functions
	for a = 1, #h do
		love[h[a]] = mini['old_' .. h[a]]
	end
	local h = mini.hijacked_globals
	if not h then return end
	for a = 1, #h do
		_G[h[a]] = mini['old_' .. h[a]]
	end
end


-- get 3 random objects from wise_man_has
-- exclude the last one (just donated)
function wise_get_offer()
	local wmh = game.wise_man_has
	local r = random_list(#game.wise_man_has - 1)
	return { wmh[r[1]], wmh[r[2]], wmh[r[3]] }
end


-- remove an item from wise man's inventory
-- preserve order, as last item is recently donated and won't be offered
function wise_man_lose(trinket_name)
	local wmh = game.wise_man_has
	local lost = false
	for a = 1, #wmh do
		lost = lost or (wmh[a] == trinket_name)
		if lost then game.wise_man_has[a] = wmh[a + 1] end
	end
end


wise_advices = {
	advice_yellow = {'advice on turning yellow arrows', 'There is a genie in the red box, that makes yellow arrows turn by themselves. It never rotates the same arrow twice in a row.'},
	advice_genie = {'advice on stopping yellow arrows', 'To get rid of the genie, you\'d have to go back in time, helped by a pill.'},
	advice_pill = {'advice on the location of the pill', 'There is a pill at the very end of this road, locked by purple arrows.'},
	advice_purple = {'advice on the behavior of purple arrows', 'All purple arrows turn as one. To make a lasting change, seek places with odd number of purple arrows!'},
	advice_map = {'story of the map', 'Long ago I created a map of this area. It shows passages, gates and where to unlock them. But I traded it with a certain explorer. Should you meet him, ask him about it!'},
	advice_boxes = {'advice on the behavior of boxes', 'If you leave boxes alone, they will open at the end of the level. Otherwise, they will just disappear. Such it is also in life. Too strong a wish becomes an obstacle towards what you seek.'},
}


function draw_title()
	local t = screen.tilesize
	local tt = t / 8
	hatch:send('line_transparency', .7)
	love.graphics.setShader(hatch)
	inplayfield(function (x, y, p)
		draw_floor(x - .5, y - .5)
	end)
	playfield = big_playfield
	inplayfield(function (x, y, p)
		draw_image(p, x - .5, y - .5)
	end)
	playfield = small_playfield
	love.graphics.setShader()
	draw_image({name = 'po'}, 1.5, 0.5)
	draw_image({name = 'ca'}, 1.5, 1.5)
	draw_image({name = 'version'}, 2.5, 2.5)
	love.graphics.setColor(1, 1, 1)
	--love.graphics.rectangle('fill', t * 2 + tt , t * 3 + tt, t - 2 * tt, t- 2 * tt)
	--love.graphics.setColor(.4, .4, .4)
	love.graphics.printf('path of colored arrows',
		t * 2, t * 3 + (t - 3 * screen.fontheight) / 2,
		t, 'center'
	)
	love.graphics.setColor(1, 1, 1)
end
