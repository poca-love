--[[
Game glitch is a part of POCA - a puzzle game

POCA is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

require('special')

gg = {

hijacked_love_functions = {
	'update', 'draw', 'resize', 'mousepressed', 'keypressed'
},


draw = function() -- hijacked function
	love.graphics.push()
	love.graphics.translate(screen.w / 2 - 23 * gg.scale, 0)
	-- BACKGROUND
	love.graphics.clear(gg.bgr / 255, gg.bgg / 255, gg.bgb / 255)
	-- decorations
	love.graphics.setColor(1,1,1, 1)
	for a = 0, 9 do
		if gg.decos[a].show then
			love.graphics.print(gg.decos[a].look, gg.decos[a].x, gg.decos[a].y)
		end
	end
	-- FOREGROUND
	-- stars and dots
	for a = 0, #gg.stars, 4 do
		love.graphics.setColor(1,1,1, gg.stars[a].alpha) -- alpha 1 if nil
		for b = 0, 3 do
			love.graphics.print('*',
				gg.stars[a + b].x, gg.stars[a + b].y
			)
		end
		for b = 0, 2, 2 do
			love.graphics.print('.',
				gg.dots[(a + b) / 2].x, gg.dots[(a + b) / 2].y - gg.dotdy
			)
		end
		-- lines - visible only after frame 60
		love.graphics.setColor(1,1,1,
			(gg.stars[a].alpha or 1) * (math.min(gg.bb, 180) - 60) / 300
		)
		love.graphics.line(
			gg.stars[a].x + gg.linedy, gg.stars[a].y + gg.linedy,
			gg.stars[a + 1].x + gg.linedy, gg.stars[a + 1].y + gg.linedy,
			gg.stars[a + 3].x + gg.linedy, gg.stars[a + 3].y + gg.linedy,
			gg.stars[a + 2].x + gg.linedy, gg.stars[a + 2].y + gg.linedy,
			gg.stars[a].x + gg.linedy, gg.stars[a].y + gg.linedy

		)
		-- lines connecting 2 steps
		local step_height = 3 * gg.scale
		love.graphics.line (
			gg.stars[a].x + gg.linedy, gg.stars[a].y + gg.linedy,
			gg.stars[a].x + gg.linedy, gg.stars[a].y + gg.linedy - step_height
		)
		love.graphics.line (
			gg.stars[a + 2].x + gg.linedy, gg.stars[a + 2].y + gg.linedy,
			gg.stars[a + 2].x + gg.linedy, gg.stars[a + 2].y + gg.linedy - step_height
		)

	end
	love.graphics.pop()
end,



resize = function() -- hijacked function
	resize_common()
	love.graphics.setFont(screen.defaultfont)
end,


mousepressed = function(mx, my, button) -- hijacked function
	-- no interaction intended
end,


keypressed = function(k) -- hijacked function
	-- no interaction intended
end,


init = function(self)
end,


main = function(self)
	hijack(self)
	love.graphics.setFont(screen.defaultfont)
	-- init
	self.resize()
	self.bb = 0 -- main timer
	self.aa = 0
	self.lastbb = 320
	self.bgr = 128
	self.bgg = 0
	self.bgb = 128
	self.speed = 25
	self.scale = screen.tilesize / 8  -- math.floor(screen.h / 45)
	self.offsetx = - (self.scale * 23 - screen.w) / 2
	self.dotdy = screen.fontheight / 3.4
	self.linedy = screen.fontheight / 2.7
	self.steps = 15 -- math.floor(screen.h / 30)
	-- falling decorations in background look like this
	self.decos = {
		[0] = { look = "." },
		[1] = { look = "." },
		[2] = { look = "." },
		[3] = { look = ":" },
		[4] = { look = "." },
		[5] = { look = "." },
		[6] = { look = "^" },
		[7] = { look = "|" },
		[8] = { look = ":" },
		[9] = { look = "|" }
	}
	for a = 0, 9 do
		self.decos[a].x = math.random(50 * self.scale)
		self.decos[a].y = math.random(self.steps * 2 * self.scale)
	end
	-- last 3 decos fall faster, so shift them up
	for a = 7, 9 do self.decos[a].y = self.decos[a].y /2 end
	-- stars and dots are used in foreground
	self.stars, self.dots = {}, {}
	-- music
	play_music('stairway', true)
	gg.music_failed = (not current.music) or (not current.music:isPlaying())
	-- important, as this is timed with music
end,


quit = function(self)
	release(self)
	love.resize()
	change_screen('menu_objects')
end,


update = function(dt) -- hijacked function
	-- BACKGROUND
	-- fade bgcolor from purple to blue to black
	if gg.bb > .15625 * gg.lastbb and gg.bb < .25625 * gg.lastbb then
		gg.bgr = 128 - (gg.bb - .15625 * gg.lastbb) * 8
		gg.bgg = 0
		gg.bgb = 128
	elseif gg.bb > .3125 * gg.lastbb and gg.bb < .4125 * gg.lastbb then
		gg.bgr = 0
		gg.bgg = 0
		gg.bgb = 128 - (gg.bb - .3125 * gg.lastbb) * 4
	end
	-- turn decorations on
	if math.random() > 0.01 then
		gg.decos[math.random(0, 6)].show = true
	end
	-- turn decorations off and shift them randomly
	if math.random() > 0.8 then
		local a = math.random(0, 6)
		gg.decos[a].show = false
		gg.decos[a].x = math.random() * 50 * gg.scale
		gg.decos[a].y = math.random() * gg.steps * 2 * gg.scale
	end
	-- turn on and off decrations with characters | : |
	gg.decos[7].show = (gg.bb >= 53) and (gg.bb < 90)
	gg.decos[8].show = (gg.bb >= 153) and (gg.bb < 190)
	gg.decos[9].show = (gg.bb >= 103) and (gg.bb < 149)
	-- make decorations drop
	for a = 0, 6 do
		if gg.decos[a].show then gg.decos[a].y = gg.decos[a].y + 1 end
	end
	-- make elongated decorations drop faster
	for a = 7, 9 do
		if gg.decos[a].show then gg.decos[a].y = gg.decos[a].y + 6 end
	end

	-- FOREGROUND
	for step = 0, gg.steps - 1 do
		-- rotate stars along a flattened circle
		local as = gg.aa / gg.speed + step / 2
		gg.stars[0 + step * 4] = {
			x = math.sin(as - 1/4) * 15 * gg.scale,
			y = math.cos(as - 1/4) * 2 * gg.scale
		}
		gg.stars[1 + step * 4] = {
			x = math.sin(as + 1/4) * 15 * gg.scale,
			y = math.cos(as + 1/4) * 2 * gg.scale
		}
		gg.stars[2 + step * 4] = {
			x = math.sin(as - 1/4) * 5 * gg.scale,
			y = math.cos(as - 1/4) * 2/3 * gg.scale
		}
		gg.stars[3 + step * 4] = {
			x = math.sin(as + 1/4) * 5 * gg.scale,
			y = math.cos(as + 1/4) * 2/3 * gg.scale
		}
		-- place dots between some of the stars
		for a = 0, 1 do
			gg.dots[a + step * 2] = {
				x = (gg.stars[a + step * 4].x + gg.stars[a + 2 + step * 4].x) / 2,
				y = (gg.stars[a + step * 4].y + gg.stars[a + 2 + step * 4].y) / 2,
			}
		end
	end
	-- stretch and center the whole thing
	for step = 0, gg.steps - 1 do
		for a = step * 4, step * 4 + 3 do
			gg.stars[a].x = gg.stars[a].x + 23 * gg.scale
			gg.stars[a].y = gg.stars[a].y + 3 * gg.scale * (step +
				gg.aa * 2 / gg.speed - 1)
		end
		for a = step * 2, step * 2 + 1 do
			gg.dots[a].x = gg.dots[a].x + 23 * gg.scale
			gg.dots[a].y = gg.dots[a].y - 3 + 3 * gg.scale * (step +
				gg.aa * 2 / gg.speed - 1)
		end
	end

	-- fade ends
	for a = 0, gg.steps * 4 - 1 do
		-- gg.aa goes from 0 to gg.speed / 2
		if a > gg.steps * 4 - 5 then -- fade last step into bgr bgg bgb
			gg.stars[a].alpha = 1 - gg.aa / (gg.speed / 2)
		elseif a < 4 then -- fade first step
			gg.stars[a].alpha = gg.aa / (gg.speed / 2)
		end
	end

	-- update timers
	gg.aa = (gg.aa + 1) % (gg.speed / 2)
	gg.bb = gg.bb + 1

	-- exit when done
	if (settings.music and (not gg.music_failed) and (not current.music:isPlaying())) or gg.bb > gg.lastbb then
		gg:quit()
	end

	-- wait
	love.timer.sleep(.15)
end,


} -- end of gg
