--[[
POCA - a puzzle game
Copyright 2019, 2020 Pajo <xpio at tut dot by>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

require('images')
require('special')
require('levels')
require('hints')
require('anim')
require('menu')
require('trinkets')
require('saves')
require('background')
-- minigames
require('rainbowring')
require('rubens')
require('pocketexplosions')
-- sequences
require('license')
require('gameglitch')
require('ending')
-- load optional debug things
local d = package.loaders[2]('debug_things')
if type(d) == 'function' then d() end
d = nil

function init()

	-- this table will be saved when quitting or saving game
	game = {
		purple = false,
		devil = false,
		underpill = false,
		shuffled = false,
		wisepuzzle = false,
		searched_corpses = {},
		seen_levels = {},
		solved_levels = {},
		shown_screens = {},
		painted_levels = {},
		trinkets = {},
		former_trinkets = {},
		secrets_arrows_used = {},
		currentlevel = 1,
		wise_man_has = {
			'pencil', 'pocketexplosions', 'frame',
			'advice_yellow', 'advice_genie', 'advice_pill',
			'advice_purple', 'advice_map', 'advice_boxes',
		},
		matches = 3,
		cigarettes = 3,
	}

	if current and current.music then
		current.music:stop()
		current.music:release()
		current.music = nil
	end
	-- this table is reset on start or load
	current = {
		symbolic = false,
		music = nil,
		played = nil,
		temppurple = false,
		died = false,
		showkill = false,
		timer = 0,
		screen = 'playfield', -- 'playfield', 'menu_objects', 'story_beginning' ...
		phase = 'play', -- see next_phase()
		trinket = nil,
		painting = false,
	}

	small_playfield = {{}, {}, {}, {}, name = 'small'}
	big_playfield = {{}, {}, {}, {}, name = 'big'}
	setup_big_playfield() -- unsets game.purple and current.temppurple
	playfield = small_playfield
end


function love.load()
	hatch = love.graphics.newShader [[
		uniform float line_distance;
		uniform float line_transparency;
		vec4 effect(vec4 color, Image tex, vec2 texture_coords, vec2 screen_coords)
		{
			vec4 tc = Texel(tex, texture_coords) * color;
			float thickness = line_distance / 4.0;
			float m = mod(screen_coords.x, line_distance);
			float n = mod(screen_coords.y, line_distance);
			m = clamp((m + n) / 2.0 - thickness, line_transparency, 1.0);
			return vec4(vec3(tc.r, tc.g, tc.b) * m, tc.a);
		}
	]]
	-- initialize uniforms separarely for compatibility
	hatch:send('line_distance', 16.0)
	hatch:send('line_transparency', .7)
	directions = {
		{1, 0}, {1, -1}, {0, -1}, {-1, -1},
		{-1, 0}, {-1, 1}, {0, 1}, {1, 1}
	}
	math.randomseed(os.time())
	settings = {
		speed = 0, -- normal
		sounds = true,
		sounds_volume = 1,
		music = true,
		music_volume = 1,
		vibrate = false,
	}
	load_settings()
	load_sounds()
	-- load_game includes init() and setuplevel()
	load_game('autosave')
	screen = {}
	love.resize()
	-- first time, enter game directly, afterwards, enter title screen
	if not (current.screen == 'story_beginning') then change_screen('menu_main') end
end


function explode_clicked()
	if not playfield.clickedx then return end
	local p = playfield[playfield.clickedx][playfield.clickedy]
	if p.kind == 'pill' then
		events('pill on')
		return
	end
	if p.kind == 'memory' then -- remember
		playfield[playfield.clickedx][playfield.clickedy] = p.memory
		playfield.remembered = true
		return
	end
	-- mark all purple arrow tiles if a purple arrow is clicked
	-- even if only a memory
	if p.kind == 'purple' then
		current.temppurple = not current.temppurple
		inplayfield(function (x, y, p) if p.kind == 'purple' or (p.memory and p.memory.kind == 'purple') then
			playfield[x][y].hit = true
			local pf = playfield[x][y].memory or playfield[x][y]
			pf.center = 'dot'
		end end)
	end

	-- clicked arrows should explode, even if player dies
	-- to explain turning of yellow arrows, and just for fun
	-- first, copy them to new table
	local newtile = recognizeink('0')
	newtile.trinket = p.trinket -- preserve trinket information
	newtile.afterclick = p.afterclick -- preserve ac function
	if p.shape == 'x' or p.shape == '+' then -- perhaps needs more conditions later
		newtile.arrows = {}
		newtile.color = images[p.name].color
		newtile.shape = p.shape
		newtile.gift = p.gift
		newtile.arrowgift = p.arrowgift
		if p.sleep then
			newtile.sleep = 4
			newtile.sleeping_name = p.sleeping_name or p.name
		end

		for dir, arr in ipairs(p.arrows) do
			-- arrows are false or integer: distance from tile center
			newtile.arrows[dir] = arr
			if arr then
				local discard, distance, pf = check_direction(playfield.clickedx, playfield.clickedy, directions[dir][1], directions[dir][2], playfield.clickedx, playfield.clickedy)
				local target
				if pf and pf.kind == 'yellow' then
					pf.hit = true
					target = pf
				end -- mark yellow arrows for rotation
				explode_arrow(playfield.clickedx, playfield.clickedy, dir, distance, false, target)
			end
		end
	end
	if game.underpill then return end
	playfield[playfield.clickedx][playfield.clickedy] = newtile
end


function forget_clicked()
	--only when underpill, transform clicked (and exploded) arrow to the memory
	if not (game.underpill and playfield.clickedx) then return end
	if playfield.remembered then playfield.remembered = false ; return end
	local p = playfield[playfield.clickedx][playfield.clickedy]
	local p_d = p.devil -- to preserve devil after becoming a memory
	playfield[playfield.clickedx][playfield.clickedy] = recognizeink(get_memory_name(p.name))
	if playfield[playfield.clickedx][playfield.clickedy].memory then
		playfield[playfield.clickedx][playfield.clickedy].memory.devil = p_d
	end
end


function check_player_death()
	inplayfield(function(x, y, p)
		if not p.arrows then return end
		for direction, arrow in ipairs(p.arrows) do
			if not p.backgrounded and arrow then
				local death, distance = check_direction(x, y,
					directions[direction][1], directions[direction][2],
					playfield.clickedx, playfield.clickedy)
				if death then
					explode_arrow(x, y, direction, distance, true) -- kill=true
					current.died = true
					if playfield.name == 'big' then setup_big_death() end
				end
				if not current.died and playfield.wisemanx then
					local wisedeath, distance = check_direction(x, y,
						directions[direction][1], directions[direction][2],
						playfield.wisemanx, playfield.wisemany
					)
					if wisedeath then
						explode_arrow(x, y, direction, distance, false)
						game.wisepuzzle = true
						play_sound('dead')
					end
				end
			end
		end
	end)
end


function influence_and_rotate()
	-- devil influence on yellow arrows
	if game.devil then
		local dx, dy
		local yellow_arrows = {}
		inplayfield(function(x, y, p)
			if p.kind == 'yellow' then
				if p.devil then
				-- don't insert last influenced yellow arrow
					playfield[x][y].devil = nil -- unset but remember in case there are no more arrows
					dx, dy = x, y
				elseif not (not playfield.remembered and
				playfield.clickedx == x and playfield.clickedy == y) then
				-- don't insert arrow just passing into memory
					table.insert(yellow_arrows, {x, y})
				end
			end
		end)
		if #yellow_arrows > 0 then
			-- choose among available yellow arrows
			dx, dy = unpack(yellow_arrows[math.random(#yellow_arrows)])
			playfield[dx][dy].center = 'dot'
			if playfield[dx][dy].hit then
				rotate_arrows(dx, dy, 0) -- will just remove dot
			else
				rotate_arrows(dx, dy, -1)
			end
			playfield[dx][dy].hit = false -- so won't be rotated later
		end
		if dx then --set devil on some of the arrows in foreground
			playfield[dx][dy].devil = true
			-- unset any devil in memories
			inplayfield(function(x, y, p)
				if p.memory and p.memory.devil then
					playfield[x][y].memory.devil = nil
				end
			end)
		end -- new or last dx, dy in case there was only 1
	end
	-- turn yellow and purple arrows
	inplayfield(function(x, y, p)
		if (p.kind == 'yellow' or p.kind == 'purple' or (p.kind == 'memory' and p.memory.kind == 'purple')) and p.hit then
			rotate_arrows(x, y)
			p.hit = false
		end
	end)
	-- wake regenerating arrows up
	inplayfield(function(x, y, p)
		if p.sleep and p.sleep > 0 then
			p.sleep = p.sleep - 1
			if p.sleep == 0 then
				-- wake arrows but preserve redirection
				-- (only relevant for wise man battle)
				playfield[x][y] = recognizeink(p.sleeping_name)
				grow_arrows(x, y)
			end
		end
	end)
	-- rotate red arrows
	inplayfield(function(x, y, p)
		if p.kind == 'red' or p.kind == 'loadred' then rotate_arrows(x, y) end
	end)
end


function next_phase()
	local nextphases = {'rotate', 'forget', 'die', 'level', 'play', 'explode'}
	setmetatable(nextphases, {__index = function (table, key)
		for a = 1, #table do
			if rawget(table, a) == key then return table[a + 1] or table[1] end
		end
	end})

	current.phase = nextphases[current.phase] or 'play' -- nextphases['opengifts'] == nil , etc.
	if current.phase == 'explode' then -- 1. explode clicked arrow
		explode_clicked()
	elseif current.phase == 'rotate' then -- 2.
		influence_and_rotate()
	elseif current.phase == 'forget' then -- 3. pass to memory if underpill
		forget_clicked()
	elseif current.phase == 'die' then -- 4. check for player death
		check_player_death()
	elseif current.phase == 'level' then -- 5. check for level end
		check_level()
	elseif current.phase == 'play' then -- 6. await next click
		playfield = small_playfield
	end
end


function remember_arrows()
	if game.underpill then return end
	playfield.clickedx, playfield.clickedy = nil, nil
	inplayfield(function (x, y, p)
		if p.memory then playfield[x][y] = p.memory end
	end)
end


function resize_common()
	-- variables from the main game, needed for menu, etc.
	-- disregard screen sizes below 50x50 (crashes font otherwise)
	screen.w = math.max(love.graphics.getWidth(), 50)
	screen.h = math.max(love.graphics.getHeight(), 50)
	screen.tilesize = math.min(screen.w * .9 / 4, screen.h * .9 / 5)
	screen.playfieldsize = screen.tilesize * 4
	screen.playfieldx = math.floor((screen.w - screen.playfieldsize) / 2)
	screen.playfieldy = math.floor((screen.h - screen.playfieldsize) / 2) + screen.tilesize / 2
	screen.pfcanvas = love.graphics.newCanvas(screen.playfieldsize, screen.playfieldsize)
	screen.defaultfont = love.graphics.newFont(screen.tilesize / 5)
	screen.fontheight = screen.defaultfont:getHeight()
	love.graphics.setFont(screen.defaultfont)
	local s = love.graphics.getPixelDimensions() / love.graphics.getDimensions()
	hatch:send('line_distance', screen.tilesize * s / 10)
end


function love.resize()
	resize_common()
	screen.pfquads = {}
	for x = 1, 4 do
		screen.pfquads[x] = {}
		for y = 1, 4 do
		screen.pfquads[x][y] = love.graphics.newQuad(
			(x - 1) * screen.tilesize, (y - 1) * screen.tilesize,
			screen.tilesize, screen.tilesize,
			screen.playfieldsize, screen.playfieldsize
		)
		end
	end
	screen.minimap = love.graphics.newCanvas(screen.tilesize * 4/5, screen.tilesize * 4/5)
	screen.minimap:renderTo(draw_minimap)
	love.graphics.setCanvas({ {screen.pfcanvas}, stencil = true })
	if current.screen == 'menu_main' then
		draw_title()
	else
		draw_playfield()
	end
	love.graphics.setCanvas()
end


function love.update(dt)
	if current.screen ~= 'playfield' then return end
	if dt < 1/50 then
		love.timer.sleep(1/30 - dt)
	end
	current.timer = current.timer + dt
	if not update_anim(dt) then next_phase() end
	screen.changed = true --!! stub
	if screen.changed then
		love.graphics.setCanvas({ {screen.pfcanvas}, stencil = true })
		draw_playfield()
		love.graphics.setCanvas()
		if playfield.name == 'big' then
			screen.minimap:renderTo(draw_minimap)
		end
		screen.changed = false
	end
end


function inplayfield(f)
	for y = 1, 4 do for x = 1, 4 do
		f(x, y, playfield[x][y])
	end end
end


function love.draw()
	draw_background()
	if current.screen == 'playfield' then
		draw_osd()
		draw_foreground()
	else
		draw_menu()
	end
end


function check_direction(x, y, dx, dy, targetx, targety)
	local p, xx, yy
	for a = 1, 4 do
		xx, yy = x + dx * a, y + dy * a
		if not playfield[xx] then return false, a end -- arrow hits playfield edge
		p = playfield[xx][yy]
		if not p then return false, a end --||--
		if xx == targetx and yy == targety then return true, a end -- arrow hits player
		if not p.passive then return false, a, p end -- arrow hits another object. return that object
	end
	return false, 3
end


function check_level()
	local function update(list)
		update_seen_levels(
			list, game.currentlevel,
			game.devil, game.underpill, game.purple,
			levels[game.currentlevel].has_yellow,
			levels[game.currentlevel].has_purple
		)
	end
	-- things to do when not dead
	if game.wisepuzzle then
		if playfield.wisemanx then
			start_wise_man_puzzle()
			return
		end
		for x = 1, 4 do for y = 1, 4 do
			if game.shuffled[x][y].x ~= x or game.shuffled[x][y].y ~= y then return end
		end end
		-- puzzle solved
		setuplevel(level_labels['Remorse'])
		return
	end
	-- teleport arrows in Secrets use this.
	-- they work only if player survived
	if not current.died and playfield.goto then
		game.secrets_arrows_used[playfield.goto[2]] = true
		fade_to_level(playfield.goto[1])
		return
	end
	-- afterclick: get trinket, search corpse
	if
		not current.died and
		playfield[playfield.clickedx][playfield.clickedy].afterclick
	then
		playfield[playfield.clickedx][playfield.clickedy]:afterclick()
		--get_trinket(playfield[playfield.clickedx][playfield.clickedy].trinket)
	end

	-- checks for the level end (when all arrows are eliminated)
	-- game should generally check for arrows not shapes!
	local anymemories, anyactivearrows = false, false
	inplayfield(function(x, y, p)
		anymemories = anymemories or p.memory
		if p.arrows then for direction, arrow in ipairs(p.arrows) do
			anyactivearrows = anyactivearrows or arrow
		end end
	end)
	if game.underpill then
		if not anymemories and not current.died then
			update(game.solved_levels)
			-- consider it's seen too (for levels where states are changed)
			update(game.seen_levels)
			game.purple = current.temppurple
			setuplevel(game.currentlevel - 1)
			return
		end
	else
		if not anyactivearrows and not open_gifts() then
			if playfield.name == 'small' then
				if anymemories then
					remember_arrows()
					play_sound('backwards')
				else -- level solved
					update(game.solved_levels)
					-- consider it's seen too (for levels where states are changed)
					update(game.seen_levels)
					game.purple = current.temppurple
					if current.symbolic then
						-- press big playfield
						current.phase = 'play'
						clicked(big_playfield.selectedx, big_playfield.selectedy, big_playfield)
					else
						-- go to next level
						setuplevel(game.currentlevel + 1)
					end
				end
			else -- big playfield
				ending:main()
			end
		end
	end
end


function open_gifts()
	if game.underpill then return end
	local opened = false
	inplayfield(function(x, y, p)
		if p.name == 'J' or p.name == 'K' then
			playfield[x][y] = recognizeink(p.gift)
			playfield[x][y].gift = p.gift
			playfield[x][y].ingift = true
			if p.name == 'K' then
				events('devil on')
				playfield[x][y].ingift = 'devil'
			end
			opened = true
		end
	end)
	if opened then
		animate_gifts()
		--playfield.clickedx, playfield.clickedy = nil, nil
		--necessary to preserve clicked for opening arrowgifts
	end
	return opened
end


function clicked(mx, my, pf)
	-- skip death animation and fast-forward any remaining flying arrows
	if playfield.afterdeath_phase then
		local current_speed = settings.speed
		settings.speed = 2
		if not update_anim(0) then current.phase = 'play' end
		settings.speed = current_speed
	end
	if current.screen ~= 'playfield' or current.phase ~= 'play' then
		return
	end
	playfield = pf or small_playfield
	-- click above playfield
	if mx < 1 or mx > 4 or my < 0 or my > 4 then return end
	if my == 0 then -- levels panel
		play_sound('tone')
		if mx == 1 then
			if
				current.painting or
				level_labels[game.currentlevel] == 'Secrets'
			then
				if has_trinket('pencil') then paint_mode() end
			elseif traversable(-1) then
				setuplevel(game.currentlevel - 1)
			end
		elseif mx == 4 then
			if level_labels[game.currentlevel] == 'Secrets' then
				-- do the big click if previously solved
				if game.solved_levels[game.currentlevel] then
					clicked(big_playfield.selectedx, big_playfield.selectedy, big_playfield)
					return
				end
			elseif current.painting then
				-- turn off painting when not in Secrets
				current.painting = false
				playfield.paint_mode = false
			elseif traversable(1) then
				setuplevel(game.currentlevel + 1)
			end
		else
			change_screen('menu_objects')
		end
		return
	end
	-- click on playfield
	if playfield.paint_mode then
		paint_mode(mx, my)
		return
	end
	-- click after death
	if current.died then
		setuplevel()
		return
	end
	-- clickedx, clickedy from previous turn
	if  -- arrowgift
		playfield.clickedx and
		playfield[playfield.clickedx][playfield.clickedy].arrowgift and
		not (mx == playfield.clickedx and my == playfield.clickedy) and
		not game.underpill and not playfield.remembered
	then
		local gift = playfield[playfield.clickedx][playfield.clickedy].gift
		-- open arrowgift
		animate_arrow_gift(playfield.clickedx, playfield.clickedy)
		-- preserve gift
		playfield[playfield.clickedx][playfield.clickedy].gift = gift
	end
	-- new clickedx, clickedy
	playfield.remembered = false
	if game.shuffled then
		-- wise man battle
		local oldx, oldy = playfield.clickedx, playfield.clickedy
		local oldsx, oldsy
		for x = 1, 4 do for y = 1, 4 do
			if game.shuffled[x][y].x == mx and game.shuffled[x][y].y == my then
				playfield.clickedx = x
				playfield.clickedy = y
			end
		end end
		if oldx and game.wisepuzzle then
			-- swap shuffled tiles
			oldsx = game.shuffled[oldx][oldy].x
			oldsy = game.shuffled[oldx][oldy].y
			game.shuffled[oldx][oldy].x = game.shuffled[playfield.clickedx][playfield.clickedy].x
			game.shuffled[oldx][oldy].y = game.shuffled[playfield.clickedx][playfield.clickedy].y
			game.shuffled[playfield.clickedx][playfield.clickedy].x = oldsx
			game.shuffled[playfield.clickedx][playfield.clickedy].y = oldsy
			playfield.clickedx, playfield.clickedy = nil, nil
		end
	else
		-- the usual click
		play_sound('click')
		playfield.clickedx, playfield.clickedy = mx, my
		if
			playfield[mx][my + 1] and
			playfield[mx][my + 1].kind == 'click_hint'
		then
			playfield[mx][my + 1] = recognizeink()
		end
	end
	if playfield.clickedx and playfield[playfield.clickedx][playfield.clickedy].onclick then
		playfield[playfield.clickedx][playfield.clickedy].onclick()
	end

	next_phase()
end


-- checks if point is in rectangle
function isin(mx, my, x, y, w, h)
	return mx >= x and mx <= x + w and my >= y and my <= y + h
end


-- returns a random list of given length
function random_list(length)
	local r = {1}
	for a = 2, length do
		table.insert(r, math.random(#r + 1), a)
	end
	return r
end


function love.mousepressed(mx, my, button)
	if current.screen == 'playfield' then
		small_playfield.selectedx = nil -- remove keyboard cursor
		local x = math.floor((mx - screen.playfieldx) / screen.tilesize) + 1
		local y = math.floor((my - screen.playfieldy) / screen.tilesize) + 1
		clicked(x, y)
	else
		menu_click(mx, my)
	end
end


function love.mousereleased(mx, my, button)
	love.mouse.setGrabbed(false)
	menu_click(mx, my, true)
end


function love.keypressed(k)
	if current.screen == 'playfield' then
		if k == 'escape' then
			love.event.push('quit')
		elseif k == 'left' then
			cursor_move(-1, 0)
		elseif k == 'right' then
			cursor_move(1, 0)
		elseif k == 'up' then
			cursor_move(0, -1)
		elseif k == 'down' then
			cursor_move(0, 1)
		elseif k == 'return' and small_playfield.selectedx then
			clicked(small_playfield.selectedx, small_playfield.selectedy)
		elseif k == 'm' then
			change_screen('menu_main')
		end
	else --menu
		if k == 'escape' or k == 'm' then
			change_screen('playfield')
		elseif k == 'left' then
			menu_select(-1, 0)
		elseif k == 'right' then
			menu_select(1, 0)
		elseif k == 'up' then
			menu_select(0, -1)
		elseif k == 'down' then
			menu_select(0, 1)
		elseif k == 'return' then
			menu_press()
		end
	end
	-- global keys
	if k == 'q' then
		love.event.push('quit')
	end
	if debug_keys then debug_keys(k) end
end


function cursor_move(x, y)
	-- default cursor position
	if not small_playfield.selectedx then
		small_playfield.selectedx, small_playfield.selectedy = 1, 1
		return
	end
	-- skip wide cursor
	if (small_playfield.selectedy < 1) and
		(small_playfield.selectedx + x > 1) and
		(small_playfield.selectedx + x < 4)
	then
		x = 2 * x
	end
	-- move cursor
	small_playfield.selectedx = (small_playfield.selectedx + x - 1) % 4 + 1
	small_playfield.selectedy = (small_playfield.selectedy + y) % 5

end


function love.quit()
	save(game, 'autosave')
	save(settings, 'settings')
end
